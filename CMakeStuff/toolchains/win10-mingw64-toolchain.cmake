message("\n${BoldRed}WIN10-MINGW64 environment${ColourReset}\n")
message("Please run the configuration like this:")
message("cmake -G \"Unix Makefiles\" -DCMAKE_BUILD_TYPE=Release ../development")


set(CMAKE_C_IMPLICIT_INCLUDE_DIRECTORIES "c:/msys64/mingw64/include")
set(CMAKE_CXX_IMPLICIT_INCLUDE_DIRECTORIES "c:/msys64/mingw64/bin")

set(HOME_DEVEL_DIR "$ENV{HOME}/devel")


# We do not build the tests under Win10.
set (MAKE_TEST 0)


if(WIN32 OR _WIN32)
	message(STATUS "Building with WIN32 defined.")
endif()

# see https://cmake.org/pipermail/cmake/2015-December/062166.html
set(CMAKE_NO_SYSTEM_FROM_IMPORTED 1)

set(LINKER_FLAGS "${LINKER_FLAGS} -Wl,--no-as-needed")

find_package(ZLIB REQUIRED)


if(MAKE_TEST)

  find_package(QuaZip-Qt6)
	#set(Quazip5_FOUND 1)
	#set(Quazip5_INCLUDE_DIRS /c/msys64/mingw64/include/QuaZip-Qt5-1.3)
	#set(Quazip5_LIBRARY /c/msys64/mingw64/bin/libquazip1-qt5.dll) 
	#if(NOT TARGET Quazip5::Quazip5)
		#add_library(Quazip5::Quazip5 UNKNOWN IMPORTED)
		#set_target_properties(Quazip5::Quazip5 PROPERTIES
			#IMPORTED_LOCATION             "${Quazip5_LIBRARY}"
			#INTERFACE_INCLUDE_DIRECTORIES "${Quazip5_INCLUDE_DIRS}")
	#endif()

endif()


set(liblzf_FOUND 1)
set(liblzf_INCLUDE_DIRS "${HOME_DEVEL_DIR}/lzf/development")
set(liblzf_LIBRARIES "${HOME_DEVEL_DIR}/lzf/build-area/mingw64/liblzf.dll") 
if(NOT TARGET liblzf::liblzf)
	add_library(liblzf::liblzf UNKNOWN IMPORTED)
	set_target_properties(liblzf::liblzf PROPERTIES
		IMPORTED_LOCATION             "${liblzf_LIBRARIES}"
		INTERFACE_INCLUDE_DIRECTORIES "${liblzf_INCLUDE_DIRS}"
		)
endif()


set(PwizLite_FOUND 1)
set(PwizLite_INCLUDE_DIRS "${HOME_DEVEL_DIR}/pwizlite/development/src")
set(PwizLite_LIBRARIES "${HOME_DEVEL_DIR}/pwizlite/build-area/mingw64/src/libpwizlite.dll") 
if(NOT TARGET PwizLite::PwizLite)
	add_library(PwizLite::PwizLite UNKNOWN IMPORTED)
	set_target_properties(PwizLite::PwizLite PROPERTIES
		IMPORTED_LOCATION             "${PwizLite_LIBRARIES}"
		INTERFACE_INCLUDE_DIRECTORIES "${PwizLite_INCLUDE_DIRS}"
		)
endif()


set(QCustomPlotQt6_FOUND 1)
set(QCustomPlotQt6_INCLUDE_DIRS "${HOME_DEVEL_DIR}/qcustomplot/development")
# Note the QCustomPlotQt6_LIBRARIES (plural) because on Debian, the
# QCustomPlotQt6Config.cmake file has this variable name (see the unix-specific
# toolchain file.
set(QCustomPlotQt6_LIBRARIES "${HOME_DEVEL_DIR}/qcustomplot/build-area/mingw64/libQCustomPlotQt6.dll") 
# Per instructions of the lib author:
# https://www.qcustomplot.com/index.php/tutorials/settingup
message(STATUS "Setting definition -DQCUSTOMPLOT_USE_LIBRARY.")
if(NOT TARGET QCustomPlotQt6::QCustomPlotQt6)
	add_library(QCustomPlotQt6::QCustomPlotQt6 UNKNOWN IMPORTED)
	set_target_properties(QCustomPlotQt6::QCustomPlotQt6 PROPERTIES
		IMPORTED_LOCATION             "${QCustomPlotQt6_LIBRARIES}"
		INTERFACE_INCLUDE_DIRECTORIES "${QCustomPlotQt6_INCLUDE_DIRS}"
		INTERFACE_COMPILE_DEFINITIONS QCUSTOMPLOT_USE_LIBRARY
		)
endif()


set(Alglib_FOUND 1)
set(Alglib_INCLUDE_DIRS "${HOME_DEVEL_DIR}/alglib/development/src")
set(Alglib_LIBRARY "${HOME_DEVEL_DIR}/alglib/build-area/mingw64/libalglib.dll") 
if(NOT TARGET Alglib::Alglib)
	add_library(Alglib::Alglib UNKNOWN IMPORTED)
	set_target_properties(Alglib::Alglib PROPERTIES
		IMPORTED_LOCATION             "${Alglib_LIBRARY}"
		INTERFACE_INCLUDE_DIRECTORIES "${Alglib_INCLUDE_DIRS}")
endif()


# No more used since the source code is now part of our code tree.
#set(CustomPwiz_FOUND 1)
#set(CustomPwiz_INCLUDE_DIRS "${HOME_DEVEL_DIR}/custompwiz/development/src")
#set(CustomPwiz_LIBRARY "${HOME_DEVEL_DIR}/custompwiz/build-area/src/libcustompwiz.dll.a") 
#if(NOT TARGET CustomPwiz::CustomPwiz)
#add_library(CustomPwiz::CustomPwiz UNKNOWN IMPORTED)
#set_target_properties(CustomPwiz::CustomPwiz PROPERTIES
#IMPORTED_LOCATION             "${CustomPwiz_LIBRARY}"
#INTERFACE_INCLUDE_DIRECTORIES "${CustomPwiz_INCLUDE_DIRS}")
#endif()


find_package(Boost COMPONENTS iostreams thread filesystem chrono REQUIRED ) 


set(Zstd_FOUND 1)
set(Zstd_INCLUDE_DIRS "c:/msys64/mingw64/include")
set(Zstd_LIBRARY "c:/msys64/mingw64/bin/libzstd.dll") 
if(NOT TARGET Zstd::Zstd)
	add_library(Zstd::Zstd UNKNOWN IMPORTED)
	set_target_properties(Zstd::Zstd PROPERTIES
		IMPORTED_LOCATION             "${Zstd_LIBRARY}"
		INTERFACE_INCLUDE_DIRECTORIES "${Zstd_INCLUDE_DIRS}")
endif()


set(SQLite3_FOUND 1)
set(SQLite3_INCLUDE_DIRS "c:/msys64/mingw64/include")
set(SQLite3_LIBRARY "c:/msys64/mingw64/bin/libsqlite3-0.dll") 
if(NOT TARGET SQLite::SQLite3)
	add_library(SQLite::SQLite3 UNKNOWN IMPORTED)
	set_target_properties(SQLite::SQLite3 PROPERTIES
		IMPORTED_LOCATION             "${SQLite3_LIBRARY}"
		INTERFACE_INCLUDE_DIRECTORIES "${SQLite3_INCLUDE_DIRS}")
endif()


set(OdsStream_QT5_FOUND 1)
set(OdsStream_INCLUDE_DIR "${HOME_DEVEL_DIR}/odsstream/src")
set(OdsStream_QT5_LIBRARY "${HOME_DEVEL_DIR}/odsstream/build-area/mingw64/src/libodsstream-qt5.dll")
if(NOT TARGET OdsStream::Core)
	add_library(OdsStream::Core UNKNOWN IMPORTED)
	set_target_properties(OdsStream::Core PROPERTIES
		IMPORTED_LOCATION             "${OdsStream_QT5_LIBRARY}"
		INTERFACE_INCLUDE_DIRECTORIES "${OdsStream_INCLUDE_DIR}"
		)
endif()


# On Win10 all the code is relocatable.
remove_definitions(-fPIC)
