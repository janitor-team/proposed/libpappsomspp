message("APPLE macport environment")
message("Please run the configuration like this:")
message("cmake -DCMAKE_BUILD_TYPE=Debug ../development")


set(CMAKE_C_IMPLICIT_INCLUDE_DIRECTORIES "/opt/local/include")
set(CMAKE_CXX_IMPLICIT_INCLUDE_DIRECTORIES "/opt/local/lib")


set(HOME_DEVEL_DIR "/Users/rusconi/devel")


set(LINKER_FLAGS "${LINKER_FLAGS} -Wc++17-compat")

set(CMAKE_MACOSX_RPATH 0)


set(Alglib_FOUND 1)
set(Alglib_INCLUDE_DIRS "${HOME_DEVEL_DIR}/alglib/development/src")
set(Alglib_LIBRARIES "${HOME_DEVEL_DIR}/alglib/build-area/mac/libalglib.dylib") 
if(NOT TARGET Alglib::Alglib)
	add_library(Alglib::Alglib UNKNOWN IMPORTED)
	set_target_properties(Alglib::Alglib PROPERTIES
		IMPORTED_LOCATION             "${Alglib_LIBRARIES}"
		INTERFACE_INCLUDE_DIRECTORIES "${Alglib_INCLUDE_DIRS}"
		)
endif()


set(PwizLite_FOUND 1)
set(PwizLite_INCLUDE_DIRS "${HOME_DEVEL_DIR}/pwizlite/development/src")
set(PwizLite_LIBRARIES "${HOME_DEVEL_DIR}/pwizlite/build-area/mac/src/libpwizlite.dylib") 
if(NOT TARGET PwizLite::PwizLite)
	add_library(PwizLite::PwizLite UNKNOWN IMPORTED)
	set_target_properties(PwizLite::PwizLite PROPERTIES
		IMPORTED_LOCATION             "${PwizLite_LIBRARIES}"
		INTERFACE_INCLUDE_DIRECTORIES "${PwizLite_INCLUDE_DIRS}"
		)
endif()


find_package(ZLIB REQUIRED)


set(QCustomPlot_FOUND 1)
set(QCustomPlot_INCLUDE_DIRS "${HOME_DEVEL_DIR}/qcustomplot/development")
set(QCustomPlot_LIBRARIES "${HOME_DEVEL_DIR}/qcustomplot/build-area/mac/libqcustomplot.dylib") 
# Per instructions of the lib author:
# https://www.qcustomplot.com/index.php/tutorials/settingup
message(STATUS "Setting definition -DQCUSTOMPLOT_USE_LIBRARY.")
if(NOT TARGET QCustomPlot::QCustomPlot)
	add_library(QCustomPlot::QCustomPlot UNKNOWN IMPORTED)
	set_target_properties(QCustomPlot::QCustomPlot PROPERTIES
		IMPORTED_LOCATION             "${QCustomPlot_LIBRARIES}"
		INTERFACE_INCLUDE_DIRECTORIES "${QCustomPlot_INCLUDE_DIRS}"
		INTERFACE_COMPILE_DEFINITIONS QCUSTOMPLOT_USE_LIBRARY
		)
endif()


set(Zstd_FOUND 1)
set(Zstd_INCLUDE_DIRS "/opt/local/include")
set(Zstd_LIBRARIES "/opt/local/lib/libzstd.dylib") 
if(NOT TARGET Zstd::Zstd)
    add_library(Zstd::Zstd UNKNOWN IMPORTED)
    set_target_properties(Zstd::Zstd PROPERTIES
        IMPORTED_LOCATION             "${Zstd_LIBRARIES}"
        INTERFACE_INCLUDE_DIRECTORIES "${Zstd_INCLUDE_DIRS}"
        )
endif()


add_definitions(-fPIC)

