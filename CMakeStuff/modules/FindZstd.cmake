find_path(Zstd_INCLUDE_DIRS zstd.h
  PATHS /usr/local/include /usr/include)


find_library(Zstd_LIBRARY NAMES zstd
  HINTS /usr/lib /usr/local/lib)

if(Zstd_INCLUDE_DIRS AND Zstd_LIBRARY)

  mark_as_advanced(Zstd_INCLUDE_DIRS)
  mark_as_advanced(Zstd_LIBRARY)

  set(Zstd_FOUND TRUE)

endif()

if(Zstd_FOUND)

  if(NOT Zstd_FIND_QUIETLY)
    message(STATUS "Found Zstd_LIBRARY: ${Zstd_LIBRARY}")
  endif()

  if(NOT TARGET Zstd::Zstd)

    add_library(Zstd::Zstd UNKNOWN IMPORTED)

    set_target_properties(Zstd::Zstd PROPERTIES
      IMPORTED_LOCATION             "${Zstd_LIBRARY}"
      INTERFACE_INCLUDE_DIRECTORIES "${Zstd_INCLUDE_DIRS}")

  endif()

else()

  if(Zstd_FIND_REQUIRED)

    message(FATAL_ERROR "Could not find libzstd. Please do specify the
    Zstd_INCLUDE_DIRS and Zstd_LIBRARY variables using cmake!")

  endif()

endif()

