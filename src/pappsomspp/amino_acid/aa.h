/**
 * \file pappsomspp/amino_acid/aaBase.h
 * \date 7/3/2015
 * \author Olivier Langella
 * \brief amino acid model
 */

/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/


#pragma once

#include <vector>
#include "../types.h"
#include "aabase.h"
#include "aamodification.h"


namespace pappso
{


class PMSPP_LIB_DECL Aa : public AaBase
{
  public:
  Aa(char aa_letter);
  Aa(AminoAcidChar aa_char);
  Aa(const Aa &aa);
  Aa(Aa &&toCopy); // move constructor
  Aa &operator=(const Aa &toCopy);

  virtual ~Aa();

  pappso_double getMass() const override;
  int getNumberOfAtom(AtomIsotopeSurvey atom) const override final;
  int getNumberOfIsotope(Isotope isotope) const override final;
  unsigned int getNumberOfModification(AaModificationP mod) const;

  /* \brief print modification except internal modifications */
  const QString toString() const;
  /* \brief print all modifications */
  const QString toAbsoluteString() const;


  void addAaModification(AaModificationP aaModification);
  void removeAaModification(AaModificationP aaModification);


  /** @brief replaces all occurences of a modification by a new one
   * @param oldmod modification to change
   * @param newmod new modification
   */
  void replaceAaModification(AaModificationP oldmod, AaModificationP newmod);


  AaModificationP getInternalNterModification() const;
  AaModificationP getInternalCterModification() const;
  void removeInternalNterModification();
  void removeInternalCterModification();

  const std::vector<AaModificationP> &getModificationList() const;

  bool isLesser(Aa const &r) const;
  bool isAaEqual(Aa const &r) const;

  private:
  //	const pappso_double _aa_mass;
  std::vector<AaModificationP> m_listMod;
};

bool operator<(const Aa &l, const Aa &r);

bool operator==(const Aa &l, const Aa &r);

} /* namespace pappso */
