/**
 * \file trace/linearregression.cpp
 * \date 17/9/2016
 * \author Olivier Langella
 * \brief compute linear regression
 */

/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of peptider.
 *
 *     peptider is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     peptider is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with peptider.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/
#include "linearregression.h"
#include <numeric>
#include <cmath>

using namespace pappso;
LinearRegression::LinearRegression(const Trace &data)
{

  std::size_t size = data.size();
  if(size > 2)
    {
      pappso::pappso_double x_vec_mean =

        (std::accumulate(data.begin(),
                         data.end(),
                         0,
                         [](double a, const DataPoint &b) { return a + b.x; }) /
         size);
      pappso::pappso_double y_vec_mean =
        (sumYTrace(data.begin(), data.end(), 0) / size);

      pappso::pappso_double sx  = 0;
      pappso::pappso_double sxy = 0;
      for(size_t i = 0; i < size; i++)
        {
          sx += std::pow((data[i].x - x_vec_mean), 2);
          sxy += (data[i].x - x_vec_mean) * (data[i].y - y_vec_mean);
        }
      _slope = sxy / sx;

      _intercept = y_vec_mean - (_slope * x_vec_mean);
    }
}


pappso::pappso_double
LinearRegression::getIntercept() const
{
  return _intercept;
}
pappso::pappso_double
LinearRegression::getSlope() const
{
  return _slope;
}
pappso::pappso_double
LinearRegression::getYfromX(pappso::pappso_double x) const
{
  return (_slope * x + _intercept);
}

double
LinearRegression::getRmsd(const Trace &data) const
{

  std::size_t size = data.size();
  if(size > 2)
    {

      pappso::pappso_double sum_square_deviation = 0;
      for(size_t i = 0; i < size; i++)
        {
          sum_square_deviation +=
            std::pow((data[i].y - getYfromX(data[i].x)), 2);
        }
      return sqrt(sum_square_deviation / (double)size);
    }
  return 0;
}

double
LinearRegression::getNrmsd(const Trace &data) const
{
  return (getRmsd(data) / (maxYDataPoint(data.begin(), data.end())->y -
                           minYDataPoint(data.begin(), data.end())->y));
}

double
LinearRegression::getCoefficientOfDetermination(const Trace &data) const
{
  std::size_t size = data.size();
  if(size > 2)
    {
      double meanY = meanYTrace(data.begin(), data.end());
      pappso::pappso_double sum_square_deviation = 0;
      for(size_t i = 0; i < size; i++)
        {
          sum_square_deviation +=
            std::pow((data[i].y - getYfromX(data[i].x)), 2);
        }
      pappso::pappso_double sum_square_total = 0;
      for(size_t i = 0; i < size; i++)
        {
          sum_square_total += std::pow((data[i].y - meanY), 2);
        }
      return ((double)1.0 - (sum_square_deviation / sum_square_total));
    }
  return 0;
}
