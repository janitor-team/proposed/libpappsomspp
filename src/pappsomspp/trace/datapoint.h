#pragma once

#include <vector>
#include <limits>
#include <memory>

#include <QDataStream>

#include <QMetaType>

#include "../exportinmportconfig.h"
#include "../types.h"


namespace pappso
{
struct DataPoint;
typedef std::shared_ptr<const DataPoint> DataPointCstSPtr;


struct PMSPP_LIB_DECL DataPoint
{
  pappso_double x = -1;
  pappso_double y = 0;

  DataPoint();
  DataPoint(const DataPoint &other);
  DataPoint(pappso_double x, pappso_double y);
  DataPoint(std::pair<pappso_double, pappso_double> pair);
  DataPoint(const QString &text);

  // For debugging purposes.
  //~DataPoint();

  DataPointCstSPtr makeDataPointCstSPtr() const;

  void initialize(pappso_double x, pappso_double y);
  void initialize(const DataPoint &other);
  bool initialize(const QString &text);

  void reset();

  void incrementX(pappso_double value);
  void incrementY(pappso_double value);

  bool operator==(const DataPoint &other) const;

  DataPoint &operator=(const DataPoint &other);

  bool isValid() const;

  QString toString() const;
  QString toString(int decimals) const;
};

PMSPP_LIB_DECL QDataStream &operator<<(QDataStream &out, const DataPoint &dataPoint);
PMSPP_LIB_DECL QDataStream &operator>>(QDataStream &out, DataPoint &dataPoint);
} // namespace pappso

Q_DECLARE_METATYPE(pappso::DataPoint);
extern int dataPointMetaTypeId;

Q_DECLARE_METATYPE(pappso::DataPointCstSPtr);
extern int dataPointCstSPtrMetaTypeId;
