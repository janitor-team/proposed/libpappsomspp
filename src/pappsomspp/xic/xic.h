/*******************************************************************************
 * * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 * *
 * * This file is part of MassChroqPRM.
 * *
 * *     MassChroqPRM is free software: you can redistribute it and/or modify
 * *     it under the terms of the GNU General Public License as published by
 * *     the Free Software Foundation, either version 3 of the License, or
 * *     (at your option) any later version.
 * *
 * *     MassChroqPRM is distributed in the hope that it will be useful,
 * *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 * *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * *     GNU General Public License for more details.
 * *
 * *     You should have received a copy of the GNU General Public License
 * *     along with MassChroqPRM.  If not, see <http://www.gnu.org/licenses/>.
 * *
 * * Contributors:
 * *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 * implementation
 * ******************************************************************************/

#pragma once

#include <vector>
#include <memory>
#include "../trace/trace.h"
#include "../mzrange.h"

namespace pappso
{


class XicPeak;

class Xic;
typedef std::shared_ptr<const Xic> XicCstSPtr;
typedef std::shared_ptr<Xic> XicSPtr;

class MsRunXic;

class PMSPP_LIB_DECL Xic : public Trace
{
  public:
  Xic();
  Xic(const Trace & other);
  virtual ~Xic();


  XicCstSPtr makeXicCstSPtr() const;

  XicSPtr makeXicSPtr() const;

  /** \brief get the number of MS measurement between 2 retention times on this
   * xic */
  unsigned int getMsPointDistance(pappso_double rt,
                                  pappso_double rt_other) const;

  void debugPrintValues() const;


  /** @brief sort peaks by retention time
   */
  void sortByRetentionTime();

  /** @brief get the DataPoint at the given retention time */
  const DataPoint &atRetentionTime(pappso_double rt) const;
};


} // namespace pappso
