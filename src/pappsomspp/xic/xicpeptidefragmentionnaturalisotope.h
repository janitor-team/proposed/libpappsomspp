/*
 * *******************************************************************************
 * * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 * *
 * * This file is part of MassChroqPRM.
 * *
 * *     MassChroqPRM is free software: you can redistribute it and/or modify
 * *     it under the terms of the GNU General Public License as published by
 * *     the Free Software Foundation, either version 3 of the License, or
 * *     (at your option) any later version.
 * *
 * *     MassChroqPRM is distributed in the hope that it will be useful,
 * *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 * *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * *     GNU General Public License for more details.
 * *
 * *     You should have received a copy of the GNU General Public License
 * *     along with MassChroqPRM.  If not, see <http://www.gnu.org/licenses/>.
 * *
 * * Contributors:
 * *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 * implementation
 * ******************************************************************************/


#pragma once


#include "xicpeptidefragmention.h"

#include "../peptide/peptidenaturalisotopeaverage.h"

namespace pappso
{
class XicPeptideFragmentIonNaturalIsotope : public QualifiedXic
{
  private:
  PeptideNaturalIsotopeAverageSp msp_naturalIsotopeAverage;
  PeptideFragmentIonSp msp_peptideFragmentIon;

  public:
  XicPeptideFragmentIonNaturalIsotope(
    const MsRunId &msrun_id,
    const PeptideNaturalIsotopeAverageSp &naturalIsotopeAverageSp,
    const PeptideFragmentIonSp &peptideFragmentIonSp);

  XicPeptideFragmentIonNaturalIsotope(
    const XicPeptideFragmentIonNaturalIsotope &other);

  unsigned int
  getCharge() const
  {
    return msp_naturalIsotopeAverage.get()->getCharge();
  };

  const PeptideNaturalIsotopeAverageSp &
  getPeptideNaturalIsotopeAverageSp() const
  {
    return msp_naturalIsotopeAverage;
  };

  const PeptideFragmentIonSp &
  getPeptideFragmentIonSp() const
  {
    return msp_peptideFragmentIon;
  };

  ~XicPeptideFragmentIonNaturalIsotope();
};

} // namespace pappso
