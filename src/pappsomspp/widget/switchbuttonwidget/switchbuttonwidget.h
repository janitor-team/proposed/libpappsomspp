/**
 * \file pappsomspp/widget/switchbuttonwidget/switchbuttonwidget.h
 * \date 26/07/2021
 * \author Thomas Renne
 * \brief widget to transform a push button to a switch button
 */


/*******************************************************************************
 * Copyright (c) 2021 Thomas Renne <thomas.renne@e.email>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Thomas Renne <thomas.renne@e.email> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <QPushButton>
#include "../../exportinmportconfig.h"

namespace pappso
{

class PMSPP_LIB_DECL SwitchWidget : public QPushButton
{
  Q_OBJECT
  public:
  SwitchWidget(QWidget *parent = 0);
  ~SwitchWidget();

  /** @brief get the state of the switch button (on/off)
   * @return boolean true or false
   * */
  bool getSwitchValue();

  /** @brief set the state of the switch button (on/off)
   * @param switch_value boolean true or false
   * */
  void setSwitchValue(bool switch_value);

  private slots:
  void updateSwitchValue();

  private:
  bool m_switchButtonValue = false;
};
} // namespace pappso
