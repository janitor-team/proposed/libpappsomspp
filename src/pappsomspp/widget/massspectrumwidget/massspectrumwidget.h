/**
 * \file pappsomspp/widget/massspectrumwidget/massspectrumwidget.h
 * \date 22/12/2017
 * \author Olivier Langella
 * \brief plot a sectrum and annotate with peptide
 */


/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/


#pragma once


#include "../../massspectrum/qualifiedmassspectrum.h"
#include "../../peptide/peptide.h"
#include "../../peptide/peptidefragmentionlistbase.h"
#include "../../peptide/peptidenaturalisotopeaverage.h"
#include "../../psm/peptideisotopespectrummatch.h"
#include "../graphicdevicewidget.h"
#include "qcpspectrum.h"

namespace pappso
{

class PMSPP_LIB_DECL MassSpectrumWidget : public GraphicDeviceWidget
{
  Q_OBJECT
  public:
  MassSpectrumWidget(QWidget *parent = 0);
  ~MassSpectrumWidget();

  void setQualifiedMassSpectrum(const QualifiedMassSpectrum &spectrum);
  void setMsLevel(unsigned int ms_level);
  void setMassSpectrumCstSPtr(const MassSpectrumCstSPtr &spectrum);
  void setPeptideSp(const PeptideSp &peptide_sp);
  void plot();
  void rescale();

  void setPeptideCharge(unsigned int parent_ion_charge);
  void setMaximumIsotopeNumber(unsigned int max_isotope_number);
  void setMaximumIsotopeRank(unsigned int max_isotope_rank);
  // void setIsolationWindow(PrecisionPtr precision);
  void setIonList(const std::list<PeptideIon> &ion_list);
  void setMs1Precision(PrecisionPtr precision);
  void setMs2Precision(PrecisionPtr precision);

  bool savePdf(const QString &fileName, int width = 0, int height = 0);
  void toQPaintDevice(QPaintDevice *device, const QSize &size) override;
  void highlightPrecursorPeaks();
  // void setIsotopeMassList(std::vector<pappso::PeptideNaturalIsotopeAverageSp>
  // & isotope_mass_list);


  signals:
  void mzChanged(double mz) const;
  void peakChanged(pappso::DataPointCstSPtr peak_match) const;
  void ionChanged(pappso::PeakIonIsotopeMatchCstSPtr ion) const;

  protected:
  friend class QCPSpectrum;
  void mzChangeEvent(pappso_double mz) const;
  void peakChangeEvent(const DataPoint *p_peak_match);

  private:
  void peptideAnnotate();
  void setVisibleMassDelta(bool visible);
  void clearData();
  void computeIsotopeMassList();

  private:
  unsigned int _tag_nmost_intense  = 10;
  unsigned int _max_isotope_number = 0;
  unsigned int _max_isotope_rank   = 1;
  MassSpectrumCstSPtr _spectrum_sp;
  PrecisionPtr _p_ms1_precision = PrecisionFactory::getDaltonInstance(0.1);
  PrecisionPtr _p_ms2_precision = PrecisionFactory::getDaltonInstance(0.5);
  unsigned int _peptide_charge  = 3;
  unsigned int _ms_level;
  PeptideSp _peptide_sp;
  std::list<PeptideIon> _ion_list;

  std::list<PeakIonIsotopeMatch> _peak_ion_isotope_match_list;

  /** @brief list of isotope precursors
   */
  std::vector<pappso::PeptideNaturalIsotopeAverageSp> _isotope_mass_list;

  QCPSpectrum *_custom_plot = nullptr;
  bool _is_visible_mass_delta;

  const DataPoint *_p_mouse_peak = nullptr;
};


} // namespace pappso
