/**
 * \file pappsomspp/widget/httpbutton/httpbutton.cpp
 * \date 20/04/2021
 * \author Olivier Langella
 * \brief push button to trigger web browser on URL
 */


/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "httpbutton.h"
#include "../../utils.h"

#include <QDebug>
#include <QDesktopServices>

using namespace pappso;

HttpButton::HttpButton(QWidget *parent) : QPushButton(parent)
{
  qDebug();
}

pappso::HttpButton::~HttpButton()
{
}

void
pappso::HttpButton::mousePressEvent(QMouseEvent *e)
{
  if(!text().isEmpty())
    {
      if(text().startsWith("MOD:"))
        {
          QDesktopServices::openUrl(getOlsUrl(this->text()));
          // qDebug() << getOlsUrl(this->text());
        }
      else if(text().startsWith("PubMed:"))
        {
          QDesktopServices::openUrl(getPubMedUrl(this->text()));
          // qDebug() << getPubMedUrl(this->text());
        }
      else if(text().startsWith("RESID:"))
        {
          QDesktopServices::openUrl(getRESIDUrl(this->text()));
          // qDebug() << getPubMedUrl(this->text());
        }
      else if(text().startsWith("ChEBI:"))
        {
          QDesktopServices::openUrl(getChEBIUrl(this->text()));
          // qDebug() << getPubMedUrl(this->text());
        }
      else if(text().startsWith("Unimod:"))
        {
          QDesktopServices::openUrl(getUnimodUrl(this->text()));
          // qDebug() << getPubMedUrl(this->text());
        }
      else
        {
          qDebug() << "unknown" << this->text();
        }
    }
  QPushButton::mousePressEvent(e);
}

void
pappso::HttpButton::setText(const QString &text)
{
  QPushButton::setText(text);
}


const QUrl
HttpButton::getOlsUrl(QString psimod_accession)
{

  QString iri(QString("http://purl.obolibrary.org/obo/%1")
                .arg(psimod_accession.replace(":", "_")));
  QUrl url(
    QString("http://www.ebi.ac.uk/ols/ontologies/mod/terms?iri=%1").arg(iri));
  return url;
}

const QUrl
HttpButton::getPubMedUrl(QString accession)
{
  // https://pubmed.ncbi.nlm.nih.gov/18688235/
  QUrl url(QString("https://pubmed.ncbi.nlm.nih.gov/%1/")
             .arg(accession.replace("PubMed:", "")));
  return url;
}


const QUrl
HttpButton::getRESIDUrl(QString accession)
{
  // https://annotation.dbi.udel.edu/cgi-bin/resid?id=AA0470
  QUrl url(QString("https://annotation.dbi.udel.edu/cgi-bin/resid?id=%1")
             .arg(accession.replace("RESID:", "")));
  return url;
}


const QUrl
HttpButton::getChEBIUrl(QString accession)
{
  // ChEBI:37629
  // https://www.ebi.ac.uk/chebi/searchId.do?chebiId=37628
  QUrl url(QString("https://www.ebi.ac.uk/chebi/searchId.do?chebiId=%1")
             .arg(accession.replace("ChEBI:", "")));
  return url;
}


const QUrl
HttpButton::getUnimodUrl(QString accession)
{
  // Unimod:23
  // http://www.unimod.org/modifications_view.php?editid1=23
  QUrl url(QString("http://www.unimod.org/modifications_view.php?editid1=%1")
             .arg(accession.replace("Unimod:", "")));
  return url;
}
