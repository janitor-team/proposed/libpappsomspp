/**
 * \file pappsomspp/widget/obo/obochooserwidget/obochooserwidget.cpp
 * \date 19/04/2021
 * \author Olivier Langella
 * \brief display obo term list and choose items
 */


/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "obochooserwidget.h"

#include "ui_uiobochooserwidget.h"
#include "../../../exception/exceptionnotfound.h"
#include <QDebug>

using namespace pappso;

OboChooserWidget::OboChooserWidget(QWidget *parent)
  : QWidget(parent), ui(new Ui::OboChooserWidgetForm)
{
  qDebug();
  ui->setupUi(this);


  connect(ui->oboListWidget,
          &OboListWidget::oboTermChanged,
          ui->oboTermForm,
          &OboTermForm::displayOboTerm);
}

pappso::OboChooserWidget::~OboChooserWidget()
{
  delete ui;
}

bool
pappso::OboChooserWidget::isOboTermSelected() const
{
  return ui->oboTermForm->isOboTerm();
}

const pappso::OboPsiModTerm &
pappso::OboChooserWidget::getOboPsiModTermSelected() const
{
  if(ui->oboTermForm->isOboTerm())
    {
    }
  else
    {
      throw pappso::ExceptionNotFound(tr("OBO term not available"));
    }
  return ui->oboTermForm->getOboPsiModTerm();
}

void
pappso::OboChooserWidget::setMzTarget(double target_mz)
{
  qDebug();
  ui->oboListWidget->filterMzPrecision(target_mz,
                                       ui->oboListWidget->getPrecisionPtr());
}

void
pappso::OboChooserWidget::setPrecision(pappso::PrecisionPtr precision)
{
  ui->oboListWidget->filterMzPrecision(ui->oboListWidget->getMzTarget(),
                                       precision);
}
