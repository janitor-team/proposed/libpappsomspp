/**
 * \file pappsomspp/widget/obo/obolistwidget/obolistmodel.h
 * \date 19/04/2021
 * \author Olivier Langella
 * \brief MVC model of OBO term list
 */


/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <QStringListModel>
#include "../../../exportinmportconfig.h"
#include "../../../obo/obopsimodterm.h"
#include "../../../obo/obopsimodhandlerinterface.h"
#include <vector>


namespace pappso
{

/**
 * @todo write docs
 */
class PMSPP_LIB_DECL OboListModel : public QStringListModel
{
  public:
  /**
   * Default constructor
   */
  OboListModel(QObject *parent = nullptr);

  /**
   * Destructor
   */
  ~OboListModel();

  void loadPsiMod();


  QVariant data(const QModelIndex &index,
                int role = Qt::DisplayRole) const override;

  int rowCount(const QModelIndex &parent = QModelIndex()) const override;


  const OboPsiModTerm &getOboPsiModTerm(int row) const;

  protected:
  class OboPsiModHandler : public OboPsiModHandlerInterface
  {

    public:
    OboPsiModHandler(OboListModel *parent);
    virtual ~OboPsiModHandler();

    void setOboPsiModTerm(const OboPsiModTerm &term) override;

    private:
    pappso::OboListModel *mp_parent;
  };

  std::vector<OboPsiModTerm> m_oboPsiModTermList;
};
} // namespace pappso
