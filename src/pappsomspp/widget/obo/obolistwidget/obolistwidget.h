/**
 * \file pappsomspp/widget/obo/obolistwidget/obolistwidget.h
 * \date 17/04/2021
 * \author Olivier Langella
 * \brief handles a list of obo term, select and click
 */


/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once


#include <QWidget>
#include "../../../exportinmportconfig.h"
#include "../../../precision.h"
#include "obolistmodel.h"
#include <QItemSelection>

namespace Ui
{
class OboListWidgetForm;
}

namespace pappso
{

class OboListProxyModel;

class PMSPP_LIB_DECL OboListWidget : public QWidget
{
  Q_OBJECT
  public:
  /**
   * Default constructor
   */
  explicit OboListWidget(QWidget *parent = nullptr);

  /**
   * Destructor
   */
  ~OboListWidget();

  void filterMzPrecision(double target_mz, PrecisionPtr precision);

  /** @brief get the current mz value used to filter term list
   */
  double getMzTarget() const;

  /** @brief get the current precision used to filter term list
   */
  PrecisionPtr getPrecisionPtr() const;

  signals:
  void oboTermChanged(OboPsiModTerm oboTerm) const;

  private slots:
  void onSelectionChanged(const QItemSelection &selected,
                          const QItemSelection &deselected);
  void onFilterChanged();

  void onFilterChanged(pappso::PrecisionPtr precision);


  private:
  Ui::OboListWidgetForm *ui;

  OboListModel *mpa_oboListModel           = nullptr;
  OboListProxyModel *mpa_oboListProxyModel = nullptr;
};

} // namespace pappso

Q_DECLARE_METATYPE(pappso::OboPsiModTerm);
