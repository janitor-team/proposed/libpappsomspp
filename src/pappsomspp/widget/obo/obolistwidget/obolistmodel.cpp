/**
 * \file pappsomspp/widget/obo/obolistwidget/obolistmodel.h
 * \date 19/04/2021
 * \author Olivier Langella
 * \brief MVC model of OBO term list
 */


/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "obolistmodel.h"
#include "../../../obo/obopsimod.h"
#include "obolistwidget.h"
#include "../../../exception/exceptionnotfound.h"

#include <QColor>
#include <QDebug>

using namespace pappso;

OboListModel::OboListModel(QObject *parent) : QStringListModel(parent)
{
}

OboListModel::~OboListModel()
{
}

pappso::OboListModel::OboPsiModHandler::OboPsiModHandler(
  pappso::OboListModel *parent)
{
  mp_parent = parent;
}

pappso::OboListModel::OboPsiModHandler::~OboPsiModHandler()
{
}

void
pappso::OboListModel::OboPsiModHandler::setOboPsiModTerm(
  const pappso::OboPsiModTerm &term)
{
  mp_parent->m_oboPsiModTermList.push_back(term);
}


void
OboListModel::loadPsiMod()
{
  OboPsiModHandler handler(this);
  OboPsiMod reader(handler);

  qDebug() << m_oboPsiModTermList.size();
}

QVariant
pappso::OboListModel::data(const QModelIndex &index, int role) const
{

  // generate a log message when this method gets called
  std::size_t row = index.row();
  // int col = index.column();
  // qDebug() << QString("row %1, col%2, role %3")
  //         .arg(row).arg(col).arg(role);
  if(row < m_oboPsiModTermList.size())
    {

      switch(role)
        {
          case Qt::CheckStateRole:

            break;
          case Qt::BackgroundRole:
            // return QVariant(QColor("grey"));
            break;
          case Qt::SizeHintRole:
            // qDebug() << "ProteinTableModel::headerData " <<
            // ProteinTableModel::getColumnWidth(section);
            // return QSize(PeptideTableModel::getColumnWidth(col), 30);
            break;
          case Qt::ToolTipRole:
            break;
          case Qt::DisplayRole:
            return QVariant(QString("%1  %2 %3")
                              .arg(m_oboPsiModTermList[row].m_accession)
                              .arg(m_oboPsiModTermList[row].m_diffMono)
                              .arg(m_oboPsiModTermList[row].m_name));
            break;
          case Qt::UserRole:
            QVariant value;
            value.setValue(m_oboPsiModTermList[row]);
            return value;
            break;
        }
    }
  return QVariant();
}

int
pappso::OboListModel::rowCount(const QModelIndex &parent [[maybe_unused]]) const
{
  return (int)m_oboPsiModTermList.size();
}

const pappso::OboPsiModTerm &
pappso::OboListModel::getOboPsiModTerm(int row) const
{
  if(row < (int) m_oboPsiModTermList.size())
    {
      return m_oboPsiModTermList[row];
    }
  else
    {
      throw pappso::ExceptionNotFound(tr("OBO term not found"));
    }
}
