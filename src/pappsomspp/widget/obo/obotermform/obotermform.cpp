/**
 * \file pappsomspp/widget/obo/obotermform/obotermform.cpp
 * \date 20/04/2021
 * \author Olivier Langella
 * \brief display an obo term form
 */


/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "obotermform.h"

#include "ui_uiobotermform.h"
#include <QDebug>
#include "../../../exception/exceptionnotfound.h"

using namespace pappso;

//"Oxidation of methionine to methionine sulfoxide with neutral loss of CH3SOH."
//[PubMed:18688235, PubMed:9004526]
QRegularExpression OboTermForm::m_findExternalLinks("^(.*)\\s\\[(.*)\\]$");


OboTermForm::OboTermForm(QWidget *parent)
  : QWidget(parent), ui(new Ui::OboTermForm)
{
  qDebug();
  ui->setupUi(this);

  OboPsiModTerm empty;
  displayOboTerm(empty);
}

pappso::OboTermForm::~OboTermForm()
{
  delete ui;
}

void
pappso::OboTermForm::displayOboTerm(pappso::OboPsiModTerm oboTerm)
{
  qDebug() << oboTerm.m_accession;
  ui->accessionHttpButton->setText(oboTerm.m_accession);
  ui->nameLabel->setText(oboTerm.m_name);
  ui->definitionLabel->setText(oboTerm.m_definition);
  ui->diffFormulaLabel->setText(oboTerm.m_diffFormula);
  if(oboTerm.isValid())
    {
      ui->diffMonoLabel->setText(QString::number(oboTerm.m_diffMono, 'f', 4));
      parseDefinitionLabel();
    }
  else
    {
      ui->diffMonoLabel->setText("");
    }
  ui->originLabel->setText(oboTerm.m_origin);
  ui->psiModLabel->setText(oboTerm.m_psiModLabel);


  m_oboPsiModTerm = oboTerm;
}

const pappso::OboPsiModTerm &
pappso::OboTermForm::getOboPsiModTerm() const
{
  if(isOboTerm())
    {
    }
  else
    {
      throw pappso::ExceptionNotFound(tr("OBO term not available"));
    }
  return m_oboPsiModTerm;
}

bool
pappso::OboTermForm::isOboTerm() const
{
  return m_oboPsiModTerm.isValid();
}

void
pappso::OboTermForm::parseDefinitionLabel()
{
  QString label = ui->definitionLabel->text();
  //"Oxidation of methionine to methionine sulfoxide with neutral loss of
  // CH3SOH." [PubMed:18688235, PubMed:9004526]

  QLayoutItem *child;
  while((child = ui->xrefScrollAreaLayout->takeAt(0)) != 0)
    {
      if(child->widget() != NULL)
        {
          delete(child->widget());
        }
      delete child;
    }

  ui->xrefScrollArea->hide();
  QRegularExpressionMatch match = m_findExternalLinks.match(label);
  if(match.hasMatch())
    {
      QStringList pline = match.capturedTexts();
      ui->definitionLabel->setText(pline[1]);

      QStringList list_xref = pline[2].split(",");

      ui->xrefScrollArea->show();
      for(auto xref : list_xref)
        {
          HttpButton *newButton = new HttpButton;
          newButton->setText(xref.trimmed());
          newButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
          ui->xrefScrollAreaLayout->addWidget(newButton);
        }
    }
}
