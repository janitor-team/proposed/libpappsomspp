// Copyright 2021 Filippo Rusconi
// GPLv3+

#pragma once


/////////////////////// StdLib includes


/////////////////////// Qt includes
#include <QString>


/////////////////////// Local includes
#include "../../exportinmportconfig.h"
#include "baseplotcontext.h"

namespace pappso
{

class PMSPP_LIB_DECL MassSpecTracePlotContext : public BasePlotContext
{
  public:
  MassSpecTracePlotContext();
  MassSpecTracePlotContext(const MassSpecTracePlotContext &other);

  virtual ~MassSpecTracePlotContext();

  MassSpecTracePlotContext &operator=(const MassSpecTracePlotContext &other);

  BasePlotContext m_baseContext;

  int m_lastZ                 = -1;
  double m_lastMz             = std::numeric_limits<double>::min();
  double m_lastTicIntensity   = std::numeric_limits<double>::min();
  double m_lastMr             = std::numeric_limits<double>::min();
  double m_lastResolvingPower = std::numeric_limits<double>::min();

  QString toString() const;
};

} // namespace pappso

