/* This code comes right from the msXpertSuite software project.
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

/////////////////////// StdLib includes


/////////////////////// Qt includes
#include <QObject>
#include <QString>
#include <QWidget>
#include <QBrush>
#include <QColor>
#include <QVector>


/////////////////////// QCustomPlot
#include <qcustomplot.h>


/////////////////////// pappsomspp includes


/////////////////////// Local includes
#include "../../exportinmportconfig.h"
#include "basetraceplotwidget.h"
#include "massspectraceplotcontext.h"


namespace pappso
{


class PMSPP_LIB_DECL MassSpecTracePlotWidget : public BaseTracePlotWidget
{
  Q_OBJECT

  public:
  explicit MassSpecTracePlotWidget(QWidget *parent = 0);
  explicit MassSpecTracePlotWidget(QWidget *parent,
                                   const QString &x_axis_label,
                                   const QString &y_axis_label);

  virtual ~MassSpecTracePlotWidget();

  /******* Mass spectrum-specific calculations *******/
  /******* Mass spectrum-specific calculations *******/
  void setChargeMinimalFractionalPart(double charge_fractional_part);
  double getChargeMinimalFractionalPart() const;

  void setChargeStateEnvelopePeakSpan(int interval);
  int getChargeStateEnvelopePeakSpan() const;

  bool deconvolute();
  bool deconvoluteIsotopicCluster();
  bool deconvoluteChargedState(int span = 1);
  bool computeResolvingPower();
  /******* Mass spectrum-specific calculations *******/


  /******* Mouse and keyboard event handlers *******/
  /******* Mouse and keyboard event handlers *******/
  virtual void keyPressEvent(QKeyEvent *event) override;
  virtual void keyReleaseEvent(QKeyEvent *event) override;

  virtual void mouseMoveHandler(QMouseEvent *event) override;

  virtual void mousePressHandler(QMouseEvent *event) override;
  virtual void mouseReleaseHandler(QMouseEvent *event) override;

  virtual void mouseMoveHandlerNotDraggingCursor() override;
  virtual void mouseMoveHandlerDraggingCursor() override;
  /******* Mouse and keyboard event handlers *******/

  const MassSpecTracePlotContext &refreshBaseContext() const;

  signals:

  // Here we have signals that are specific of the mass spectrum-oriented
  // version of the plot widget.

  void keyPressEventSignal(const MassSpecTracePlotContext &context);

  void massDeconvolutionSignal(const MassSpecTracePlotContext &context);
  void resolvingPowerComputationSignal(const MassSpecTracePlotContext &context);

  void mouseReleaseEventSignal(const MassSpecTracePlotContext &context);

  void newKeyPressEventSignal(pappso::DataPoint context);
  void testKeyPressEventSignal(pappso::MassSpecTracePlotContext context);


  protected:
  mutable MassSpecTracePlotContext m_context;

  // This value is the tolerance on the fractional part of the charge value that
  // is computed by the deconvolution. If the z value that is deconvoluted is,
  // for example, 2.980, and m_chargeFractionalPartTolerance = 0.990, then no z
  // value will be displayed: we are not reaching an integer near enough for the
  // z value to be considered valid.
  double m_chargeMinimalFractionalPart = 0.990;

  // When performing deconvolutions based on the distance between peaks belong
  // to the same charge state envelope, this value indicates the distance
  // between the peaks that are used for the calculation. When the peaks are
  // consecutive, the distance is 1. If there is one peak in between, the
  // distance is 2.
  int m_chargeStateEnvelopePeakSpan = 1;
};


} // namespace pappso


Q_DECLARE_METATYPE(pappso::MassSpecTracePlotContext);
extern int massSpecTracePlotContextMetaTypeId;

Q_DECLARE_METATYPE(pappso::MassSpecTracePlotContext *);
extern int massSpecTracePlotContextPtrMetaTypeId;
