// Copyright 2021 Filippo Rusconi
// GPLv3+

/////////////////////// StdLib includes


/////////////////////// Qt includes
#include <QString>


/////////////////////// Local includes
#include "../../exportinmportconfig.h"
#include "massspectraceplotcontext.h"

namespace pappso
{


MassSpecTracePlotContext::MassSpecTracePlotContext()
{
}


MassSpecTracePlotContext::MassSpecTracePlotContext(
  const MassSpecTracePlotContext &other)
  : BasePlotContext(other.m_baseContext),
    m_lastZ(other.m_lastZ),
    m_lastMz(other.m_lastMz),
    m_lastTicIntensity(other.m_lastTicIntensity),
    m_lastMr(other.m_lastMr),
    m_lastResolvingPower(other.m_lastResolvingPower)
{
}


MassSpecTracePlotContext::~MassSpecTracePlotContext()
{
}


MassSpecTracePlotContext &
MassSpecTracePlotContext::operator=(const MassSpecTracePlotContext &other)
{
  if(this == &other)
    return *this;

  m_baseContext = other.m_baseContext;

  m_lastZ              = other.m_lastZ;
  m_lastMz             = other.m_lastMz;
  m_lastTicIntensity   = other.m_lastTicIntensity;
  m_lastMr             = other.m_lastMr;
  m_lastResolvingPower = other.m_lastResolvingPower;

  return *this;
}


QString
MassSpecTracePlotContext::toString() const
{
  QString text("Base context:\n");

  text += m_baseContext.toString();

  text += "\n";

  text += QString("last z: %1").arg(m_lastZ);
  text += QString("last m/z: %1").arg(m_lastMz, 0, 'f', 6);
  text += QString("last TIC intensity: %1").arg(m_lastTicIntensity, 0, 'g', 0);
  text += QString("last Mr: %1").arg(m_lastMr, 0, 'f', 6);
  text +=
    QString("last resolving power: %1").arg(m_lastResolvingPower, 0, 'g', 0);

  text += "\n";

  return text;
}


} // namespace pappso

