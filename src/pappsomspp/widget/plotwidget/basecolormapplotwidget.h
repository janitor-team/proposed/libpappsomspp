/* This code comes right from the msXpertSuite software project.
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

/////////////////////// StdLib includes


/////////////////////// Qt includes
#include <QObject>
#include <QString>
#include <QWidget>
#include <QBrush>
#include <QColor>
#include <QVector>


/////////////////////// QCustomPlot
#include <cstddef>
#include <qcustomplot.h>


/////////////////////// Local includes
#include "../../exportinmportconfig.h"
#include "baseplotwidget.h"
#include "colormapplotconfig.h"
#include "../../trace/trace.h"
#include "../../vendors/tims/timsframe.h"


namespace pappso
{


class BaseColorMapPlotWidget;

typedef std::shared_ptr<BaseColorMapPlotWidget> BaseColorMapPlotWidgetSPtr;
typedef std::shared_ptr<const BaseColorMapPlotWidget>
  BaseColorMapPlotWidgetCstSPtr;

class PMSPP_LIB_DECL BaseColorMapPlotWidget : public BasePlotWidget
{
  Q_OBJECT;

  public:
  explicit BaseColorMapPlotWidget(QWidget *parent);
  explicit BaseColorMapPlotWidget(QWidget *parent,
                                  const QString &x_axis_label,
                                  const QString &y_axis_label);

  virtual ~BaseColorMapPlotWidget();

  virtual void
  setColorMapPlotConfig(const ColorMapPlotConfig &color_map_config);
  virtual const ColorMapPlotConfig &getColorMapPlotConfig();
  const ColorMapPlotConfig *getOrigColorMapPlotConfig();

  virtual QCPColorMap *addColorMap(
    std::shared_ptr<std::map<double, MapTrace>> double_map_trace_map_sp,
    const ColorMapPlotConfig color_map_plot_config,
    const QColor &color);

  virtual QCPColorMap *
  addColorMap(const TimsFrame &tims_frame,
              const ColorMapPlotConfig color_map_plot_config,
              const QColor &color);

  virtual void transposeAxes();

  // Change the scale of the intensity to log10 (color, z virtual axis)
  virtual void zAxisScaleToLog10();
  virtual void zAxisFilterLowPassPercentage(double threshold_percentage);
  
  /** @brief fix maximum value for the intensity
   */
  virtual void zAxisFilterLowPassThreshold(double threshold);
  
  virtual void zAxisFilterHighPassPercentage(double threshold_percentage);

  virtual void zAxisDataResetToOriginal();

  DataKind xAxisDataKind() const;
  DataKind yAxisDataKind() const;

  AxisScale axisScale(Axis axis) const;
  AxisScale xAxisScale() const;
  AxisScale yAxisScale() const;
  AxisScale zAxisScale() const;

  virtual void setPlottingColor(QCPAbstractPlottable *plottable_p,
                                const QColor &new_color) override;
  virtual QColor getPlottingColor(int index = 0) const override;

  void dataTo3ColString(QString &data_string);
  void dataToMatrixString(QString &data_string, bool detailed = false);

  void currentXaxisRangeIndices(int &lower, int &upper);
  void currentYaxisRangeIndices(int &lower, int &upper);

  signals:

  protected:
  QCPColorMapData *mpa_origColorMapData = nullptr;

  ColorMapPlotConfig m_colorMapPlotConfig;
  ColorMapPlotConfig *mpa_origColorMapPlotConfig = nullptr;
};


} // namespace pappso
