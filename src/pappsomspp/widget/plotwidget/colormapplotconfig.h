// Copyright Filippo Rusconi, GPLv3+

/////////////////////// StdLib includes
#include <limits>


/////////////////////// Qt includes
#include <QString>


/////////////////////// Local includes
#include "../../types.h"
#include "../../utils.h"
#include "../../exportinmportconfig.h"


#pragma once

namespace pappso
{

struct PMSPP_LIB_DECL ColorMapPlotConfig
{
  DataKind xAxisDataKind = DataKind::unset;
  DataKind yAxisDataKind = DataKind::unset;

  AxisScale xAxisScale = AxisScale::orig;
  AxisScale yAxisScale = AxisScale::orig;
  AxisScale zAxisScale = AxisScale::orig;

  std::size_t keyCellCount = 0;
  std::size_t mzCellCount  = 0;

  double lastMinZFilterThresholdPercentage = 0.0;
  double lastMaxZFilterThresholdPercentage = 0.0;

  double minKeyValue = std::numeric_limits<double>::max();
  double maxKeyValue = std::numeric_limits<double>::min();

  double minMzValue = std::numeric_limits<double>::max();
  double maxMzValue = std::numeric_limits<double>::max();

  double origMinZValue = std::numeric_limits<double>::max();
  double lastMinZValue     = std::numeric_limits<double>::max();

  double origMaxZValue = std::numeric_limits<double>::min();
  double lastMaxZValue     = std::numeric_limits<double>::min();

  ColorMapPlotConfig();

  ColorMapPlotConfig(const ColorMapPlotConfig &other);

  ColorMapPlotConfig(DataKind x_axis_data_kind,
                     DataKind y_axis_data_kind,

                     AxisScale x_axis_scale,
                     AxisScale y_axis_scale,
                     AxisScale z_axis_scale,

                     std::size_t key_cell_count,
                     std::size_t mz_cell_count,

                     double min_key_value,
                     double max_key_value,

                     double min_mz_value,
                     double max_mz_value,

                     double orig_min_z_value,
                     double orig_max_z_value);

  ColorMapPlotConfig &operator=(const ColorMapPlotConfig &other);

  void setOrigMinZValue(double value);
  void setOrigAndLastMinZValue(double value);

  void setOrigMaxZValue(double value);
  void setOrigAndLastMaxZValue(double value);

  QString toString() const;
};


} // namespace pappso
