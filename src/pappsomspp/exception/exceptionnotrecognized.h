/**
 * \file pappsomspp/exception/exceptionnotrecognized.h
 * \date 17/11/2020
 * \author Olivier Langella
 * \brief excetion to use when an item type is not recognized (file format,
 * object type...)
 */

/*******************************************************************************
 * Copyright (c) 2020 Olivier Langella
 *<olivier.langella@universite-paris-saclay.fr>
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/
#pragma once

#include "../pappsoexception.h"

namespace pappso
{

/** @brief excetion to use when an item type is not recognized
 *
 * item types can be files, objects...
 */

class ExceptionNotRecognized : public PappsoException
{
  public:
  ExceptionNotRecognized(const QString &message) throw()
    : PappsoException(message)
  {
  }


  virtual QException *
  clone() const override
  {
    return new ExceptionNotRecognized(*this);
  }
};
} // namespace pappso
