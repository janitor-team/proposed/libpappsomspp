/**
 * \file pappsomspp/vendors/tims/timsms2centroidfilter.cpp
 * \date 27/09/2019
 * \author Olivier Langella
 * \brief special process for tims MS2 spectrum (centroid like)
 */

/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "timsms2centroidfilter.h"

using namespace pappso;

TimsMs2CentroidFilter::TimsMs2CentroidFilter()
{
}

TimsMs2CentroidFilter::~TimsMs2CentroidFilter()
{
}

TimsMs2CentroidFilter::TimsMs2CentroidFilter([
  [maybe_unused]] const TimsMs2CentroidFilter &other)
{
}


MassSpectrum &
TimsMs2CentroidFilter::filter(MassSpectrum &data_points) const
{
  return data_points;
}
