/**
 * \file pappsomspp/vendors/tims/mzcalibration/mzcalibratiostore.cpp
 * \date 12/11/2020
 * \author Olivier Langella
 * \brief store a collection of MzCalibration models
 */


/*******************************************************************************
 * Copyright (c) 2020 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "mzcalibrationstore.h"
#include <QVariant>
#include "mzcalibrationmodel1.h"
#include "../../../pappsoexception.h"
#include <QDebug>

namespace pappso
{
MzCalibrationStore::MzCalibrationStore()
{
}

MzCalibrationStore::~MzCalibrationStore()
{
}

MzCalibrationInterfaceSPtr
MzCalibrationStore::getInstance(double T1_frame,
                                double T2_frame,
                                const QSqlRecord &mz_calibration_record)
{
  //qDebug();
  MzCalibrationInterfaceSPtr msp_mzCalibration = nullptr;

  QString mz_calibration_key =
    QString("%1-%2-%3-%4")
      .arg(mz_calibration_record.value("Id").toInt())
      .arg(mz_calibration_record.value("ModelType").toInt())
      .arg(QString::number(T1_frame, 'g', 10))
      .arg(QString::number(T2_frame, 'g', 10));


  auto itmap = m_mapMzCalibrationSPtr.find(mz_calibration_key);
  if(itmap != m_mapMzCalibrationSPtr.end())
    {
      //qDebug() << mz_calibration_key << " calibration object found";
      msp_mzCalibration = itmap->second;
    }
  else
    {
      if(mz_calibration_record.value("ModelType").toInt() == 1)
        {
          msp_mzCalibration = std::make_shared<MzCalibrationModel1>(
            T1_frame,
            T2_frame,
            mz_calibration_record.value("DigitizerTimebase")
              .toDouble(), // MzCalibration.DigitizerTimebase
            mz_calibration_record.value("DigitizerDelay")
              .toDouble(), // MzCalibration.DigitizerDelay
            mz_calibration_record.value("C0").toDouble(), // MzCalibration.C0
            mz_calibration_record.value("C1").toDouble(), // MzCalibration.C1
            mz_calibration_record.value("C2").toDouble(), // MzCalibration.C2
            mz_calibration_record.value("C3").toDouble(), // MzCalibration.C3
            mz_calibration_record.value("C4").toDouble(), // MzCalibration.C4
            mz_calibration_record.value("T1").toDouble(),
            mz_calibration_record.value("T2").toDouble(),
            mz_calibration_record.value("dC1").toDouble(),
            mz_calibration_record.value("dC2").toDouble());
        }
      else
        {
          //qDebug();
          throw PappsoException(
            QObject::tr(
              "ERROR in MzCalibrationStore::getInstance : MzCalibration "
              "ModelType \"%1\" not available")
              .arg(mz_calibration_record.value("ModelType").toInt()));
        }

      m_mapMzCalibrationSPtr.insert(
        std::pair<QString, MzCalibrationInterfaceSPtr>(mz_calibration_key,
                                                       msp_mzCalibration));
    }

  if(msp_mzCalibration == nullptr)
    {
      //qDebug();
      throw PappsoException(QObject::tr(
        "ERROR in MzCalibrationStore::getInstance MzCalibration is NULL"));
    }

  //qDebug();
  return (msp_mzCalibration);
}

} // namespace pappso
