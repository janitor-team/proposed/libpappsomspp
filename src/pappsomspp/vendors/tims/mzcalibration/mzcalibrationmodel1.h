/**
 * \file pappsomspp/vendors/tims/mzcalibration/mzcalibrationmodel1.h
 * \date 11/11/2020
 * \author Olivier Langella
 * \brief implement Bruker's model type 1 formula to compute m/z
 */

/*******************************************************************************
 * Copyright (c) 2020 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "mzcalibrationinterface.h"


#pragma once


namespace pappso
{

/**
 * @todo write docs
 */
class MzCalibrationModel1 : public MzCalibrationInterface
{
  public:
  /**
   * Default constructor
   */
  MzCalibrationModel1(double T1_frame,
                      double T2_frame,
                      double digitizerTimebase,
                      double digitizerDelay,
                      double C0,
                      double C1,
                      double C2,
                      double C3,
                      double C4,
                      double T1_ref,
                      double T2_ref,
                      double dC1,
                      double dC2);


  /**
   * Destructor
   */
  virtual ~MzCalibrationModel1();

  virtual double getMzFromTofIndex(quint32 tof_index) override;

  virtual quint32 getTofIndexFromMz(double mz) override;

  private:
  // double m_arrMasses[600000] = {0};
};


class MzCalibrationModel1Cached : public MzCalibrationModel1
{
  public:
  /**
   * Default constructor
   */
  MzCalibrationModel1Cached(double T1_frame,
                            double T2_frame,
                            double digitizerTimebase,
                            double digitizerDelay,
                            double C0,
                            double C1,
                            double C2,
                            double C3,
                            double C4,
                            double T1_ref,
                            double T2_ref,
                            double dC1,
                            double dC2);


  /**
   * Destructor
   */
  virtual ~MzCalibrationModel1Cached();

  virtual double getMzFromTofIndex(quint32 tof_index) override;

  private:
  double m_arrMasses[600000] = {0};
  quint32 m_max=600000;
};
} // namespace pappso
