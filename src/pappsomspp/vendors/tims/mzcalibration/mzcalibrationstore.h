/**
 * \file pappsomspp/vendors/tims/mzcalibration/mzcalibratiostore.h
 * \date 12/11/2020
 * \author Olivier Langella
 * \brief store a collection of MzCalibration models
 */

/*******************************************************************************
 * Copyright (c) 2020 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <QSqlRecord>
#include "mzcalibrationinterface.h"
#include <map>

namespace pappso
{
/**
 * @todo write docs
 */
class MzCalibrationStore
{
  public:
  /**
   * @todo write docs
   */
  MzCalibrationStore();

  /**
   * @todo write docs
   */
  virtual ~MzCalibrationStore();

  MzCalibrationInterfaceSPtr getInstance(
    double T1_frame, double T2_frame, const QSqlRecord &mzcalibration_record);

  private:
  std::map<QString, MzCalibrationInterfaceSPtr> m_mapMzCalibrationSPtr;
};
} // namespace pappso
