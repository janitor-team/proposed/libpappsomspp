/**
 * \file pappsomspp/vendors/tims/timsframe.h
 * \date 23/08/2019
 * \author Olivier Langella
 * \brief handle a single Bruker's TimsTof frame
 */

/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <memory>
#include <QByteArray>
#include <vector>
#include "timsframebase.h"
#include "../../xic/xic.h"
#include "../../msrun/xiccoord/xiccoordtims.h"

namespace pappso
{

class TimsFrame;
typedef std::shared_ptr<TimsFrame> TimsFrameSPtr;
typedef std::shared_ptr<const TimsFrame> TimsFrameCstSPtr;

class TimsBinDec;

class TimsDirectXicExtractor;

/**
 * @todo write docs
 */
class TimsFrame : public TimsFrameBase
{
  friend TimsDirectXicExtractor;

  public:
  /**
   * @param timsId tims frame id
   * @param scanNum total number of scans in this frame
   * @param p_bytes pointer on the decompressed binary buffer
   * @param len size of the decompressed binary buffer
   */
  TimsFrame(std::size_t timsId,
            quint32 scanNum,
            char *p_bytes,
            std::size_t len);
  /**
   * Copy constructor
   *
   * @param other TODO
   */
  TimsFrame(const TimsFrame &other);

  /**
   * Destructor
   */
  virtual ~TimsFrame();


  virtual std::size_t getNbrPeaks(std::size_t scanNum) const override;

  /** @brief cumulate scan list into a trace
   * @param scanNumBegin first scan to cumulate
   * @param scanNumEnd last scan to cumulate
   * @return Trace mz and intensity values
   */
  virtual Trace cumulateScanToTrace(std::size_t scanNumBegin,
                                    std::size_t scanNumEnd) const override;

  /** @brief cumulate scan list into a trace into a raw spectrum map
   * @param rawSpectrum simple map of integers to cumulate raw counts
   * @param scanNumBegin first scan to cumulate
   * @param scanNumEnd last scan to cumulate
   */
  virtual void cumulateScansInRawMap(std::map<quint32, quint32> &rawSpectrum,
                                     std::size_t scanNumBegin,
                                     std::size_t scanNumEnd) const override;

  virtual quint64 cumulateSingleScanIntensities(std::size_t scanNum) const override;
  
  virtual quint64
  cumulateScansIntensities(std::size_t scanNumBegin,
                           std::size_t scanNumEnd) const override;

  /** @brief get raw index list for one given scan
   * index are not TOF nor m/z, just index on digitizer
   */
  virtual std::vector<quint32>
  getScanIndexList(std::size_t scanNum) const override;

  /** @brief get raw intensities without transformation from one scan
   * it needs intensity normalization
   */
  virtual std::vector<quint32>
  getScanIntensities(std::size_t scanNum) const override;

  /** @brief get the mass spectrum corresponding to a scan number
   * @param scanNum the scan number to retrieve
   * */
  virtual pappso::MassSpectrumCstSPtr
  getMassSpectrumCstSPtr(std::size_t scanNum) const;

  virtual pappso::MassSpectrumSPtr
  getMassSpectrumSPtr(std::size_t scanNum) const override;


  protected:
  /** @brief constructor for binary independant tims frame
   * @param timsId tims frame identifier in the database
   * @param scanNum the total number of scans contained in this frame
   */
  TimsFrame(std::size_t timsId, quint32 scanNum);

  void extractTimsXicListInRtRange(
    std::vector<XicCoordTims *>::iterator &itXicListbegin,
    std::vector<XicCoordTims *>::iterator &itXicListend,
    XicExtractMethod method) const;


  /** @brief cumulate a scan into a map
   *
   * @param scanNum scan number 0 to (m_scanNumber-1)
   */
  virtual void cumulateScan(std::size_t scanNum,
                            std::map<quint32, quint32> &accumulate_into) const;


  /** @brief get the raw index tof_index and intensities (normalized)
   *
   * @param scanNum the scan number to extract
   * @return trace vector
   *
   */
  virtual pappso::TraceSPtr getRawTraceSPtr(std::size_t scanNum) const;


  private:
  /** @brief unshuffle data packet of tims compression type 2
   * @param src is a zstd decompressed buffer pointer
   */
  void unshufflePacket(const char *src);


  /** @brief get offset for this spectrum in the binary file
   *
   * @param scanNum scan number in the frame in the order it lies in binary
   * file, from 0 to N-1
   */

  std::size_t getScanOffset(std::size_t scanNum) const;

  private:
  struct XicComputeStructure
  {
    XicComputeStructure(const TimsFrame *fram_p,
                        const XicCoordTims &xic_struct);


    Xic *xic_ptr = nullptr;
    std::size_t mobilityIndexBegin;
    std::size_t mobilityIndexEnd;
    std::size_t mzIndexLowerBound;
    std::size_t mzIndexUpperBound;
    double tmpIntensity = 0;
  };

  protected:
  QByteArray m_timsDataFrame;
};
} // namespace pappso
