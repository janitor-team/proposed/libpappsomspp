/**
 * \file pappsomspp/vendors/tims/xicextractor/timsxicextractorinterface.h
 * \date 21/09/2019
 * \author Olivier Langella
 * \brief minimum functions to extract XICs from Tims Data
 */

/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once


#include "../../../mzrange.h"
#include "../../../xic/xic.h"
#include "../timsdata.h"
#include "../../../xicextractor/msrunxicextractorinterface.h"

namespace pappso
{

/**
 * @todo set of minimum functions to build XICs using Tims data
 */
class TimsXicExtractorInterface : public MsRunXicExtractorInterface
{
  friend TimsData;

  public:
  TimsXicExtractorInterface(MsRunReaderSPtr &msrun_reader);
  virtual ~TimsXicExtractorInterface();

  protected:
  TimsData *mp_timsData;
};

} // namespace pappso
