/**
 * \file pappsomspp/vendors/tims/timsframerecord.h
 * \date 19/6/2022
 * \author Olivier Langella
 * \brief simple structure to store SQL lite frame records
 */

/*******************************************************************************
 * Copyright (c) 2022 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once


namespace pappso
{
struct TimsFrameRecord
{
  std::size_t tims_offset         = 0;
  double accumulation_time        = 0;
  std::size_t mz_calibration_id   = 0;
  double frame_t1                 = 0;
  double frame_t2                 = 0;
  double frame_time               = 0;
  int msms_type                   = 0;
  std::size_t tims_calibration_id = 0;
};
} // namespace pappso
