/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "msrunid.h"
#include <QFileInfo>
#include <QDir>


int msRunIdMetaTypeId = qRegisterMetaType<pappso::MsRunId>("pappso::MsRunId");


int msRunIdCstSPtrMetaTypeId =
  qRegisterMetaType<pappso::MsRunIdCstSPtr>("pappso::MsRunIdCstSPtr");


namespace pappso
{

MsRunId::MsRunId()
{
}


MsRunId::MsRunId(const QString &file_name) : m_fileName(file_name)
{
  // by default, the sample name is given by the file name
  QFileInfo fileinfo(m_fileName);
  if(fileinfo.fileName() == "analysis.tdf")
    {
      m_fileName = fileinfo.absoluteDir().absolutePath();
    }
  m_sampleName = QFileInfo(m_fileName).baseName();
}


MsRunId::MsRunId(const QString &file_name, const QString &run_id)
  : MsRunId(file_name)
{
  m_runId = run_id;
}


MsRunId::MsRunId(const MsRunId &other)
  : m_fileName(other.m_fileName),
    m_runId(other.m_runId),
    m_xmlId(other.m_xmlId),
    m_sampleName(other.m_sampleName),
    m_mzFormat(other.m_mzFormat)
{
}


MsRunId::~MsRunId()
{
}


void
MsRunId::setSampleName(const QString &name)
{
  m_sampleName = name;
}


const QString &
MsRunId::getSampleName() const
{
  return m_sampleName;
}


void
MsRunId::setFileName(const QString &file_name)
{
  m_fileName = file_name;

  QFileInfo file_info(file_name);
  QString extension = file_info.suffix();

  if(m_sampleName.isEmpty())
    {
      // set sample name by default :
      m_sampleName = file_info.baseName();
    }

  m_mzFormat = MzFormat::unknown;

  if(extension.toLower() == "mzxml")
    {
      m_mzFormat = MzFormat::mzXML;
    }
  else if(extension.toLower() == "mgf")
    {
      m_mzFormat = MzFormat::MGF;
    }
  else if(extension.toLower() == "mzml")
    {
      m_mzFormat = MzFormat::mzML;
    }
}

void
MsRunId::setRunId(const QString &run_id)
{
  m_runId = run_id;
}


const QString &
MsRunId::getRunId() const
{
  return m_runId;
}


void
MsRunId::setXmlId(const QString &xml_id)
{
  m_xmlId = xml_id;
}


const QString &
MsRunId::getXmlId() const
{
  return m_xmlId;
}


const QString &
MsRunId::getFileName() const
{
  return m_fileName;
}


void
MsRunId::setMzFormat(MzFormat format)
{
  m_mzFormat = format;
}


MzFormat
MsRunId::getMzFormat() const
{
  return m_mzFormat;
}


bool
MsRunId::operator==(const MsRunId &other) const
{
  if(m_xmlId == other.m_xmlId)
    return true;
  return false;
}


MsRunId &
MsRunId::operator=(const MsRunId &other)
{
  m_xmlId      = other.m_xmlId;
  m_fileName   = other.m_fileName;
  m_sampleName = other.m_sampleName;
  m_mzFormat   = other.m_mzFormat;

  return *this;
}


QString
MsRunId::toString() const
{
  QString text = QString(
                   "file name: '%1'\n"
                   "run id: '%2'\n"
                   "xml id: '%3'\n"
                   "sample name: '%4'\n")
                   .arg(m_fileName)
                   .arg(m_runId)
                   .arg(m_xmlId)
                   .arg(m_sampleName);

  return text;
}


bool
MsRunId::isValid() const
{
  return !m_fileName.isEmpty() && !m_runId.isEmpty() && !m_xmlId.isEmpty() &&
         m_mzFormat != MzFormat::unknown;
}


} // namespace pappso
