#include "mgfoutput.h"

using namespace pappso;

MgfOutput::MgfOutput(QIODevice *p_output_device)
{

  mpa_outputStream = new QTextStream(p_output_device);
}

MgfOutput::~MgfOutput()
{
}

void
MgfOutput::write(const QualifiedMassSpectrum &mass_spectrum)
{
  /*
   * BEGIN IONS
TITLE=20120906_balliau_extract_1_A01_urnb-1.15968.15968.2
RTINSECONDS=2843.58
PEPMASS=638.36934732588 270659.09375
CHARGE=2+
89.01705933 1255.7088623047
89.06031799 258758.140625
90.06361389 12425.6162109375
*/
  *mpa_outputStream << "BEGIN IONS" << Qt::endl;
  *mpa_outputStream << "TITLE="
                    << mass_spectrum.getMassSpectrumId()
                         .getMsRunIdCstSPtr()
                         .get()
                         ->getSampleName()
                    << "."
                    << mass_spectrum.getMassSpectrumId().getSpectrumIndex()
                    << Qt::endl;
  *mpa_outputStream << "RTINSECONDS="
                    << QString::number(mass_spectrum.getRtInSeconds(), 'f', 2)
                    << Qt::endl;
  *mpa_outputStream << "PEPMASS="
                    << QString::number(mass_spectrum.getPrecursorMz(), 'g', 15)
                    << " "
                    << QString::number(
                         mass_spectrum.getPrecursorIntensity(), 'f', 5)
                    << Qt::endl;
  *mpa_outputStream << "CHARGE=" << mass_spectrum.getPrecursorCharge() << "+"
                    << Qt::endl;

  if(mass_spectrum.getMassSpectrumCstSPtr() != nullptr)
    {
      for(auto &&peak : *(mass_spectrum.getMassSpectrumCstSPtr().get()))
        {
          *mpa_outputStream << QString::number(peak.x, 'g', 15) << " "
                            << QString::number(peak.y, 'g', 15) << Qt::endl;
        }
    }
  *mpa_outputStream << "END IONS" << Qt::endl;
}

void
MgfOutput::close()
{
  mpa_outputStream->flush();
}
