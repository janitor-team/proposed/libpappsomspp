/**
 * \file pappsomspp/msrun/output/mzxmloutput.h
 * \date 23/11/2019
 * \author Olivier Langella
 * \brief write msrun peaks into mzxml output stream
 */

/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include "../msrunreader.h"
#include <QXmlStreamWriter>
#include "../../processing/uimonitor/uimonitorinterface.h"

namespace pappso
{
/**
 * @todo use msrunreader as input and writes all peaks into an mzXML output
 * stream
 */
class PMSPP_LIB_DECL MzxmlOutput
{
  public:
  /**
   * Default constructor
   */
  MzxmlOutput(UiMonitorInterface &monitor, QIODevice *p_output_device);

  /**
   * Destructor
   */
  ~MzxmlOutput();

  void write(MsRunReader *p_msrunreader);
  void close();

  void maskMs1(bool mask_ms1);

  void setReadAhead(bool read_ahead);

  private:
  void writeHeader(MsRunReader *p_msrunreader);

  void writeQualifiedMassSpectrum(const QualifiedMassSpectrum &spectrum);


  std::size_t getScanNumber(const QualifiedMassSpectrum &spectrum) const;
  std::size_t
  getPrecursorScanNumber(const QualifiedMassSpectrum &spectrum) const;
  std::size_t getScanNumberFromNativeId(const QString &native_id) const;


  class Translater : public SpectrumCollectionHandlerInterface
  {
    public:
    Translater(MzxmlOutput *p_mzxml_output);
    virtual ~Translater();
    virtual void
    setQualifiedMassSpectrum(const QualifiedMassSpectrum &spectrum) override;
    virtual bool needPeakList() const override;

    private:
    MzxmlOutput *mp_output;
  };


  private:
  UiMonitorInterface &m_monitor;
  QXmlStreamWriter *mpa_outputStream;
  bool m_isReadAhead = false;
  bool m_ms1IsMasked = false;
};
} // namespace pappso
