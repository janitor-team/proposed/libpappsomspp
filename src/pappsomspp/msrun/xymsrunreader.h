
#pragma once

#include "../msfile/msfileaccessor.h"


namespace pappso
{

class XyMsRunReader : public MsRunReader
{
  friend class MsFileAccessor;

  public:
  XyMsRunReader(MsRunIdCstSPtr &msrun_id_csp);
  virtual ~XyMsRunReader();

  virtual MassSpectrumSPtr
  massSpectrumSPtr(std::size_t spectrum_index) override;
  virtual MassSpectrumCstSPtr
  massSpectrumCstSPtr(std::size_t spectrum_index) override;

  virtual QualifiedMassSpectrum
  qualifiedMassSpectrum(std::size_t spectrum_index,
                        bool want_binary_data = true) const override;

  virtual void
  readSpectrumCollection(SpectrumCollectionHandlerInterface &handler) override;

  virtual pappso::XicCoordSPtr newXicCoordSPtrFromSpectrumIndex(
    std::size_t spectrum_index, pappso::PrecisionPtr precision) const override;

  virtual pappso::XicCoordSPtr newXicCoordSPtrFromQualifiedMassSpectrum(
    const pappso::QualifiedMassSpectrum &mass_spectrum,
    pappso::PrecisionPtr precision) const override;

  virtual void
  readSpectrumCollectionByMsLevel(SpectrumCollectionHandlerInterface &handler,
                                  unsigned int ms_level) override;


  virtual std::size_t spectrumListSize() const override;

  virtual bool releaseDevice() override;

  virtual bool acquireDevice() override;


  protected:
  QString m_fileName;
  virtual void initialize() override;
  virtual bool accept(const QString &file_name) const override;

  QualifiedMassSpectrum
  qualifiedMassSpectrumFromXyMSDataFile(MassSpectrumId mass_spectrum_id) const;
};

} // namespace pappso
