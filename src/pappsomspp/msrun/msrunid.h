/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/


#pragma once

/////////////////////// StdLib includes
#include <memory>


/////////////////////// Qt includes
#include <QString>
#include <QMetaType>


/////////////////////// Local includes
#include "../types.h"
#include "../exportinmportconfig.h"


namespace pappso
{

class MsRunId;
typedef std::shared_ptr<const MsRunId> MsRunIdCstSPtr;


/** @brief MS run identity
 * MsRunId identifies an MS run with a unique ID (XmlId)
 * and contains eventually informations on its location (local disk path or URL)
 */
class PMSPP_LIB_DECL MsRunId
{
  public:
  MsRunId();
  MsRunId(const QString &file_name);
  MsRunId(const QString &file_name, const QString &run_id);
  MsRunId(const MsRunId &other);
  virtual ~MsRunId();

  bool operator==(const MsRunId &other) const;
  MsRunId &operator=(const MsRunId &other);

  void setFileName(const QString &file_name);
  const QString &getFileName() const;

  void setRunId(const QString &run_id);
  const QString &getRunId() const;

  /** @brief set an XML unique identifier for this MsRunId
   * @param xml_id this id must respect XML constraints (no space characters)
   */
  void setXmlId(const QString &xml_id);
  const QString &getXmlId() const;

  /** @brief set a sample name for this MsRunId
   */
  void setSampleName(const QString &name);
  const QString &getSampleName() const;

  void setMzFormat(MzFormat format);
  MzFormat getMzFormat() const;

  QString toString() const;

  bool isValid() const;

  private:
  QString m_fileName = "NOT_SET";
  QString m_runId;
  QString m_xmlId; /* a1.... */
  QString m_sampleName;
  MzFormat m_mzFormat = MzFormat::mzXML;
};


} // namespace pappso


Q_DECLARE_METATYPE(pappso::MsRunId);
extern int msRunIdMetaTypeId;

Q_DECLARE_METATYPE(pappso::MsRunIdCstSPtr);
extern int msRunIdCstSPtrMetaTypeId;
