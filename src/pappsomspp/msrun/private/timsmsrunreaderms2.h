/**
 * \file pappsomspp/msrun/private/timsmsrunreaderms2.h
 * \date 10/09/2019
 * \author Olivier Langella
 * \brief MSrun file reader for native Bruker TimsTOF specialized for MS2
 * purpose
 */

/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


#pragma once

#include "../../types.h"
#include "../../msfile/msfileaccessor.h"
#include "../../vendors/tims/timsdata.h"

namespace pappso
{

class PMSPP_LIB_DECL TimsMsRunReaderMs2 : public MsRunReader
{
  friend class MsFileAccessor;
  /**
   * @todo write docs
   */
  public:
  TimsMsRunReaderMs2(MsRunIdCstSPtr &msrun_id_csp);
  virtual ~TimsMsRunReaderMs2();

  virtual MassSpectrumSPtr
  massSpectrumSPtr(std::size_t spectrum_index) override;
  virtual MassSpectrumCstSPtr
  massSpectrumCstSPtr(std::size_t spectrum_index) override;

  virtual QualifiedMassSpectrum
  qualifiedMassSpectrum(std::size_t spectrum_index,
                        bool want_binary_data = true) const override;

  virtual void
  readSpectrumCollection(SpectrumCollectionHandlerInterface &handler) override;


  virtual pappso::XicCoordSPtr newXicCoordSPtrFromSpectrumIndex(
    std::size_t spectrum_index, pappso::PrecisionPtr precision) const override;

  virtual pappso::XicCoordSPtr newXicCoordSPtrFromQualifiedMassSpectrum(
    const pappso::QualifiedMassSpectrum &mass_spectrum,
    pappso::PrecisionPtr precision) const override;


  virtual void
  readSpectrumCollectionByMsLevel(SpectrumCollectionHandlerInterface &handler,
                                  unsigned int ms_level) override;

  virtual std::size_t spectrumListSize() const override;

  virtual bool hasScanNumbers() const override;

  void setMs2FilterCstSPtr(pappso::FilterInterfaceCstSPtr filter);
  void setMs1FilterCstSPtr(pappso::FilterInterfaceCstSPtr filter);

  /** @brief enable or disable simple centroid filter on raw tims data for MS2
   */
  void setMs2BuiltinCentroid(bool centroid);

  /** @brief Get all the precursors id which match the values
   * @return list of precursors Ids
   */
  virtual std::vector<std::size_t>
  getPrecursorsIDFromMzRt(int charge, double mz_val, double rt_sec, double k0);


  virtual bool releaseDevice() override;

  virtual bool acquireDevice() override;

  /** @brief give an access to the underlying raw data pointer
   */
  virtual TimsDataSp getTimsDataSPtr();


  /** @brief retention timeline
   * get retention times along the MSrun in seconds
   * @return vector of retention times (seconds)
   */
  virtual std::vector<double> getRetentionTimeLine() override;

  virtual Trace getTicChromatogram() override;

  protected:
  virtual void initialize() override;
  virtual bool accept(const QString &file_name) const override;

  private:
  TimsDataSp msp_timsData = nullptr;

  pappso::FilterInterfaceCstSPtr msp_ms1Filter;
  pappso::FilterInterfaceCstSPtr msp_ms2Filter;
  /** @brief enable builtin centroid on raw tims integers by default
   */
  bool m_builtinMs2Centroid = true;
};


} // namespace pappso
