/**
 * \file pappsomspp/msrun/private/timsmsrunreaderms2.h
 * \date 10/09/2019
 * \author Olivier Langella
 * \brief MSrun file reader for native Bruker TimsTOF specialized for MS2
 * purpose
 */

/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


#pragma once

#include "timsmsrunreaderms2.h" 
#include "../../types.h"
#include "../../msfile/msfileaccessor.h"
#include "../../msfile/msfilereader.h"
#include "../../vendors/tims/timsdata.h"

namespace pappso
{

class PMSPP_LIB_DECL TimsMsRunReaderMs2Selected : public TimsMsRunReaderMs2
{
  friend class MsFileAccessor;
  /**
   * @todo write docs
   */
  public:
  TimsMsRunReaderMs2Selected(MsRunIdCstSPtr &msrun_id_csp);
  virtual ~TimsMsRunReaderMs2Selected();

  virtual MassSpectrumSPtr
  massSpectrumSPtr(std::size_t spectrum_index) override;
  virtual MassSpectrumCstSPtr
  massSpectrumCstSPtr(std::size_t spectrum_index) override;

  virtual QualifiedMassSpectrum
  qualifiedMassSpectrum(std::size_t spectrum_index,
                        bool want_binary_data = true) const override;

  virtual void
  readSpectrumCollection(SpectrumCollectionHandlerInterface &handler) override;

  virtual std::size_t spectrumListSize() const override;

  virtual bool hasScanNumbers() const override;

  void setMs2FilterCstSPtr(pappso::FilterInterfaceCstSPtr filter);
  void setMs1FilterCstSPtr(pappso::FilterInterfaceCstSPtr filter);

  protected:
  virtual void initialize() override;
  virtual bool accept(const QString &file_name) const override;

  private:
  TimsData *mpa_timsData = nullptr;
};


} // namespace pappso
