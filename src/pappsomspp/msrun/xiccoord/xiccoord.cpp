/**
 * \file pappsomspp/msrun/xiccoord/xiccoord.cpp
 * \date 22/04/2021
 * \author Olivier Langella
 * \brief XIC coordinate in MSrun
 */


/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


#include "xiccoord.h"

using namespace pappso;

pappso::XicCoord::XicCoord()
  : mzRange(pappso_double(1), PrecisionFactory::getPpmInstance(10.0)),
    rtTarget(0)
{
}


XicCoord::XicCoord(const XicCoord &other)
  : mzRange(other.mzRange), rtTarget(other.rtTarget)
{
  xicSptr = other.xicSptr;
}

XicCoord::~XicCoord()
{
}

pappso::XicCoordSPtr
pappso::XicCoord::initializeAndClone() const
{
  XicCoordSPtr xic_coord_sp = std::make_shared<XicCoord>(*this);

  xic_coord_sp.get()->xicSptr = std::make_shared<Xic>();

  return xic_coord_sp;
}


pappso::XicCoordSPtr
pappso::XicCoord::addition(XicCoordSPtr &to_add) const
{
  XicCoordSPtr xic_coord_sp = std::make_shared<XicCoord>(*this);

  // xic_coord_sp.get()->xicSptr = nullptr;

  xic_coord_sp.get()->rtTarget += to_add.get()->rtTarget;

  xic_coord_sp.get()->mzRange += to_add.get()->mzRange;

  return xic_coord_sp;
}

XicCoordSPtr
XicCoord::multiplyBy(double number) const
{
  XicCoordSPtr xic_coord_sp = std::make_shared<XicCoord>(*this);

  // xic_coord_sp.get()->xicSptr = nullptr;

  xic_coord_sp.get()->rtTarget *= number;
  xic_coord_sp.get()->mzRange *= number;

  return xic_coord_sp;
}

pappso::XicCoordSPtr
pappso::XicCoord::divideBy(double number) const
{
  XicCoordSPtr xic_coord_sp = std::make_shared<XicCoord>(*this);

  // xic_coord_sp.get()->xicSptr = nullptr;

  xic_coord_sp.get()->rtTarget /= number;
  xic_coord_sp.get()->mzRange *= (double)((double)1 / number);

  return xic_coord_sp;
}


void
pappso::XicCoord::reset()
{

  xicSptr = nullptr;

  rtTarget = 0;
  mzRange  = MzRange(0.0, 0.0);
}

QString
pappso::XicCoord::toString() const
{
  return QString("mz=%1 rt=%2").arg(mzRange.toString()).arg(rtTarget);
}
