/**
 * \file pappsomspp/msrun/xiccoord/xiccoordtims.h
 * \date 22/04/2021
 * \author Olivier Langella
 * \brief XIC coordinate in a Tims MSrun
 */


/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include "xiccoord.h"

namespace pappso
{


struct XicCoordTims;

typedef std::shared_ptr<XicCoordTims> XicCoordTimsSPtr;

/** @brief coordinates of the XIC to extract and the resulting XIC after
 * extraction
 *
 * to extract a XIC, we need basically the mass to extract it
 * this structure is meant to extact a XIC quickly and not to maintain
 * information about it : no peptide, no scan number, no retention time...
 *
 */
struct PMSPP_LIB_DECL XicCoordTims : XicCoord
{
  /**
   * Default constructor
   */
  XicCoordTims() : XicCoord(){};

  /**
   * Copy constructor
   *
   * @param other TODO
   */
  XicCoordTims(const XicCoordTims &other);

  /**
   * Destructor
   */
  virtual ~XicCoordTims();


  /** @brief intialize the XIC and make a deep copy of object
   */
  virtual XicCoordSPtr initializeAndClone() const override;


  virtual XicCoordSPtr addition(XicCoordSPtr &to_add) const override;

  virtual XicCoordSPtr multiplyBy(double number) const override;
  virtual XicCoordSPtr divideBy(double number) const override;

  virtual void reset() override;

  virtual QString toString() const override;


  /** @brief mobility index begin
   */
  std::size_t scanNumBegin;

  /** @brief mobility index end
   */
  std::size_t scanNumEnd;
};

} // namespace pappso
