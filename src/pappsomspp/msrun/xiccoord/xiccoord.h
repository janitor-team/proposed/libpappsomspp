/**
 * \file pappsomspp/msrun/xiccoord/xiccoord.h
 * \date 22/04/2021
 * \author Olivier Langella
 * \brief XIC coordinate in MSrun
 */


/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include "../../exportinmportconfig.h"
#include "../../mzrange.h"
#include "../../xic/xic.h"
#include <memory>

namespace pappso
{


struct XicCoord;

typedef std::shared_ptr<XicCoord> XicCoordSPtr;

/** @brief coordinates of the XIC to extract and the resulting XIC after
 * extraction
 *
 * to extract a XIC, we need basically the mass to extract it
 * this structure is meant to extact a XIC quickly and not to maintain
 * information about it : no peptide, no scan number, no retention time...
 *
 */
struct PMSPP_LIB_DECL XicCoord
{
  /**
   * Default constructor
   */
  XicCoord();

  /**
   * Copy constructor
   *
   * @param other TODO
   */
  XicCoord(const XicCoord &other);

  /**
   * Destructor
   */
  virtual ~XicCoord();


  /** @brief intialize the XIC and make a deep copy of object
   */
  virtual XicCoordSPtr initializeAndClone() const;

  /** @brief compute a new XIC coord as the sum of the given one
   */
  virtual XicCoordSPtr addition(XicCoordSPtr &to_add) const;


  /** @brief compute a new xic coord as a product by
   */
  virtual XicCoordSPtr multiplyBy(double number) const;


  /** @brief compute a new xic coord as a division by
   */
  virtual XicCoordSPtr divideBy(double number) const;


  /** @brief reset to zero
   */
  virtual void reset();

  /** @brief get a description of the XIC coordinate in a string
   */
  virtual QString toString() const;


  /** @brief the mass to extract
   * */
  MzRange mzRange;

  /** @brief the targeted retention time to extract around
   * intended in seconds, and related to one msrun. This is not a reference,
   * just to save memory and cpu usage when extracting xic
   */
  double rtTarget = 0;

  /** @brief extracted xic
   */
  XicSPtr xicSptr = nullptr;
};
} // namespace pappso
