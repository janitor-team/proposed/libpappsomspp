/**
 * \file pappsomspp/msfile/timsmsfilereader.h
 * \date 06/09/2019
 * \author Olivier Langella
 * \brief MSrun file reader for native Bruker TimsTOF raw data
 */

/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


#pragma once

#include "msfilereader.h"
#include "../msrun/msrunid.h"
#include "../msrun/msrunreader.h"

namespace pappso
{

/**
 * @todo write docs
 */
class TimsMsFileReader : MsFileReader
{
  public:
  TimsMsFileReader(const QString &file_name);
  virtual ~TimsMsFileReader();

  virtual MzFormat getFileFormat() override;

  virtual std::vector<MsRunIdCstSPtr>
  getMsRunIds(const QString &run_prefix) override;

  MsRunReader *selectMsRunReader(const QString &file_name) const;

  private:
  virtual std::size_t initialize();
};
} // namespace pappso
