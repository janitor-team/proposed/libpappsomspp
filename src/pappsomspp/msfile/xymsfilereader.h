
#pragma once

#include <QString>

#include <pwiz/data/msdata/MSData.hpp>

#include "msfilereader.h"
#include "../msrun/msrunid.h"
#include "../msrun/msrunreader.h"


namespace pappso
{


class XyMsFileReader : MsFileReader
{
  private:
  virtual std::size_t initialize();

  public:
  XyMsFileReader(const QString &file_name);
  virtual ~XyMsFileReader();

  virtual MzFormat getFileFormat() override;

  virtual std::vector<MsRunIdCstSPtr>
  getMsRunIds(const QString &run_prefix) override;

  MsRunReader *selectMsRunReader(const QString &file_name) const;
};

} // namespace pappso
