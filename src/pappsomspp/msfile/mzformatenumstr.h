/**
 * \file pappsomspp/msfile/mzformatenumstr.h
 * \date 12/2/2021
 * \author Olivier Langella <olivier.langella@universite-paris-saclay.fr>
 * \brief convert mzformat enumerations to strings
 *
 */

/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella
 *<olivier.langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once
#include "../exportinmportconfig.h"
#include <QString>
#include "../types.h"

namespace pappso
{

/** @brief static functions to convert mz file formats
 */
class PMSPP_LIB_DECL MzFormatEnumStr
{
  public:
  /** @brief convert mz format enumeration to human readable string
   */
  static const QString toString(MzFormat mz_format_enum);
};

} // namespace pappso
