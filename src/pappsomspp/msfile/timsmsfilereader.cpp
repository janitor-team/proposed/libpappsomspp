/**
 * \file pappsomspp/msfile/timsmsfilereader.cpp
 * \date 06/09/2019
 * \author Olivier Langella
 * \brief MSrun file reader for native Bruker TimsTOF raw data
 */

/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "timsmsfilereader.h"
#include "../vendors/tims/timsdata.h"
#include "../pappsoexception.h"
#include "../exception/exceptionnotimplemented.h"
#include <QDebug>
#include <QFileInfo>

namespace pappso
{


TimsMsFileReader::TimsMsFileReader(const QString &file_name)
  : MsFileReader(file_name)
{
  qDebug() << " " << m_fileName;
  initialize();
}


TimsMsFileReader::~TimsMsFileReader()
{
}


std::size_t
TimsMsFileReader::initialize()
{

  m_fileFormat = MzFormat::unknown;
  try
    {
      TimsData tims_data(m_fileName);
      m_fileFormat = MzFormat::brukerTims;
    }
  catch(ExceptionNotImplemented &error)
    {
      m_fileFormat = MzFormat::brukerTims;
      throw pappso::ExceptionNotImplemented(
        QObject::tr("Error reading Bruker tims data in %1 :\n%2")
          .arg(m_fileName)
          .arg(error.qwhat()));
    }
  catch(PappsoException &error)
    {
      return 0;
    }

  return 1;
}


MzFormat
TimsMsFileReader::getFileFormat()
{
  return m_fileFormat;
}


std::vector<MsRunIdCstSPtr>
TimsMsFileReader::getMsRunIds(const QString &run_prefix)
{
  std::vector<MsRunIdCstSPtr> ms_run_ids;

  if(!initialize())
    return ms_run_ids;

  // Finally create the MsRunId with the file name.
  MsRunId ms_run_id(m_fileName);
  ms_run_id.setMzFormat(m_fileFormat);

  // We need to set the unambiguous xmlId string.
  ms_run_id.setXmlId(QString("%1a1").arg(run_prefix));

  ms_run_id.setRunId("a1");

  // Now set the sample name to the run id:

  ms_run_id.setSampleName(QFileInfo(m_fileName).baseName());

  qDebug() << "Current ms_run_id:" << ms_run_id.toString();

  // Finally make a shared pointer out of it and append it to the vector.
  ms_run_ids.push_back(std::make_shared<MsRunId>(ms_run_id));

  return ms_run_ids;
}


} // namespace pappso
