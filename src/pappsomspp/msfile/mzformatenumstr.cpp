/**
 * \file pappsomspp/msfile/mzformatenumstr.cpp
 * \date 12/2/2021
 * \author Olivier Langella <olivier.langella@universite-paris-saclay.fr>
 * \brief convert mzformat enumerations to strings
 *
 */

/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella
 *<olivier.langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "mzformatenumstr.h"
#include "../pappsoexception.h"
#include <QObject>

using namespace pappso;

const QString
pappso::MzFormatEnumStr::toString(pappso::MzFormat mz_format_enum)
{

  QString mz_format_str;
  switch(mz_format_enum)
    {
      case MzFormat::abSciexT2D:
        mz_format_str = "abSciexT2D";
        break;
      case MzFormat::abSciexWiff:
        mz_format_str = "abSciexWiff";
        break;
      case MzFormat::agilentMassHunter:
        mz_format_str = "agilentMassHunter";
        break;
      case MzFormat::brukerBaf:
        mz_format_str = "brukerBaf";
        break;
      case MzFormat::brukerFid:
        mz_format_str = "brukerFid";
        break;
      case MzFormat::brukerTims:
        mz_format_str = "brukerTims";
        break;
      case MzFormat::brukerYep:
        mz_format_str = "brukerYep";
        break;
      case MzFormat::MGF:
        mz_format_str = "MGF";
        break;
      case MzFormat::msn:
        mz_format_str = "msn";
        break;
      case MzFormat::mz5:
        mz_format_str = "mz5";
        break;
      case MzFormat::mzML:
        mz_format_str = "mzML";
        break;
      case MzFormat::mzXML:
        mz_format_str = "mzXML";
        break;
      case MzFormat::SQLite3:
        mz_format_str = "SQLite3";
        break;
      case MzFormat::thermoRaw:
        mz_format_str = "thermoRaw";
        break;
      case MzFormat::watersRaw:
        mz_format_str = "watersRaw";
        break;
      case MzFormat::xy:
        mz_format_str = "xy";
        break;
      default:
        throw pappso::PappsoException(QObject::tr("MzFormat unknown :\n%1")
                                        .arg((std::uint8_t)mz_format_enum));
    }
  return mz_format_str;
}
