
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include <QDir>
#include <QDebug>
#include "obopsimod.h"
#include "../pappsoexception.h"
#include <iostream>

inline void
initMyResource()
{
  Q_INIT_RESOURCE(libpappsomsppresources);
}


namespace pappso
{

OboPsiMod::OboPsiMod(OboPsiModHandlerInterface &handler) : m_handler(handler)
{
  qDebug();
  initMyResource();
  parse();
}

OboPsiMod::~OboPsiMod()
{
}


void
OboPsiMod::parse()
{
  // std::cout << "OboPsiMod::parse Begin parsing OBO file" << std::endl;
  qDebug() << "OboPsiMod::parse Begin parsing OBO file";
  QFile obofile(":/resources/PSI-MOD.obo");
  if(!obofile.exists())
    {
      throw PappsoException(
        QObject::tr("PSI-MOD OBO resource file : %1 not found")
          .arg(obofile.fileName()));
    }
  obofile.open(QIODevice::ReadOnly);
  QTextStream p_in(&obofile);

  // Search accession conta
  // QTextStream in(p_in);
  QString line = p_in.readLine();
  bool in_term = false;
  while(!p_in.atEnd())
    {
      // qDebug() << "OboPsiMod::parse line "<< line;
      if(line.startsWith("[Term]"))
        {
          in_term = true;
          m_term.clearTerm();
        }
      else if(line.isEmpty())
        {
          if(in_term)
            {
              m_handler.setOboPsiModTerm(m_term);
              in_term = false;
            }
        }
      else
        {
          if(in_term)
            m_term.parseLine(line);
          // m_handler.setSequence(line);
        }
      line = p_in.readLine();
    }
  if(in_term)
    {
      m_handler.setOboPsiModTerm(m_term);
    }
  // p_in->close();

  obofile.close();
}

} // namespace pappso
