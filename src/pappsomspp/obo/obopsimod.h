/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <QRegularExpression>
#include "../types.h"
#include "../exportinmportconfig.h"
#include "obopsimodterm.h"
#include "obopsimodhandlerinterface.h"

namespace pappso
{
class PMSPP_LIB_DECL OboPsiMod
{
  public:
  OboPsiMod(OboPsiModHandlerInterface &handler);
  ~OboPsiMod();

  private:
  OboPsiModTerm m_term;
  OboPsiModHandlerInterface &m_handler;

  /** @brief starts reading obo file and reports each term with the callback
   * function
   */
  void parse();
};
} // namespace pappso
