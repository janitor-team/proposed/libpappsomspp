/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <QRegularExpression>
#include "../exportinmportconfig.h"

namespace pappso
{

class OboPsiMod;

class PMSPP_LIB_DECL OboPsiModTerm
{
  friend OboPsiMod;

  public:
  OboPsiModTerm();
  virtual ~OboPsiModTerm();
  OboPsiModTerm(const OboPsiModTerm &);
  OboPsiModTerm &operator=(const OboPsiModTerm &);

  bool isValid() const;

  public:
  QString m_accession;
  QString m_name;
  QString m_definition;
  QString m_psiModLabel;
  QString m_psiMsLabel;
  QString m_diffFormula;
  QString m_origin;

  double m_diffMono = 0;

  private:
  void parseLine(const QString &line);
  void clearTerm();

  static QRegularExpression m_firstParse;
  static QRegularExpression m_findExactPsiModLabel;
  static QRegularExpression m_findRelatedPsiMsLabel;
};

} // namespace pappso


extern int oboPsiModTermMetaTypeId;
