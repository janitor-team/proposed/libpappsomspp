
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "peptideinterface.h"

namespace pappso
{

const QString
PeptideInterface::getSequenceLi() const
{
  return QString(this->getSequence()).replace("L", "I");
}
const QString
PeptideInterface::getFormula(unsigned int charge) const
{
  QString carbon(
    QString("C %1").arg(this->getNumberOfAtom(AtomIsotopeSurvey::C) -
                        this->getNumberOfIsotope(Isotope::C13)));
  if(this->getNumberOfIsotope(Isotope::C13) > 0)
    {
      carbon.append(
        QString(" (13)C %1").arg(this->getNumberOfIsotope(Isotope::C13)));
    }
  QString hydrogen(
    QString("H %1").arg(this->getNumberOfAtom(AtomIsotopeSurvey::H) + charge -
                        this->getNumberOfIsotope(Isotope::H2)));
  if(this->getNumberOfIsotope(Isotope::H2) > 0)
    {
      hydrogen.append(
        QString(" (2)H %1").arg(this->getNumberOfIsotope(Isotope::H2)));
    }
  QString oxygen(
    QString("O %1").arg(this->getNumberOfAtom(AtomIsotopeSurvey::O) -
                        this->getNumberOfIsotope(Isotope::O17) -
                        this->getNumberOfIsotope(Isotope::O18)));
  if(this->getNumberOfIsotope(Isotope::O17) > 0)
    {
      oxygen.append(
        QString(" (17)O %1").arg(this->getNumberOfIsotope(Isotope::O17)));
    }
  if(this->getNumberOfIsotope(Isotope::O18) > 0)
    {
      oxygen.append(
        QString(" (18)O %1").arg(this->getNumberOfIsotope(Isotope::O18)));
    }
  QString nitrogen(
    QString("N %1").arg(this->getNumberOfAtom(AtomIsotopeSurvey::N) -
                        this->getNumberOfIsotope(Isotope::N15)));
  if(this->getNumberOfIsotope(Isotope::N15) > 0)
    {
      nitrogen.append(
        QString(" (15)N %1").arg(this->getNumberOfIsotope(Isotope::N15)));
    }

  QString sulfur(
    QString("S %1").arg(this->getNumberOfAtom(AtomIsotopeSurvey::S) -
                        this->getNumberOfIsotope(Isotope::S33) -
                        this->getNumberOfIsotope(Isotope::S34) -
                        this->getNumberOfIsotope(Isotope::S36)));
  if(this->getNumberOfIsotope(Isotope::S33) > 0)
    {
      sulfur.append(
        QString(" (33)S %1").arg(this->getNumberOfIsotope(Isotope::S33)));
    }
  if(this->getNumberOfIsotope(Isotope::S34) > 0)
    {
      sulfur.append(
        QString(" (34)S %1").arg(this->getNumberOfIsotope(Isotope::S34)));
    }
  if(this->getNumberOfIsotope(Isotope::S36) > 0)
    {
      sulfur.append(
        QString(" (36)S %1").arg(this->getNumberOfIsotope(Isotope::S36)));
    }
  return QString("%1 %2 %3 %4 %5")
    .arg(carbon)
    .arg(hydrogen)
    .arg(oxygen)
    .arg(nitrogen)
    .arg(sulfur);
}
} // namespace pappso
