
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "grpsubgroupset.h"

namespace pappso
{

GrpSubGroupSet::GrpSubGroupSet()
{
}

GrpSubGroupSet::GrpSubGroupSet(const GrpSubGroupSet &other)
  : m_grpSubGroupPtrList(other.m_grpSubGroupPtrList)
{
}

GrpSubGroupSet::~GrpSubGroupSet()
{
}


void
GrpSubGroupSet::addAll(const GrpSubGroupSet &other)
{

  std::list<GrpSubGroup *>::iterator it(m_grpSubGroupPtrList.begin());
  std::list<GrpSubGroup *>::iterator itEnd(m_grpSubGroupPtrList.end());
  std::list<GrpSubGroup *>::const_iterator itIn(
    other.m_grpSubGroupPtrList.begin());
  std::list<GrpSubGroup *>::const_iterator itInEnd(
    other.m_grpSubGroupPtrList.end());

  while((itIn != itInEnd) && (it != itEnd))
    {
      if(*itIn < *it)
        {
          it++;
          continue;
        }
      if(*itIn > *it)
        {
          it = m_grpSubGroupPtrList.insert(it, *itIn);
          it++;
          itIn++;
          continue;
        }
      if(*itIn == *it)
        {
          itIn++;
          it++;
        }
    }
  while(itIn != itInEnd)
    {
      m_grpSubGroupPtrList.push_back(*itIn);
      itIn++;
    }
}

void
GrpSubGroupSet::remove(GrpSubGroup *p_remove_sub_group)
{
  m_grpSubGroupPtrList.remove(p_remove_sub_group);
}

bool
GrpSubGroupSet::contains(GrpSubGroup *p_sub_group) const
{

  std::list<GrpSubGroup *>::const_iterator it(m_grpSubGroupPtrList.begin()),
    itEnd(m_grpSubGroupPtrList.end());


  while(it != itEnd)
    {
      if(p_sub_group == *it)
        {
          // this subgroup is already in list
          return true;
        }
      if(p_sub_group > *it)
        {
          return false;
        }
      it++;
    }
  return false;
}
void
GrpSubGroupSet::add(GrpSubGroup *p_add_sub_group)
{

  std::list<GrpSubGroup *>::iterator it(m_grpSubGroupPtrList.begin()),
    itEnd(m_grpSubGroupPtrList.end());


  while(it != itEnd)
    {
      if(p_add_sub_group == *it)
        {
          // this subgroup is already in list
          return;
        }
      if(p_add_sub_group > *it)
        {
          it = m_grpSubGroupPtrList.insert(it, p_add_sub_group);
          return;
        }
      it++;
    }
  m_grpSubGroupPtrList.push_back(p_add_sub_group);
}


const QString
GrpSubGroupSet::printInfos() const
{
  QString infos;
  std::list<GrpSubGroup *>::const_iterator it(m_grpSubGroupPtrList.begin()),
    itEnd(m_grpSubGroupPtrList.end());


  while(it != itEnd)
    {
      infos.append((*it)->getFirstAccession() + " " +
                   QString("0x%1").arg(
                     (quintptr)*it, QT_POINTER_SIZE * 2, 16, QChar('0')) +
                   "\n");
      it++;
    }

  return infos;
}

} // namespace pappso
