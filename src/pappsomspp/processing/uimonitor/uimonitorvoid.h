/**
 * \file pappsomspp/processing/uimonitor/uimonitorvoid.h
 * \date 14/5/2021
 * \author Olivier Langella
 * \brief simle monitor that does nothing
 **/

/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include "uimonitorinterface.h"
#include <QTextStream>

namespace pappso
{


/**
 * @todo nothing monitor to save CPU and memory if needed
 */
class PMSPP_LIB_DECL UiMonitorVoid : public UiMonitorInterface
{
  public:
  virtual bool
  shouldIstop() override
  {
    return false;
  };

  virtual void setTotalSteps(std::size_t total_number_of_steps
                             [[maybe_unused]]) override{};
  virtual void count() override{};
  virtual void setTitle(const QString &title [[maybe_unused]]) override{};
  virtual void setStatus(const QString &status [[maybe_unused]]) override{};
  virtual void appendText(const QString &text [[maybe_unused]]) override{};
};
} // namespace pappso
