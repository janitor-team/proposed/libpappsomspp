/**
 * \file pappsomspp/processing/uimonitor/uimonitortextpercent.cpp
 * \date 25/9/2021
 * \author Olivier Langella
 * \brief simle text monitor implementation of the User Interface Monitor
 *displaying percent progression
 **/

/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "uimonitortextpercent.h"
#include <QDebug>

using namespace pappso;

UiMonitorTextPercent::UiMonitorTextPercent(QTextStream &output_stream)
  : UiMonitorText(output_stream)
{
  m_unit       = -1;
  m_totalSteps = 1;
}

UiMonitorTextPercent::~UiMonitorTextPercent()
{
}


void
pappso::UiMonitorTextPercent::setTotalSteps(std::size_t total_number_of_steps)
{
  if(m_count > 0)
    {

      m_outputStream << Qt::endl;
      m_outputStream.flush();
    }
  UiMonitorText::setTotalSteps(total_number_of_steps);
  m_unit = -1;
  if(m_totalSteps == 0)
    {
      m_totalSteps = -1;
    }
}

void
pappso::UiMonitorTextPercent::count()
{
  m_count++;
  if(m_count <= m_totalSteps)
    {
      int unit = (int)(((float)m_count / (float)m_totalSteps) * 10);
      // qDebug() << unit;
      if(unit > m_unit)
        {
          m_outputStream << (unit * 10) << "% ";
          m_outputStream.flush();
          m_unit = unit;
        }
    }
}
