/**
 * \file pappsomspp/processing/uimonitor/uimonitortextpercent.h
 * \date 25/9/2021
 * \author Olivier Langella
 * \brief simle text monitor implementation of the User Interface Monitor
 *displaying percent progression
 **/

/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include "uimonitorinterface.h"
#include "uimonitortext.h"
#include <QTextStream>

namespace pappso
{


/**
 * @todo write docs
 */
class PMSPP_LIB_DECL UiMonitorTextPercent : public UiMonitorText
{
  public:
  /**
   * Default constructor
   */
  UiMonitorTextPercent(QTextStream &output_stream);

  /**
   * Destructor
   */
  virtual ~UiMonitorTextPercent();


  /** @brief count steps
   * report when a step is computed in an algorithm
   */
  virtual void count() override;


  virtual void setTotalSteps(std::size_t total_number_of_steps);


  protected:
  int m_unit;
};
} // namespace pappso
