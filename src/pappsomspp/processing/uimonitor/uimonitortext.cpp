/**
 * \file pappsomspp/processing/uimonitor/uimonitortext.cpp
 * \date 14/5/2021
 * \author Olivier Langella
 * \brief simle text monitor implementation of the User Interface Monitor
 **/

/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include <QObject>

#include "uimonitortext.h"

using namespace pappso;

UiMonitorText::UiMonitorText(QTextStream &output_stream)
  : m_outputStream(output_stream)
{
}

UiMonitorText::~UiMonitorText()
{
}

bool
pappso::UiMonitorText::shouldIstop()
{
  // by defaut not interruptible
  return false;
}

void
pappso::UiMonitorText::setTitle(const QString &title)
{
  m_outputStream << title << Qt::endl;
  m_outputStream.flush();
}

void
pappso::UiMonitorText::setStatus(const QString &status)
{
  m_outputStream << status << Qt::endl;
  m_outputStream.flush();
}

void
pappso::UiMonitorText::count()
{
  m_count++;
  if(m_count <= m_totalSteps)
    {
      m_outputStream << m_count << " ";
    }
  else
    {
      m_outputStream << QObject::tr("%1 on %2").arg(m_count).arg(m_totalSteps)
                     << " ";
    }
  m_outputStream.flush();
}

void
pappso::UiMonitorText::appendText(const QString &text)
{
  m_outputStream << text;
  m_outputStream.flush();
}

void
pappso::UiMonitorText::setTotalSteps(std::size_t total_number_of_steps)
{
  UiMonitorInterface::setTotalSteps(total_number_of_steps);
  m_count = 0;
}
