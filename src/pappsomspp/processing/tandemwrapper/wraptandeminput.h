/**
 * \file pappsomspp/processing/tandemwrapper/wraptandeminput.h
 * \date 13/11/2021
 * \author Olivier Langella
 * \brief rewrites tandem xml input file with temporary files
 */

/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of PAPPSOms-tools.
 *
 *     PAPPSOms-tools is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms-tools is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <QDebug>
#include <QFile>
#include "../xml/xmlstreamreaderinterface.h"
/**
 * @todo write docs
 */
namespace pappso
{

/**
 * @todo write docs
 */
class WrapTandemInput : public XmlStreamReaderInterface
{
  public:
  /**
   * Default constructor
   */
  WrapTandemInput(const QString &destinationMzXmlFile,
                  const QString &destinationTandemInputFile,
                  const QString &destinationTandemOutputFile);

  /**
   * Destructor
   */
  virtual ~WrapTandemInput();


  const QString &getOriginalMsDataFileName() const;
  const QString &getOriginalTandemOutputFileName() const;

  const QString &getOriginalTandemPresetFileName() const;


  protected:
  virtual void readStream() override;

  private:
  QString m_destinationMzXmlFileName;
  QString m_originMzDataFileName;
  QString m_destinationTandemInputFileName;
  QString m_originTandemPresetFileName;
  QString m_originTandemOutpuFileName;
  QString m_destinationTandemOutputFileName;
  QFile m_destinationTandemInputFile;
  QXmlStreamWriter m_writerXmlTandemInput;
};
} // namespace pappso
