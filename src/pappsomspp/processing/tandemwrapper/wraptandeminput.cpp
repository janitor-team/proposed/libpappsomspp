/**
 * \file pappsomspp/processing/tandemwrapper/wraptandeminput.cpp
 * \date 13/11/2021
 * \author Olivier Langella
 * \brief rewrites tandem xml input file with temporary files
 */

/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of PAPPSOms-tools.
 *
 *     PAPPSOms-tools is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms-tools is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


#include "wraptandeminput.h"
#include <QFileInfo>
#include "../../pappsoexception.h"


namespace pappso
{
WrapTandemInput::WrapTandemInput(const QString &destinationMzXmlFile,
                                 const QString &destinationTandemInputFile,
                                 const QString &destinationTandemOutputFile)
  : m_destinationTandemInputFile(destinationTandemInputFile)
{
  m_destinationMzXmlFileName        = destinationMzXmlFile;
  m_destinationTandemOutputFileName = destinationTandemOutputFile;
  m_destinationTandemInputFileName =
    QFileInfo(destinationTandemInputFile).absoluteFilePath();

  if(destinationTandemInputFile.isEmpty())
    {
      throw pappso::PappsoException(
        QObject::tr("ERROR: X!Tandem input file path is empty"));
    }

  if(!m_destinationTandemInputFile.open(QIODevice::WriteOnly))
    {
      throw pappso::PappsoException(
        QObject::tr("ERROR: unable to open %1 tandem output file for write")
          .arg(destinationTandemInputFile));
    }

  m_writerXmlTandemInput.setDevice(&m_destinationTandemInputFile);
  m_writerXmlTandemInput.setAutoFormatting(true);
  m_writerXmlTandemInput.writeStartDocument("1.0");
}

WrapTandemInput::~WrapTandemInput()
{
  m_destinationTandemInputFile.close();
}


void
WrapTandemInput::readStream()
{
  qDebug();
  if(m_qxmlStreamReader.readNextStartElement())
    {
      if(m_qxmlStreamReader.name().toString() == "bioml")
        {
          cloneStartElement(m_writerXmlTandemInput);
          qDebug();
          while(m_qxmlStreamReader.readNextStartElement())
            {
              // qDebug() << m_qxmlStreamReader.name();
              // read_note();
              cloneStartElement(m_writerXmlTandemInput);

              QString type =
                m_qxmlStreamReader.attributes().value("type").toString();
              QString label =
                m_qxmlStreamReader.attributes().value("label").toString();

              if((type == "input") && (label == "spectrum, path"))
                {
                  //<note type="input" label="spectrum, path">

                  m_originMzDataFileName = m_qxmlStreamReader.readElementText();
                  m_writerXmlTandemInput.writeCharacters(
                    m_destinationMzXmlFileName);
                  // m_qxmlStreamReader.skipCurrentElement();
                }
              else if((type == "input") && (label == "output, path"))
                {
                  //<note type="input" label="output, path">
                  m_originTandemOutpuFileName =
                    m_qxmlStreamReader.readElementText();
                  m_writerXmlTandemInput.writeCharacters(
                    m_destinationTandemOutputFileName);
                  // m_qxmlStreamReader.skipCurrentElement();
                }
              // list path, default parameters
              else if((type == "input") &&
                      (label == "list path, default parameters"))
                {
                  //<note type="input" label="list path, default
                  // parameters">/gorgone/pappso/tmp/xtpcpp.AjyZGg/Lumos_trypsin_rev_camC_oxM_10ppm_HCDOT_12102017CH.xml</note>

                  m_originTandemPresetFileName =
                    m_qxmlStreamReader.readElementText();
                  m_writerXmlTandemInput.writeCharacters(
                    m_originTandemPresetFileName);
                }
              else
                {
                  m_writerXmlTandemInput.writeCharacters(
                    m_qxmlStreamReader.readElementText());
                }
              m_writerXmlTandemInput.writeEndElement();
            }
        }
      else
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("Not an X!Tandem input file"));
          m_qxmlStreamReader.skipCurrentElement();
        }
    }
  m_writerXmlTandemInput.writeEndDocument();
  m_destinationTandemInputFile.close();
  qDebug();
}


const QString &
WrapTandemInput::getOriginalMsDataFileName() const
{
  return m_originMzDataFileName;
}

const QString &
WrapTandemInput::getOriginalTandemOutputFileName() const
{
  return m_originTandemOutpuFileName;
}

const QString &
WrapTandemInput::getOriginalTandemPresetFileName() const
{
  return m_originTandemPresetFileName;
}
} // namespace pappso
