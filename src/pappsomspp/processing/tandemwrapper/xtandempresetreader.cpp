/**
 * \file pappsomspp/processing/tandemwrapper/xtandempresetreader.cpp
 * \date 06/02/2020
 * \author Olivier Langella
 * \brief read tandem preset file to get centroid parameters and number of
 * threads
 */

/*******************************************************************************
 * Copyright (c) 2020 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of PAPPSOms-tools.
 *
 *     PAPPSOms-tools is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms-tools is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "xtandempresetreader.h"
#include "../../pappsoexception.h"
#include "../../precision.h"

namespace pappso
{
XtandemPresetReader::XtandemPresetReader()
{
}

XtandemPresetReader::~XtandemPresetReader()
{
}


void
XtandemPresetReader::read_note()
{
  // <note type="input"
  // label="output,path">/gorgone/pappso/jouy/users/Celine/2019_Lumos/20191222_107_Juste_APD/metapappso_condor/test_run/20191222_18_EF1_third_step_test_condor_22janv.xml</note>
  if(m_qxmlStreamReader.name().toString() != "note")
    {
      m_qxmlStreamReader.skipCurrentElement();
      return;
    }
  Q_ASSERT(m_qxmlStreamReader.name().toString() == "note");
  QString label;


  m_countNote++;
  if((m_qxmlStreamReader.attributes().hasAttribute("type")) &&
     (m_qxmlStreamReader.attributes().value("type").toString() == "input"))
    {
      if(m_qxmlStreamReader.attributes().hasAttribute("label"))
        {
          label = m_qxmlStreamReader.attributes().value("label").toString();
        }
    }
  if(label.isEmpty())
    {
    }
  else
    {
      if(label == "spectrum, timstof MS2 filters")
        {
        }
      else if(label == "spectrum, threads")
        {
          m_threads = m_qxmlStreamReader.readElementText().toInt();
          return;
        }
      else if(label == "spectrum, fragment monoisotopic mass error units")
        {

          m_ms2precisionUnit = PrecisionUnit::dalton;
          if(m_qxmlStreamReader.readElementText().trimmed().toLower() == "ppm")
            {
              m_ms2precisionUnit = PrecisionUnit::ppm;
            }
          return;
        }
      else if(label == "spectrum, fragment monoisotopic mass error")
        {
          m_ms2precisionValue = m_qxmlStreamReader.readElementText().toDouble();
          return;
        }
    }
  m_qxmlStreamReader.skipCurrentElement();
  // qDebug() << "end ";
  //
}


int
XtandemPresetReader::getNumberOfThreads() const
{
  return m_threads;
}


const QString
XtandemPresetReader::getMs2FiltersOptions() const
{
  QString filter_suite;
  PrecisionPtr precision = PrecisionFactory::getPrecisionPtrInstance(
    m_ms2precisionUnit, m_ms2precisionValue);
  QString charge_decon_filter = QString("chargeDeconvolution|%1")
                                  .arg(precision->toString().replace(" ", ""));

  PrecisionPtr precision_exclu = PrecisionFactory::getPrecisionPtrInstance(
    m_ms2precisionUnit, m_ms2precisionValue / 2);
  QString mz_exclusion_filter =
    QString("mzExclusion|%1").arg(precision_exclu->toString().replace(" ", ""));
  filter_suite = charge_decon_filter + " " + mz_exclusion_filter;
  return filter_suite;
}


void
XtandemPresetReader::readStream()
{
  qDebug();
  m_countNote         = 0;
  m_threads           = -1;
  m_ms2precisionUnit  = PrecisionUnit::dalton;
  m_ms2precisionValue = -1;
  if(m_qxmlStreamReader.readNextStartElement())
    {
      if(m_qxmlStreamReader.name().toString() == "bioml")
        {
          qDebug();
          while(m_qxmlStreamReader.readNextStartElement())
            {
              // qDebug() << m_qxmlStreamReader.name();
              read_note();
            }
        }
      else
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("Not an X!Tandem preset file"));
          m_qxmlStreamReader.skipCurrentElement();
        }
    }
  if(m_threads == -1)
    {
      qDebug();
      m_qxmlStreamReader.raiseError(QObject::tr("Not an X!Tandem preset file"));
    }
  qDebug();
}

int
XtandemPresetReader::getCountNote() const
{
  return m_countNote;
}

} // namespace pappso
