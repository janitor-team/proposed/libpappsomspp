
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/
#include "tracedetectionmoulon.h"

namespace pappso
{
TraceDetectionMoulon::TraceDetectionMoulon(
  unsigned int smoothing_half_window_length,
  pappso_double tic_start,
  pappso_double tic_stop)
  : m_xicFilterSmoothing(smoothing_half_window_length)
{
  m_ticStart = tic_start;
  m_ticStop  = tic_stop;
}

TraceDetectionMoulon::TraceDetectionMoulon(const TraceDetectionMoulon &other)
  : m_xicFilterSmoothing(other.m_xicFilterSmoothing)
{
  m_ticStart = other.m_ticStart;
  m_ticStop  = other.m_ticStop;
}

TraceDetectionMoulon::~TraceDetectionMoulon()
{
}


void
TraceDetectionMoulon::setFilterMorphoMean(const FilterMorphoMean &smooth)
{
  m_xicFilterSmoothing = smooth;
}

void
TraceDetectionMoulon::setTicStart(double tic_start)
{
  m_ticStart = tic_start;
}
void
TraceDetectionMoulon::setTicStop(double tic_stop)
{
  m_ticStop = tic_stop;
}

unsigned int
TraceDetectionMoulon::getSmoothingHalfEdgeWindows() const
{
  return m_xicFilterSmoothing.getMeanHalfEdgeWindows();
}

pappso_double
TraceDetectionMoulon::getTicStart() const
{
  return m_ticStart;
}

pappso_double
TraceDetectionMoulon::getTicStop() const
{
  return m_ticStop;
}

void
TraceDetectionMoulon::detect(const Trace &xic,
                             TraceDetectionSinkInterface &sink,
                             bool remove_peak_base) const
{

  Trace xic_smoothed(xic);

  m_xicFilterSmoothing.filter(xic_smoothed);

  // detect peaks :
  bool banked(false);
  unsigned int nb_tic_start(0);

  std::vector<DataPoint>::const_iterator it_smoothed;
  std::vector<DataPoint>::const_iterator it, it_begin;

  // TracePeak *p_current_peak = nullptr;

  for(it_smoothed = xic_smoothed.begin(), it = xic.begin();
      it_smoothed != xic_smoothed.end();
      ++it_smoothed, ++it)
    {

      if((nb_tic_start == 0) && (it_begin != xic.end()) && (banked == false))
        {
          // delete(p_current_peak);
          // p_current_peak = nullptr;
          it_begin = xic.end();
        }

      if(it_smoothed->y >= m_ticStart)
        {
          nb_tic_start++;
          if(it_begin == xic.end())
            {
              // p_current_peak = new TracePeak;
              // p_current_peak->setLeftBoundary(*it_smoothed);
              it_begin = it;
              banked   = false;
            }
          if((nb_tic_start == 2) && (banked == false))
            {
              banked = true;
            }
        }
      else
        {
          nb_tic_start = 0;
        }
      if(it_smoothed->y <= m_ticStop)
        {
          if(it_begin != xic.end())
            {

              if(banked)
                {
                  TracePeak peak(it_begin, it + 1, remove_peak_base);
                  sink.setTracePeak(peak);
                }
              banked = false;
            }
        }
    }
}
} // namespace pappso
