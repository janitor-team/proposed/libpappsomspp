/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once


#include "tracedetectioninterface.h"
#include "../../processing/filters/filtermorpho.h"

namespace pappso
{


class PMSPP_LIB_DECL TraceDetectionMoulon : public TraceDetectionInterface
{
  private:
  FilterMorphoMean m_xicFilterSmoothing;
  pappso_double m_ticStart;
  pappso_double m_ticStop;

  public:
  TraceDetectionMoulon(unsigned int smoothing_half_window_length,
                       pappso_double tic_start,
                       pappso_double tic_stop);
  TraceDetectionMoulon(const TraceDetectionMoulon &other);
  virtual ~TraceDetectionMoulon();


  void setFilterMorphoMean(const FilterMorphoMean &smooth);
  void setTicStart(double tic_start);
  void setTicStop(double tic_stop);

  unsigned int getSmoothingHalfEdgeWindows() const;
  pappso_double getTicStart() const;
  pappso_double getTicStop() const;


  void detect(const Trace &xic,
              TraceDetectionSinkInterface &sink,
              bool remove_peak_base) const override;
};


} // namespace pappso
