/**
 * \file pappsomspp/filers/filterlocalmaximum.h
 * \date 24/09/2019
 * \author Olivier Langella
 * \brief filter to select local maximum in a spectrum
 * inspired from the proteowizard library "LocalMaximumPeakDetector"
 */

/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include "filterinterface.h"

namespace pappso
{

/**
 * @brief finds all local maxima, i.e. any point that has a greater y value than
 * both of its neighboring points and only keep those points
 *
 * usefull documentation :
 * http://pd.chem.ucl.ac.uk/pdnn/peaks/peakindx.htm
 */
class PMSPP_LIB_DECL FilterLocalMaximum : public FilterInterface
{
  public:
  /**
   * Default constructor
   */
  FilterLocalMaximum(std::size_t half_window_size);

  /**
   * Copy constructor
   *
   * @param other TODO
   */
  FilterLocalMaximum(const FilterLocalMaximum &other);

  /**
   * Destructor
   */
  virtual ~FilterLocalMaximum();
  Trace &filter(Trace &data_points) const override;

  std::size_t getHalfWindowSize() const;


  private:
  std::size_t m_halfWindowSize = 0;
};
} // namespace pappso
