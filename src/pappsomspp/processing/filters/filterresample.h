/**
 * \file pappsomspp/filers/filterresample.h
 * \date 28/04/2019
 * \author Olivier Langella
 * \brief collection of filters concerned by X selection
 */

/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include "filterinterface.h"
#include <cstddef>
#include "../../mzrange.h"
#include "../../exportinmportconfig.h"
#include "../combiners/selectionpolygon.h"

namespace pappso
{

class PMSPP_LIB_DECL FilterResampleKeepSmaller : public FilterInterface
{
  private:
  double m_value;

  public:
  FilterResampleKeepSmaller(double x_value);
  FilterResampleKeepSmaller(const FilterResampleKeepSmaller &other);
  virtual ~FilterResampleKeepSmaller(){};
  Trace &filter(Trace &trace) const override;
};


class PMSPP_LIB_DECL FilterResampleKeepGreater : public FilterInterface
{
  private:
  double m_value;

  public:
  FilterResampleKeepGreater(double x_value);
  FilterResampleKeepGreater(const FilterResampleKeepGreater &other);
  virtual ~FilterResampleKeepGreater(){};

  FilterResampleKeepGreater &operator=(const FilterResampleKeepGreater &other);
  Trace &filter(Trace &trace) const override;

  double getThresholdX() const;
};


class PMSPP_LIB_DECL FilterResampleRemoveXRange : public FilterInterface
{
  private:
  double m_minX;
  double m_maxX;

  public:
  FilterResampleRemoveXRange(double min_x, double max_x);
  FilterResampleRemoveXRange(const FilterResampleRemoveXRange &other);
  virtual ~FilterResampleRemoveXRange(){};

  FilterResampleRemoveXRange &
  operator=(const FilterResampleRemoveXRange &other);
  Trace &filter(Trace &trace) const override;
};


class PMSPP_LIB_DECL FilterResampleKeepXRange : public FilterInterface
{
  private:
  double m_minX;
  double m_maxX;

  public:
  FilterResampleKeepXRange(double min_x = 0, double max_x = 0);
  FilterResampleKeepXRange(const FilterResampleKeepXRange &other);
  virtual ~FilterResampleKeepXRange(){};

  FilterResampleKeepXRange &operator=(const FilterResampleKeepXRange &other);

  Trace &filter(Trace &trace) const override;
};


using SelectionPolygonSpecVector = std::vector<SelectionPolygonSpec>;

class PMSPP_LIB_DECL FilterResampleKeepPointInPolygon : public FilterInterface
{
  public:
  FilterResampleKeepPointInPolygon();

  FilterResampleKeepPointInPolygon(const SelectionPolygon &selection_polygon,
                                   DataKind data_kind);

  FilterResampleKeepPointInPolygon(
    const SelectionPolygonSpecVector &selection_polygon_specs);

  FilterResampleKeepPointInPolygon(
    const FilterResampleKeepPointInPolygon &other);

  virtual ~FilterResampleKeepPointInPolygon(){};

  void
  newSelectionPolygonSpec(const SelectionPolygonSpec &selection_polygon_spec);

  FilterResampleKeepPointInPolygon &
  operator=(const FilterResampleKeepPointInPolygon &other);

  using FilterInterface::filter;

  Trace &filter(Trace &trace) const override;
  Trace &filter(Trace &trace, double dt_value, double rt_value) const;

  private:
  std::vector<SelectionPolygonSpec> m_selectionPolygonSpecs;
  double m_lowestMz   = std::numeric_limits<double>::max();
  double m_greatestMz = std::numeric_limits<double>::min();
};


class PMSPP_LIB_DECL MassSpectrumFilterResampleRemoveMzRange
  : public MassSpectrumFilterInterface
{
  private:
  const FilterResampleRemoveXRange m_filterRange;

  public:
  MassSpectrumFilterResampleRemoveMzRange(const MzRange &mz_range);
  MassSpectrumFilterResampleRemoveMzRange(
    const MassSpectrumFilterResampleRemoveMzRange &other);
  virtual ~MassSpectrumFilterResampleRemoveMzRange(){};
  MassSpectrum &filter(MassSpectrum &spectrum) const override;
};


class PMSPP_LIB_DECL MassSpectrumFilterResampleKeepMzRange
  : public MassSpectrumFilterInterface
{
  private:
  const FilterResampleKeepXRange m_filterRange;

  public:
  MassSpectrumFilterResampleKeepMzRange(const MzRange &mz_range);
  MassSpectrumFilterResampleKeepMzRange(
    const MassSpectrumFilterResampleKeepMzRange &other);
  virtual ~MassSpectrumFilterResampleKeepMzRange(){};
  MassSpectrum &filter(MassSpectrum &spectrum) const override;
};


} // namespace pappso
