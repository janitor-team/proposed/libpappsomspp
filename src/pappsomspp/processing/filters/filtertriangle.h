/**
 * \file pappsomspp/filers/filtertriangle.h
 * \date 27/10/2019
 * \author Olivier Langella
 * \brief sum peaks under a triangle base
 */

/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include "filterinterface.h"

#include "../../trace/trace.h"

namespace pappso
{
/**
 * @todo sum peaks under a triangle base
 */
class PMSPP_LIB_DECL FilterTriangle : public FilterInterface
{
  public:
  /**
   * Default constructor
   */
  FilterTriangle();

  /**
   * Copy constructor
   *
   * @param other TODO
   */
  FilterTriangle(const FilterTriangle &other);

  /**
   * Destructor
   */
  ~FilterTriangle();

  double setTriangleSlope(double intensity, double mz);
  Trace &filter(Trace &data_points) const override;


  private:
  DataPoint sumAndRemove(Trace &trace, const DataPoint &max_intensity) const;

  private:
  double m_triangleSlope;
  double m_maxMzRange = 0;
};
} // namespace pappso
