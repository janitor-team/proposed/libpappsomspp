/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2021 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


#include <QObject>

#include "../../trace/trace.h"
#include "../../exportinmportconfig.h"
#include "filternameinterface.h"


namespace pappso
{


class FilterFloorAmplitudePercentage;

typedef std::shared_ptr<FilterFloorAmplitudePercentage>
  FilterFloorAmplitudePercentageSPtr;
typedef std::shared_ptr<const FilterFloorAmplitudePercentage>
  FilterFloorAmplitudePercentageCstSPtr;


/**
 * @brief Redefines the floor intensity of the Trace
 *
 * The amplitude of the trace is computed (maxValue - minValue)
 * Its fraction is calculated = amplitude * (percentage / 100)
 * The threshold value is computed as (minValue + fraction)
 *
 * When the values to be filtered are below that threshold they acquire that
 * threshold value.
 *
 * When the values to be filtered are above that threshold they remain
 * unchanged.
 *
 * This effectively re-floors the values to threshold.
 */
class PMSPP_LIB_DECL FilterFloorAmplitudePercentage : public FilterNameInterface
{
  public:
  FilterFloorAmplitudePercentage(double percentage);
  FilterFloorAmplitudePercentage(const QString &parameters);
  FilterFloorAmplitudePercentage(const FilterFloorAmplitudePercentage &other);

  virtual ~FilterFloorAmplitudePercentage();

  FilterFloorAmplitudePercentage &
  operator=(const FilterFloorAmplitudePercentage &other);

  Trace &filter(Trace &data_points) const override;

  double getPercentage() const;
  QString name() const override;

  QString toString() const override;

  protected:
  void buildFilterFromString(const QString &strBuildParams) override;

  private:
  double m_percentage;
};
} // namespace pappso
