/**
 * \file pappsomspp/processing/filers/filtersuitestring.h
 * \date 17/11/2020
 * \author Olivier Langella
 * \brief build a filter suite from a string
 */

/*******************************************************************************
 * Copyright (c) 2020 Olivier Langella
 *<olivier.langella@universite-paris-saclay.fr>
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once


#include <vector>
#include "filternameinterface.h"
#include "../../exportinmportconfig.h"

namespace pappso
{
/**
 * @todo write docs
 */
class PMSPP_LIB_DECL FilterSuiteString : public FilterNameInterface
{
  public:
  /**
   * @param strBuildParams string to build the filter
   * "chargeDeconvolution|0.02dalton anotherFilter|param1;param2"
   */
  FilterSuiteString(const QString &strBuildParams);
  /**
   * Destructor
   */
  virtual ~FilterSuiteString();

  pappso::Trace &filter(pappso::Trace &data_points) const override;

  virtual QString name() const override;
  QString toString() const override;

  /** @brief takes a string that describes filters to add
   * @param strBuildParams string to build the filter
   * "chargeDeconvolution|0.02dalton anotherFilter|param1;param2"
   */
  void addFilterFromString(const QString &strBuildParams);

  using FilterNameType = std::vector<FilterNameInterfaceCstSPtr>;

  FilterNameType::const_iterator begin();
  FilterNameType::const_iterator end();

  protected:
  void buildFilterFromString(const QString &strBuildParams) override;

  private:
  std::vector<FilterNameInterfaceCstSPtr> m_filterVector;
};

typedef std::shared_ptr<FilterSuiteString> FilterSuiteStringSPtr;
} // namespace pappso
