/**
 * \file pappsomspp/processing/filers/filtersuitestring.cpp
 * \date 17/11/2020
 * \author Olivier Langella
 * \brief build a filter suite from a string
 */

/*******************************************************************************
 * Copyright (c) 2020 Olivier Langella
 *<olivier.langella@universite-paris-saclay.fr>
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "filtersuitestring.h"
#include "filterchargedeconvolution.h"
#include "filtercomplementionenhancer.h"
#include "filterpass.h"
#include "filtermorpho.h"
#include <QStringList>
#include <QDebug>
#include "../../exception/exceptionnotrecognized.h"
#include "filterexclusionmz.h"


namespace pappso
{
FilterSuiteString::FilterSuiteString(const QString &strBuildParams)
{
  buildFilterFromString(strBuildParams);
}

FilterSuiteString::~FilterSuiteString()
{
}


pappso::Trace &
FilterSuiteString::filter(pappso::Trace &data_points) const
{

  qDebug();
  for(auto &&filter : m_filterVector)
    {

      qDebug() << filter.get();
      qDebug() << filter->toString();
      filter->filter(data_points);
    }

  qDebug();
  return data_points;
}


QString
pappso::FilterSuiteString::name() const
{
  // FIXME: check if this makes sense
  return "Suite of filters";
}


QString
FilterSuiteString::toString() const
{
  QStringList filter_str_list;
  for(auto &&filter : m_filterVector)
    {
      filter_str_list << filter->toString();
    }

  return filter_str_list.join(" ");
}


void
FilterSuiteString::buildFilterFromString(const QString &strBuildParams)
{
  // qInfo() << strBuildParams;
  QStringList filters = strBuildParams.split(" ", Qt::SkipEmptyParts);
  for(QString filter_str : filters)
    {
      if(filter_str.startsWith("complementIonEnhancer|"))
        {
          m_filterVector.push_back(
            std::make_shared<FilterComplementIonEnhancer>(filter_str));
        }
      else if(filter_str.startsWith("chargeDeconvolution|"))
        {
          m_filterVector.push_back(
            std::make_shared<FilterChargeDeconvolution>(filter_str));
        }
      else if(filter_str.startsWith("mzExclusion|"))
        {
          m_filterVector.push_back(
            std::make_shared<FilterMzExclusion>(filter_str));
        }
      else if(filter_str.startsWith("passQuantileBasedRemoveY|"))
        {
          m_filterVector.push_back(
            std::make_shared<FilterQuantileBasedRemoveY>(filter_str));
        }

      else if(filter_str.startsWith("antiSpike|"))
        {
          m_filterVector.push_back(
            std::make_shared<FilterMorphoAntiSpike>(filter_str));
        }
      else
        {
          throw pappso::ExceptionNotRecognized(
            QString("building Filter from string %1 is "
                    "not possible")
              .arg(filter_str));
        }
    }
}

void
FilterSuiteString::addFilterFromString(const QString &strBuildParams)
{
  buildFilterFromString(strBuildParams);
}


FilterSuiteString::FilterNameType::const_iterator
FilterSuiteString::begin()
{
  return m_filterVector.begin();
};
FilterSuiteString::FilterNameType::const_iterator
FilterSuiteString::end()
{
  return m_filterVector.end();
};

} // namespace pappso
