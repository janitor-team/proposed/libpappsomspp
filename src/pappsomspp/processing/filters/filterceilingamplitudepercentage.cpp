/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2021 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#include <qmath.h>


#include <QVector>
#include <QDebug>

#include "../../exception/exceptionnotrecognized.h"

#include "filterceilingamplitudepercentage.h"


namespace pappso
{


FilterCeilingAmplitudePercentage::FilterCeilingAmplitudePercentage(
  double percentage)
{
  m_percentage = percentage;
}


FilterCeilingAmplitudePercentage::FilterCeilingAmplitudePercentage(
  const FilterCeilingAmplitudePercentage &other)
{
  m_percentage = other.m_percentage;
}


FilterCeilingAmplitudePercentage::~FilterCeilingAmplitudePercentage()
{
}

FilterCeilingAmplitudePercentage &
FilterCeilingAmplitudePercentage::operator=(
  const FilterCeilingAmplitudePercentage &other)
{
  if(&other == this)
    return *this;

  m_percentage = other.m_percentage;

  return *this;
}


FilterCeilingAmplitudePercentage::FilterCeilingAmplitudePercentage(
  const QString &parameters)
{
  buildFilterFromString(parameters);
}


void
FilterCeilingAmplitudePercentage::buildFilterFromString(const QString &parameters)
{
  // Typical string: "CeilingAmplitudePercentage|15"
  if(parameters.startsWith(QString("%1|").arg(name())))
    {
      QStringList params = parameters.split("|").back().split(";");

      m_percentage = params.at(0).toDouble();
    }
  else
    {
      throw pappso::ExceptionNotRecognized(
        QString(
          "Building of FilterCeilingAmplitudePercentage from string %1 failed")
          .arg(parameters));
    }
}


Trace &
FilterCeilingAmplitudePercentage::filter(Trace &data_points) const
{

  auto it_min = minYDataPoint(data_points.begin(), data_points.end());
  auto it_max = maxYDataPoint(data_points.begin(), data_points.end());

  if(it_min == data_points.end() || it_max == data_points.end())
    return data_points;

  double min = it_min->y;
  double max = it_max->y;

  double amplitude = max - min;

  double amplitude_ratio = amplitude * m_percentage / 100;

  double threshold = min + amplitude_ratio;

  // Since we never remove points, we only change their y value, we can do the
  // filtering inplace.

  for(auto &&data_point : data_points)
    {
      // Change the value to be threshold (re-ceiling in action).
      if(data_point.y > threshold)
        {
          data_point.y = threshold;
        }
    }

  return data_points;
}


double
FilterCeilingAmplitudePercentage::getPercentage() const
{
  return m_percentage;
}


//! Return a string with the textual representation of the configuration data.
QString
FilterCeilingAmplitudePercentage::toString() const
{
  return QString("%1|%2").arg(name()).arg(
    QString::number(m_percentage, 'f', 2));
}


QString
FilterCeilingAmplitudePercentage::name() const
{
  return "CeilingAmplitudePercentage";
}

} // namespace pappso
