/**
 * \file pappsomspp/processing/filers/filternameinterface.h
 * \date 31/10/2020
 * \author Olivier Langella
 * \brief interface to allow filter object construction from strings
 */

/*******************************************************************************
 * Copyright (c) 2020 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <QString>
#include "../../exportinmportconfig.h"
#include "filterinterface.h"


namespace pappso
{


/** @brief Interface that allows to build filter objects from strings
 */
class PMSPP_LIB_DECL FilterNameInterface : public FilterInterface
{
  public:
  virtual QString name() const = 0;
  virtual QString toString() const = 0;

  virtual ~FilterNameInterface(){};

  protected:
  /** @brief build this filter using a string
   * @param strBuildParams a string coding the filter and its parameters
   * "filterName|param1;param2;param3"
   *
   */
  virtual void buildFilterFromString(const QString &strBuildParams) = 0;
};


typedef std::shared_ptr<FilterNameInterface> FilterNameInterfaceSPtr;
typedef std::shared_ptr<const FilterNameInterface> FilterNameInterfaceCstSPtr;

} // namespace pappso
