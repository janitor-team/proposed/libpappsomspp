/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2021 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


#include <QObject>

#include "../../trace/trace.h"
#include "../../exportinmportconfig.h"
#include "filternameinterface.h"


namespace pappso
{


class FilterCeilingAmplitudePercentage;

typedef std::shared_ptr<FilterCeilingAmplitudePercentage> FilterCeilingAmplitudePercentageSPtr;
typedef std::shared_ptr<const FilterCeilingAmplitudePercentage> FilterCeilingAmplitudePercentageCstSPtr;


/**
 * @brief Redefines the ceiling intensity of the Trace
 *
 * The amplitude of the trace is computed (maxValue - minValue)
 * Its fraction is calculated = amplitude * (percentage / 100)
 * The threshold value is computed as (minValue + fraction)
 *
 * When the values to be filtered are above that threshold they acquire that
 * threshold value. 
 *
 * When the values to be filtered are below that threshold they remain
 * unchanged.
 *
 * This effectively re-ceilings the values to threshold.
 */
class PMSPP_LIB_DECL FilterCeilingAmplitudePercentage : public FilterNameInterface
{
  public:
  FilterCeilingAmplitudePercentage(double percentage);
  FilterCeilingAmplitudePercentage(const QString &parameters);
  FilterCeilingAmplitudePercentage(const FilterCeilingAmplitudePercentage &other);

  virtual ~FilterCeilingAmplitudePercentage();

  FilterCeilingAmplitudePercentage &operator=(const FilterCeilingAmplitudePercentage &other);

  Trace &filter(Trace &data_points) const override;

  double getPercentage() const;
  QString name() const override;

  QString toString() const override;

  protected:
  void buildFilterFromString(const QString &strBuildParams) override;

  private:
  double m_percentage;
};
} // namespace pappso
