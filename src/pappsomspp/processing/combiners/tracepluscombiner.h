#pragma once

#include <vector>
#include <memory>

#include <QDataStream>

#include "../../exportinmportconfig.h"
#include "tracecombiner.h"
#include "../../types.h"
#include "../../trace/maptrace.h"
#include "../../trace/trace.h"
#include "../../trace/datapoint.h"
#include "../../mzrange.h"


namespace pappso
{

class TracePlusCombiner;

typedef std::shared_ptr<const TracePlusCombiner> TracePlusCombinerCstSPtr;
typedef std::shared_ptr<TracePlusCombiner> TracePlusCombinerSPtr;


class PMSPP_LIB_DECL TracePlusCombiner : public TraceCombiner
{

  friend class MassSpectrumPlusCombiner;

  public:
  TracePlusCombiner();
  TracePlusCombiner(int decimal_places);
  TracePlusCombiner(const TracePlusCombiner &other);
  TracePlusCombiner(TracePlusCombinerCstSPtr other);

  virtual ~TracePlusCombiner();

  virtual MapTrace &combine(MapTrace &map_trace,
                                           const Trace &trace) const override;

  MapTrace &combine(MapTrace &map_trace_out,
                                   const MapTrace &map_trace_in) const override;
};


} // namespace pappso
