#pragma once


#include <vector>
#include <memory>

#include <QDataStream>

#include "../../exportinmportconfig.h"
#include "../../types.h"
#include "../../massspectrum/massspectrum.h"
#include "massspectrumcombiner.h"

namespace pappso
{

class MassSpectrumMinusCombiner;

typedef std::shared_ptr<const MassSpectrumMinusCombiner>
  MassSpectrumMinusCombinerCstSPtr;
typedef std::shared_ptr<MassSpectrumMinusCombiner>
  MassSpectrumMinusCombinerSPtr;


class PMSPP_LIB_DECL MassSpectrumMinusCombiner : public MassSpectrumCombiner
{

  public:
  MassSpectrumMinusCombiner();
  MassSpectrumMinusCombiner(int decimal_places);
  MassSpectrumMinusCombiner(MassSpectrumMinusCombinerCstSPtr other);
  MassSpectrumMinusCombiner(const MassSpectrumMinusCombiner &other);

  virtual ~MassSpectrumMinusCombiner();

  MassSpectrumMinusCombiner &operator=(const MassSpectrumMinusCombiner &other);

  virtual MapTrace &combine(MapTrace &map_trace,
                            const Trace &trace) const override;
  virtual MapTrace &combine(MapTrace &map_trace_out,
                            const MapTrace &map_trace_in) const override;
};


} // namespace pappso
