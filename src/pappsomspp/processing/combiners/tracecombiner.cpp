#include <numeric>
#include <limits>
#include <vector>
#include <map>
#include <cmath>
#include <algorithm>

#include <QDebug>

#include "tracecombiner.h"
#include "../../types.h"
#include "../../utils.h"
#include "../../pappsoexception.h"
#include "../../exception/exceptionoutofrange.h"


namespace pappso
{


TraceCombiner::TraceCombiner()
{
}


TraceCombiner::TraceCombiner(int decimal_places)
  : MassDataCombinerInterface(decimal_places)
{
}


TraceCombiner::TraceCombiner(const TraceCombiner &other)
  : MassDataCombinerInterface(other.m_decimalPlaces)
{
}


TraceCombiner::TraceCombiner(TraceCombinerCstSPtr other)
  : MassDataCombinerInterface(other->m_decimalPlaces)
{
}


TraceCombiner::~TraceCombiner()
{
}



} // namespace pappso
