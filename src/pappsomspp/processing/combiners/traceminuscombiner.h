#pragma once

#include <vector>
#include <memory>

#include <QDataStream>

#include "../../exportinmportconfig.h"
#include "tracecombiner.h"
#include "../../types.h"
#include "../../trace/maptrace.h"
#include "../../trace/trace.h"
#include "../../trace/datapoint.h"
#include "../../mzrange.h"


namespace pappso
{

class TraceMinusCombiner;

typedef std::shared_ptr<const TraceMinusCombiner> TraceMinusCombinerCstSPtr;
typedef std::shared_ptr<TraceMinusCombiner> TraceMinusCombinerSPtr;


class PMSPP_LIB_DECL TraceMinusCombiner : public TraceCombiner
{

  friend class MassSpectrumMinusCombiner;

  protected:
  public:
  TraceMinusCombiner();
  TraceMinusCombiner(int decimal_places);
  TraceMinusCombiner(const TraceMinusCombiner &other);
  TraceMinusCombiner(TraceMinusCombinerCstSPtr other);

  virtual ~TraceMinusCombiner();

  virtual MapTrace &combine(MapTrace &map_trace,
                                           const Trace &trace) const override;

  MapTrace &combine(MapTrace &map_trace_out,
                                   const MapTrace &map_trace_in) const override;
};


} // namespace pappso
