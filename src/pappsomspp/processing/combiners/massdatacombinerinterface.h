
#pragma once

/////////////////////// StdLib includes


/////////////////////// Qt includes


/////////////////////// pappsomspp includes


/////////////////////// Local includes
#include "../../types.h"
#include "selectionpolygon.h"
#include "../../trace/maptrace.h"
#include "../../massspectrum/qualifiedmassspectrum.h"
#include "../filters/filterresample.h"
#include "../../exportinmportconfig.h"


namespace pappso
{

class PMSPP_LIB_DECL MassDataCombinerInterface
{

  public:
  MassDataCombinerInterface(int decimal_places = -1);
  virtual ~MassDataCombinerInterface();

  void setDecimalPlaces(int value);
  int getDecimalPlaces() const;

  using Iterator = std::vector<const Trace *>::const_iterator;
  virtual MapTrace &combine(MapTrace &map_trace, Iterator begin, Iterator end);

  virtual MapTrace &combine(MapTrace &map_trace, const Trace &trace) const = 0;
  virtual MapTrace &combine(MapTrace &map_trace_out,
                            const MapTrace &map_trace_in) const            = 0;

  protected:
  //! Number of decimals to use for the keys (x values)
  int m_decimalPlaces = -1;
};


} // namespace pappso
