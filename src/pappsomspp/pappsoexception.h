/**
 * \file pappsomspp/pappsoexception.cpp
 * \date 10/3/2015
 * \author Olivier Langella
 * \brief exception handler
 */

/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/


#pragma once

#include <exception>
#include <QString>
#include <QDebug>
#include <QException>

namespace pappso
{
class PappsoException : public QException
{
  public:
  PappsoException(const QString &message) throw()
  {
    qDebug() << message;
    m_message = message;
    m_stdMessage.assign(m_message.toStdString());
  }

  PappsoException(const PappsoException &other) throw()
  {
    m_message = other.m_message;
    m_stdMessage.assign(m_message.toStdString());
  }
  void
  raise() const override
  {
    throw *this;
  }
  virtual QException *
  clone() const override
  {
    return new PappsoException(*this);
  }

  virtual const QString &
  qwhat() const throw()
  {
    return m_message;
  }

  const char *
  what() const noexcept override
  {
    return m_stdMessage.c_str();
  }

  virtual ~PappsoException() throw()
  {
  }

  private:
  std::string m_stdMessage;
  QString m_message; // Description of the error
};
} // namespace pappso
