
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/
#include "peptidebuilder.h"
#include <QDebug>

namespace pappso
{
PeptideBuilder::PeptideBuilder()
{
}

PeptideBuilder::~PeptideBuilder()
{
}
void
PeptideBuilder::addFixedAaModification(char aa, AaModificationP modification)
{

  m_fixedModificationList.push_back(
    std::pair<char, AaModificationP>(aa, modification));
}

void
PeptideBuilder::setPeptide(std::int8_t sequence_database_id,
                           const ProteinSp &protein_sp,
                           bool is_decoy,
                           const QString &peptide_str,
                           unsigned int start,
                           bool is_nter,
                           unsigned int missed_cleavage_number,
                           bool semi_enzyme)
{

  qDebug() << "PeptideBuilder::setPeptide begin";

  Peptide peptide(peptide_str);

  for(auto &&aamod_pair : m_fixedModificationList)
    {
      std::vector<unsigned int> position_list =
        peptide.getAaPositionList(aamod_pair.first);
      for(auto &&position : position_list)
        {
          peptide.addAaModification(aamod_pair.second, position);
        }
    }

  PeptideSp peptide_sp = peptide.makePeptideSp();
  // set fixed modifications

  qDebug() << "PeptideBuilder::setPeptide m_sink->setPeptideSp";
  m_sink->setPeptideSp(sequence_database_id,
                       protein_sp,
                       is_decoy,
                       peptide_sp,
                       start,
                       is_nter,
                       missed_cleavage_number,
                       semi_enzyme);
  qDebug() << "PeptideBuilder::setPeptide end";
}

} // namespace pappso
