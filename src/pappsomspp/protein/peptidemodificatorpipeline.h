/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once


#include "enzymeproductinterface.h"
#include "peptidemodificatortee.h"

namespace pappso
{

class PMSPP_LIB_DECL PeptideModificatorPipeline
  : public PeptideModificatorInterface,
    public PeptideSpSinkInterface,
    public EnzymeProductInterface
{
  public:
  PeptideModificatorPipeline();
  PeptideModificatorPipeline(const PeptideModificatorPipeline &other);
  virtual ~PeptideModificatorPipeline();

  void addFixedModificationString(const QString &mod_str);
  // protein Nter modification
  void addFixedNterModificationString(const QString &mod_str);
  // protein Cter modification
  void addFixedCterModificationString(const QString &mod_str);

  void addPotentialModificationString(const QString &mod_str);
  // protein Nter modification
  void addPotentialNterModificationString(const QString &mod_str);
  // protein Cter modification
  void addPotentialCterModificationString(const QString &mod_str);

  void setSink(PeptideModificatorInterface *sink) override;

  void setPeptideSp(std::int8_t sequence_database_id,
                    const ProteinSp &protein_sp,
                    bool is_decoy,
                    const PeptideSp &peptide_sp_original,
                    unsigned int start,
                    bool is_nter,
                    unsigned int missed_cleavage_number,
                    bool semi_enzyme) override;
  void setPeptide(std::int8_t sequence_database_id,
                  const ProteinSp &protein_sp,
                  bool is_decoy,
                  const QString &peptide_str,
                  unsigned int start,
                  bool is_nter,
                  unsigned int missed_cleavage_number,
                  bool semi_enzyme) override;

  void addLabeledModificationString(const QString &mod_str);


  private:
  PeptideModificatorTee *mp_peptideModificatorTee = nullptr;

  PeptideModificatorInterface *m_sink = nullptr;

  PeptideSpSinkInterface *mp_lastPeptideSinkInterface;

  PeptideModificatorInterface *mp_firstModificator = nullptr;

  std::vector<PeptideModificatorInterface *> m_pepModificatorPtrList;

  void parseFixedModification(const QString &mod_str,
                              bool Nter,
                              bool Cter,
                              bool else_prot);
  void privAddFixedModificationString(const QString &mod_str,
                                      bool Nter,
                                      bool Cter,
                                      bool else_prot);
  void parsePotentialModification(const QString &mod_str,
                                  bool Nter,
                                  bool Cter,
                                  bool else_prot);
  void privAddPotentialModificationString(const QString &mod_str,
                                          bool Nter,
                                          bool Cter,
                                          bool else_prot);
  void parseLabeledModification(const QString &mod_str,
                                bool Nter,
                                bool Cter,
                                bool else_prot);
};


} // namespace pappso
