/**
 * \file pappsomspp/protein/protein.h
 * \date 2/7/2015
 * \author Olivier Langella
 * \brief object to handle a protein
 */

/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/


#pragma once

#include <QString>
#include <memory>
#include "../types.h"
#include "../exportinmportconfig.h"

namespace pappso
{


class Protein;

/** \brief shared pointer on a Protein object
 */
typedef std::shared_ptr<const Protein> ProteinSp;

class PMSPP_LIB_DECL Protein
{
  private:
  /** \brief  free text to describe the protein */
  QString m_description;
  /** \brief  a single unique identifier of the protein (usually the first word
   * of description) */
  QString m_accession;
  /** \brief the amino acid sequence */
  QString m_sequence;
  /** \brief number of amino acid */
  unsigned int m_length = 0;

  static QRegularExpression m_removeTranslationStopRegExp;

  public:
  Protein();
  Protein(const QString &description, const QString &sequence);
  Protein(const Protein &protein);
  ProteinSp makeProteinSp() const;

  bool operator==(const Protein &other) const;
  ~Protein();

  const QString &getSequence() const;

  void setSequence(const QString &sequence);

  const QString &getAccession() const;

  virtual void setAccession(const QString &accession);

  const QString &getDescription() const;

  void setDescription(const QString &description);

  /** \brief remove * characters at the end of the sequence
   */
  Protein &removeTranslationStop();

  /** \brief reverse characters in the sequence
   */
  Protein &reverse();

  /** \brief protein amino acid sequence size
   */
  unsigned int size() const;

  /** \brief get monoisotopic mass of ProteinSp Protein::makeProteinSp() const
   */
  pappso_double getMass() const;
};


} // namespace pappso
