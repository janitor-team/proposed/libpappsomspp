/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include "enzymeproductinterface.h"
#include <QRegularExpression>

namespace pappso
{
class PMSPP_LIB_DECL Enzyme
{
  public:
  /** \brief build the default enzyme (trypsin) with recognition_site =
   * "([KR])([^P])"
   * */
  Enzyme();

  /** \brief build any enzyme given a recognition_site
   * \param recognition_site is a regular expression that must identify 2 motifs
   * : one on Nter side one on Cter side
   * */
  Enzyme(const QString &recognition_site);
  ~Enzyme();

  /** \brief digest a protein into enzyme products
   * \param sequence_database_id integer that references the sequence fatabase
   * (file, stream, url...) \param protein_sp is the original protein to be
   * digested \param is_decoy tell if the current protein is a decoy (true) or
   * normal (false) protein \param enzyme_product is the object that will
   * receive the digestion products
   * */
  void eat(std::int8_t sequence_database_id,
           const ProteinSp &protein_sp,
           bool is_decoy,
           EnzymeProductInterface &enzyme_product) const;

  /** \brief sets the maximum number of missed cleavage allowed in the digestion
   * \param miscleavage maximum number of missed cleavade to allow (defaults is
   * 0)
   * */
  void setMiscleavage(unsigned int miscleavage);

  /** \brief get the maximum number of missed cleavage allowed in the digestion
   * @return miscleavage maximum number of missed cleavade to allow (defaults is
   * 0)
   * */
  unsigned int getMiscleavage() const;


  /** \brief take only first m_takeOnlyFirstWildcard
   * \param bool  true : switch to take only the first possibility if there are
   * X, B or Z wildcards in sequence
   */
  void setTakeOnlyFirstWildcard(bool take_only_first_wildcard);

  /** \brief if there are wildcards in the protein sequence : restrict the
   * number of possible peptide sequences \param max_peptide_variant_list_size
   * maximum number of peptide variant (default is 100)
   */
  void setMaxPeptideVariantListSize(std::size_t max_peptide_variant_list_size);


  const QRegularExpression &getQRegExpRecognitionSite() const;


  private:
  /** \brief example with a kinase == [K,R] */
  QRegularExpression m_recognitionSite;
  unsigned int m_miscleavage   = 0;
  bool m_takeOnlyFirstWildcard = false;

  std::size_t m_maxPeptideVariantListSize = 100;


  std::vector<char> m_wildCardX;
  std::vector<char> m_wildCardB;
  std::vector<char> m_wildCardZ;

  void sanityCheck(EnzymeProductInterface &enzyme_product,
                   std::int8_t sequence_database_id,
                   const ProteinSp &protein_sp,
                   bool is_decoy,
                   const PeptideStr &peptide,
                   unsigned int start,
                   bool is_nter,
                   unsigned int missed_cleavage_number,
                   bool semi_enzyme) const;
  void replaceWildcards(std::vector<std::string> *p_peptide_variant_list) const;
};

} // namespace pappso
