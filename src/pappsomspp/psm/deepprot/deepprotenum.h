/**
 * \file pappsomspp/psm/deepprot/deepprotenum.h
 * \date 22/1/2021
 * \author Olivier Langella <olivier.langella@universite-paris-saclay.fr>
 * \brief base type definition to use in DeepProt
 *
 * DeepProt is the C++ implementation of the SpecOMS algorithm
 *
 */

/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella
 *<olivier.langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once
#include "../../exportinmportconfig.h"
#include <QString>

namespace pappso
{

/** @brief definition of different class of PSMs used by DeepProt
 */
enum class DeepProtMatchType : std::uint8_t
{
  uncategorized = 0, ///< precursor mass was not compared
  ZeroMassDelta = 1, ///< peptide candidate is in precursor mz range
  ZeroMassDeltaMissedCleavage =
    2, ///< peptide candidate with missed cleavage is in precursor mz range
  ZeroMassDeltaSemiTryptic =
    3, ///< semi tryptic peptide candidate is in precursor mz range
  DeltaPosition = 4,   ///< spectrum shifter on peptide candidate has detected a
                       ///< position for the mass delta
  NoDeltaPosition = 5, ///< spectrum shifter on peptide candidate has not
                       ///< detected a position for the mass delta
  last = 6
};


/** @brief definition of different status for potential peptide candidates on
 * the same spectrum
 */
enum class DeepProtPeptideCandidateStatus : std::uint8_t
{
  unmodified      = 0, ///< precursor mass was not compared
  ZeroMassDelta   = 1,
  CterRemoval     = 2,
  NterRemoval     = 3,
  MissedCleavage  = 4,
  DeltaPosition   = 5,
  NoDeltaPosition = 6,
  last            = 7
};

class PMSPP_LIB_DECL DeepProtEnumStr
{
  public:
  static const QString toString(DeepProtMatchType match_type);
  static const QString toString(DeepProtPeptideCandidateStatus status);

  static DeepProtPeptideCandidateStatus
  DeepProtPeptideCandidateStatusFromString(const QString &name);
  static DeepProtMatchType DeepProtMatchTypeFromString(const QString &name);
};

} // namespace pappso


