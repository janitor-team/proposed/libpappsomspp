/*
 * *******************************************************************************
 * * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 * *
 * * This file is part of MassChroqPRM.
 * *
 * *     MassChroqPRM is free software: you can redistribute it and/or modify
 * *     it under the terms of the GNU General Public License as published by
 * *     the Free Software Foundation, either version 3 of the License, or
 * *     (at your option) any later version.
 * *
 * *     MassChroqPRM is distributed in the hope that it will be useful,
 * *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 * *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * *     GNU General Public License for more details.
 * *
 * *     You should have received a copy of the GNU General Public License
 * *     along with MassChroqPRM.  If not, see <http://www.gnu.org/licenses/>.
 * *
 * * Contributors:
 * *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 * implementation
 * ******************************************************************************/

#include "peakionisotopematch.h"

namespace pappso
{
std::vector<PeakIonIsotopeMatch>::iterator
findComplementIonType(std::vector<PeakIonIsotopeMatch>::iterator begin,
                      std::vector<PeakIonIsotopeMatch>::iterator end,
                      const PeakIonIsotopeMatch &peak_ion,
                      std::size_t peptide_size)
{

  return std::find_if(
    begin,
    end,
    [peak_ion, peptide_size](const PeakIonIsotopeMatch &to_compare) {
      if(to_compare.getCharge() == peak_ion.getCharge())
        {
          if((to_compare.getPeptideFragmentIonSp().get()->size() +
              peak_ion.getPeptideFragmentIonSp().get()->size()) == peptide_size)
            {
              if(peptideIonTypeIsComplement(to_compare.getPeptideIonType(),
                                            peak_ion.getPeptideIonType()))
                {
                  if(to_compare.getPeptideNaturalIsotopeAverageSp()
                       .get()
                       ->getIsotopeNumber() ==
                     peak_ion.getPeptideNaturalIsotopeAverageSp()
                       .get()
                       ->getIsotopeNumber())
                    {
                      return true;
                    }
                }
            }
        }

      return false;
    });
}


PeakIonIsotopeMatch::PeakIonIsotopeMatch(
  const DataPoint &peak,
  const PeptideNaturalIsotopeAverageSp &naturalIsotopeAverageSp,
  const PeptideFragmentIonSp &ion_sp)
  : PeakIonMatch(peak, ion_sp, naturalIsotopeAverageSp.get()->getCharge()),
    _naturalIsotopeAverageSp(naturalIsotopeAverageSp)
{
  qDebug();
}

PeakIonIsotopeMatch::PeakIonIsotopeMatch(const PeakIonIsotopeMatch &other)
  : PeakIonMatch(other)
{
  _naturalIsotopeAverageSp = other._naturalIsotopeAverageSp;
}


PeakIonIsotopeMatch::PeakIonIsotopeMatch(PeakIonIsotopeMatch &&other)
  : PeakIonMatch(std::move(other))
{
  _naturalIsotopeAverageSp = other._naturalIsotopeAverageSp;
}

PeakIonIsotopeMatch::~PeakIonIsotopeMatch()
{
}
const PeptideNaturalIsotopeAverageSp &
PeakIonIsotopeMatch::getPeptideNaturalIsotopeAverageSp() const
{
  return _naturalIsotopeAverageSp;
}


PeakIonIsotopeMatch &
PeakIonIsotopeMatch::operator=(const PeakIonIsotopeMatch &other)
{
  PeakIonMatch::operator   =(other);
  _naturalIsotopeAverageSp = other._naturalIsotopeAverageSp;

  return *this;
}


QString
PeakIonIsotopeMatch::toString() const
{
  return QString("%1isotope%2r%3mz%4")
    .arg(PeakIonMatch::toString())
    .arg(_naturalIsotopeAverageSp.get()->getIsotopeNumber())
    .arg(_naturalIsotopeAverageSp.get()->getIsotopeRank())
    .arg(getPeak().x);
}

} // namespace pappso
