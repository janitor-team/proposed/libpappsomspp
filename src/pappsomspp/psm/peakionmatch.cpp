/**
 * \file pappsomspp/psm/peakionmatch.cpp
 * \date 4/4/2015
 * \author Olivier Langella
 * \brief associate a peak and a peptide + charge
 */

/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "peakionmatch.h"

namespace pappso
{
PeakIonMatch::PeakIonMatch(const DataPoint &peak,
                           const PeptideFragmentIonSp &ion_sp,
                           unsigned int charge)
  : _peak(peak), _ion_sp(ion_sp), _charge(charge)
{
}

PeakIonMatch::PeakIonMatch(const PeakIonMatch &other)
  : _peak(other._peak), _ion_sp(other._ion_sp), _charge(other._charge)
{
}

PeakIonMatch::PeakIonMatch(PeakIonMatch &&other)
  : _peak(std::move(other._peak)),
    _ion_sp(other._ion_sp),
    _charge(std::move(other._charge))
{
}

PeakIonMatch::~PeakIonMatch()
{
}


PeakIonMatch &
PeakIonMatch::operator=(const PeakIonMatch &other)
{
  _peak   = other._peak;
  _ion_sp = other._ion_sp;
  _charge = other._charge;

  return *this;
}

const PeptideFragmentIonSp &
PeakIonMatch::getPeptideFragmentIonSp() const
{
  return _ion_sp;
}

const DataPoint &
PeakIonMatch::getPeak() const
{
  return _peak;
}

unsigned int
PeakIonMatch::getCharge() const
{
  return _charge;
}

PeptideIon
PeakIonMatch::getPeptideIonType() const
{
  return _ion_sp.get()->getPeptideIonType();
}

PeptideDirection
PeakIonMatch::getPeptideIonDirection() const
{
  return _ion_sp.get()->getPeptideFragmentSp().get()->getPeptideIonDirection();
}

QString
PeakIonMatch::toString() const
{
  return QString("%1").arg(
    _ion_sp.get()->getCompletePeptideIonName(getCharge()));
}

} // namespace pappso
