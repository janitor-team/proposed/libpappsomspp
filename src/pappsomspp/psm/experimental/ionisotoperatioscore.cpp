/**
 * \file pappsomspp/psm/experimental/ionisotoperatioscore.cpp
 * \date 18/05/2019
 * \author Olivier Langella
 * \brief psm score computed using ion isotopes
 */

/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "ionisotoperatioscore.h"
#include "../peptideisotopespectrummatch.h"
#include "../../trace/linearregression.h"

using namespace pappso;

IonIsotopeRatioScore::IonIsotopeRatioScore(const MassSpectrum &spectrum,
                                           const PeptideSp &peptide_sp,
                                           unsigned int parent_charge,
                                           PrecisionPtr precision,
                                           std::vector<PeptideIon> ion_vector)
{
  std::list<PeptideIon> ion_list(ion_vector.begin(), ion_vector.end());
  PeptideIsotopeSpectrumMatch psm_match(
    spectrum, peptide_sp, parent_charge, precision, ion_list, 1, 1);

  Trace scaterplot;

  for(PeptideIon ion_type : ion_vector)
    {
      std::vector<double> mono_th_intensities(peptide_sp.get()->size(), 0);
      std::vector<double> isotope_th_intensities(peptide_sp.get()->size(), 0);

      std::vector<double> mono_exp_intensities(peptide_sp.get()->size(), 0);
      std::vector<double> isotope_exp_intensities(peptide_sp.get()->size(), 0);
      for(const PeakIonIsotopeMatch &peak_ion_match :
          psm_match.getPeakIonIsotopeMatchList())
        {
          if(peak_ion_match.getPeptideIonType() == ion_type)
            {
              std::size_t vector_position =
                peak_ion_match.getPeptideFragmentIonSp().get()->size() - 1;
              PeptideNaturalIsotopeAverageSp iso_average_sp =
                peak_ion_match.getPeptideNaturalIsotopeAverageSp();
              if(iso_average_sp.get()->getIsotopeNumber() == 0)
                {
                  mono_th_intensities[vector_position] =
                    iso_average_sp.get()->getIntensityRatio();
                  mono_exp_intensities[vector_position] =
                    peak_ion_match.getPeak().y;
                }
              else if(iso_average_sp.get()->getIsotopeNumber() == 1)
                {
                  isotope_th_intensities[vector_position] =
                    iso_average_sp.get()->getIntensityRatio();
                  isotope_exp_intensities[vector_position] =
                    peak_ion_match.getPeak().y;
                }
            }
        }

      for(std::size_t i = 0; i < mono_th_intensities.size(); i++)
        {
          if((mono_th_intensities[i] != 0) && (isotope_th_intensities[i] != 0))
            {
              DataPoint xy(mono_th_intensities[i] / isotope_th_intensities[i],
                           mono_exp_intensities[i] /
                             isotope_exp_intensities[i]);
              scaterplot.push_back(xy);
            }
        }
    }

  scaterplot.sortX();

  LinearRegression linear_regression(scaterplot);

  m_ionIsotopeRatioScore =
    linear_regression.getCoefficientOfDetermination(scaterplot);
}


IonIsotopeRatioScore::~IonIsotopeRatioScore()
{
}

pappso::pappso_double
IonIsotopeRatioScore::getIonIsotopeRatioScore() const
{
  return m_ionIsotopeRatioScore;
}
