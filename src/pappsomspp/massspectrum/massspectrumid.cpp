/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/


#include <limits>


#include "massspectrumid.h"

namespace pappso
{


MassSpectrumId::MassSpectrumId()
{
}


MassSpectrumId::MassSpectrumId(const MsRunIdCstSPtr &msRunId)
  : mcsp_msRunId(msRunId)
{
}


MassSpectrumId::MassSpectrumId(const MsRunIdCstSPtr &msRunId,
                               std::size_t spectrum_index)
  : mcsp_msRunId(msRunId), m_nativeId(""), m_spectrumIndex(spectrum_index)
{
}


MassSpectrumId::MassSpectrumId(const MassSpectrumId &other)
  : mcsp_msRunId(other.mcsp_msRunId),
    m_nativeId(other.m_nativeId),
    m_spectrumIndex(other.m_spectrumIndex)
{
}


MassSpectrumId::~MassSpectrumId()
{
}


MassSpectrumId &
MassSpectrumId::operator=(const MassSpectrumId &other)
{
  mcsp_msRunId    = other.mcsp_msRunId;
  m_spectrumIndex = other.m_spectrumIndex;
  m_nativeId      = other.m_nativeId;

  return *this;
}


void
MassSpectrumId::setMsRunId(MsRunIdCstSPtr other)
{

  mcsp_msRunId = other;
};


const MsRunIdCstSPtr &
MassSpectrumId::getMsRunIdCstSPtr() const
{
  return mcsp_msRunId;
};


void
MassSpectrumId::setNativeId(const QString &native_id)
{
  m_nativeId = native_id;
}


const QString &
MassSpectrumId::getNativeId() const
{
  return m_nativeId;
}


void
MassSpectrumId::setSpectrumIndex(std::size_t index)
{
  m_spectrumIndex = index;
}


std::size_t
MassSpectrumId::getSpectrumIndex() const
{
  return m_spectrumIndex;
}


bool
MassSpectrumId::operator==(const MassSpectrumId &other) const
{
  return (mcsp_msRunId == other.mcsp_msRunId &&
          m_spectrumIndex == other.m_spectrumIndex);
}

bool
MassSpectrumId::isValid() const
{
  return mcsp_msRunId->isValid() &&
         m_spectrumIndex != std::numeric_limits<std::size_t>::max();
}


QString
MassSpectrumId::toString() const
{
  return QString(
           "ms run id: %1 \n"
           "native id: %2 \n"
           "m_spectrumIndex: %3\n")
    .arg(mcsp_msRunId != nullptr ? mcsp_msRunId->toString() : "nullptr")
    .arg(m_nativeId)
    .arg(m_spectrumIndex);
}

} // namespace pappso
