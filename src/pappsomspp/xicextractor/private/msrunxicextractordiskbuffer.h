/**
 * \file pappsomspp/xicextractor/private/msrunxicextractordiskbuffer.h
 * \date 18/05/2018
 * \author Olivier Langella
 * \brief proteowizard based XIC extractor featuring disk cache + write buffer
 */

/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/


#pragma once


#include "msrunxicextractordisk.h"


class MsRunXicExtractorFactory;

namespace pappso
{

class MsRunXicExtractorDiskBuffer : public MsRunXicExtractorDisk
{
  friend MsRunXicExtractorFactory;

  public:
  MsRunXicExtractorDiskBuffer(const MsRunXicExtractorDiskBuffer &other);
  virtual ~MsRunXicExtractorDiskBuffer();

  protected:
  MsRunXicExtractorDiskBuffer(MsRunReaderSPtr &msrun_reader,
                              const QDir &temporary_dir);

  virtual void storeSlices(std::map<unsigned int, MassSpectrum> &slice_vector,
                           std::size_t ipos) override;
  void appendSliceInBuffer(unsigned int slice_number,
                           MassSpectrum &spectrum,
                           std::size_t ipos);

  void flushBufferOnDisk();
  virtual void endPwizRead() override;


  protected:
  std::map<unsigned int, QByteArray> m_sliceBufferMap;

  std::size_t m_bufferSize    = 0;
  std::size_t m_bufferMaxSize = 150;
};


} // namespace pappso
