/**
 * \file pappsomspp/xicextractor/private/msrunslice.h
 * \date 12/05/2018
 * \author Olivier Langella
 * \brief one mz slice (1 dalton) of an MsRun
 */

/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include "../../massspectrum/massspectrum.h"
#include <QDataStream>

namespace pappso
{

class MsRunSlice;
typedef std::shared_ptr<const MsRunSlice> MsRunSliceSPtr;

class MsRunSlice
{

  public:
  MsRunSlice();
  MsRunSlice(const MsRunSlice &other);
  virtual ~MsRunSlice();

  MsRunSliceSPtr makeMsRunSliceSp() const;

  void setSliceNumber(unsigned int slice_number);
  unsigned int getSliceNumber() const;

  void clear();
  std::size_t size() const;

  /** @brief set number of spectrum (mz/intensity) stored in this slice
   */
  void setSize(std::size_t size);

  void appendToStream(QDataStream &stream, std::size_t ipos) const;

  const MassSpectrum &getSpectrum(std::size_t i) const;
  MassSpectrum &getSpectrum(std::size_t i);

  /** @brief set the mass spectrum for a given index (retention time)
   */
  void setSpectrum(std::size_t i, const MassSpectrum &spectrum);


  private:
  unsigned int m_sliceNumber = 0;
  std::vector<MassSpectrum> m_spectrumList;
};

QDataStream &operator>>(QDataStream &instream, MsRunSlice &slice);


} // namespace pappso
