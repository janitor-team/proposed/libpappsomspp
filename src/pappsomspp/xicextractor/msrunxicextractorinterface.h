/**
 * \file pappsomspp/xicextractor/msrunxicextractor.h
 * \date 07/05/2018
 * \author Olivier Langella
 * \brief base interface to build XICs on an MsRun file
 */

/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once


#include "../msrun/msrunreader.h"
#include <memory>
#include <vector>
#include <limits>
#include <iterator>
#include "../mzrange.h"
#include "../xic/xic.h"
#include "../processing/uimonitor/uimonitorinterface.h"


namespace pappso
{

class MsRunXicExtractorInterface;
typedef std::shared_ptr<MsRunXicExtractorInterface>
  MsRunXicExtractorInterfaceSp;

class PMSPP_LIB_DECL MsRunXicExtractorInterface
{

  public:
  /** @brief set the XIC extraction method
   */
  void setXicExtractMethod(XicExtractMethod method); // sum or max

  /** @brief set the retention time range in seconds around the target rt
   *
   * only the interesting part of the xic will be extracted, form the rt target
   * - range_in_seconds to rt target + range in seconds by default, all the LC
   * run time is extracted
   *
   * @param range_in_seconds range in seconds
   */
  void setRetentionTimeAroundTarget(double range_in_seconds);


  /** @brief extract a list of XIC given a list of xic coordinates to extract
   *
   * XicCoord is a vessel containing the xic to fill and coordinates of this XIC
   * in the MS run
   *
   * @param monitor process monitoring
   * @param xic_coord_list list of xic coordinates to extract. The order of xic
   * coordinates may change.
   */
  virtual void
  extractXicCoordSPtrList(UiMonitorInterface &monitor,
                          std::vector<XicCoordSPtr> &xic_coord_list) final;


  /** @brief multithreaded XIC extraction
   *
   * divide xic_coord_list and run extractXicCoordSPtrList separated threads
   *
   * @param monitor process monitoring
   * @param xic_coord_list list of xic coordinates to extract. The order of xic
   * coordinates may change.
   */
  virtual void extractXicCoordSPtrListParallelized(
    UiMonitorInterface &monitor,
    std::vector<XicCoordSPtr> &xic_coord_list) final;

  const MsRunIdCstSPtr &getMsRunId() const;

  /** @brief get the msrunreader currently used for XIC extraction
   */
  const MsRunReaderSPtr &getMsRunReaderSPtr() const;

  /** @brief filter interface to apply just after XIC extration on each trace
   */
  void
  setPostExtractionTraceFilterCstSPtr(pappso::FilterInterfaceCstSPtr &filter);


  protected:
  /** @brief constructor is private, use the MsRunXicExtractorFactory
   */
  MsRunXicExtractorInterface(MsRunReaderSPtr &msrun_reader);
  MsRunXicExtractorInterface(const MsRunXicExtractorInterface &other);
  virtual ~MsRunXicExtractorInterface();

  /** @brief possible post extraction process, eventually trace filters
   */
  virtual void postExtractionProcess(UiMonitorInterface &monitor,
    std::vector<XicCoordSPtr>::iterator it_xic_coord_list_begin,
    std::vector<XicCoordSPtr>::iterator it_xic_coord_list_end);

  virtual void protectedExtractXicCoordSPtrList(
    UiMonitorInterface &monitor,
    std::vector<XicCoordSPtr>::iterator it_xic_coord_list_begin,
    std::vector<XicCoordSPtr>::iterator it_xic_coord_list_end) = 0;


  protected:
  MsRunReaderSPtr msp_msrun_reader;
  XicExtractMethod m_xicExtractMethod = XicExtractMethod::max;
  double m_retentionTimeAroundTarget  = std::numeric_limits<double>::max();
  pappso::FilterInterfaceCstSPtr mcsp_postExtractionTraceFilter = nullptr;
};


} // namespace pappso
