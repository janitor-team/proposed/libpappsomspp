#pragma once

#include <QtCore/QtGlobal>

namespace pappso
{
#if defined(PMSPP_LIBRARY)
//#pragma message("PMSPP_LIBRARY is defined: defining PMSPP_LIB_DECL
// Q_DECL_EXPORT")
#define PMSPP_LIB_DECL Q_DECL_EXPORT
#else
//#pragma message("PMSPP_LIBRARY is not defined: defining PMSPP_LIB_DECL
// Q_DECL_IMPORT")
#define PMSPP_LIB_DECL Q_DECL_IMPORT
#endif

} // namespace pappso
