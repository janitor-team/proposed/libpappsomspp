//
// File: test_uimonitor.cpp
// Created by: Olivier Langella
// Created on: 25/9/2021
//
/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


// make test ARGS="-V -I 1,1"

// ./tests/catch2-only-tests [uimonitor] -s


#include <catch2/catch.hpp>
#include <QDebug>
#include <QIODevice>
#include <pappsomspp/processing/uimonitor/uimonitortextpercent.h>
#include <iostream>

using namespace pappso;
using namespace std;


TEST_CASE("UiMonitor test suite.", "[uimonitor]")
{
  // Set the debugging message formatting pattern.
  qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));

  SECTION("..:: UiMonitorTextPercent ::..", "[uimonitor]")
  {
    QTextStream output(stdout, QIODevice::WriteOnly);
    UiMonitorTextPercent monitor(output);

    monitor.setTotalSteps(20);
    for(int i = 0; i < 20; i++)
      {
        monitor.count();
      }

    monitor.setTotalSteps(0);
    for(int i = 0; i < 20; i++)
      {
        monitor.count();
      }


    monitor.setTotalSteps(100);
    for(int i = 0; i < 20; i++)
      {
        monitor.count();
      }
    monitor.setTotalSteps(0);
  }
}
