#include <catch2/catch.hpp>
#include <QDebug>
#include <QString>
#include <iostream>
#include <pappsomspp/vendors/tims/timsframebase.h>
#include <pappsomspp/mzrange.h>
#include <pappsomspp/msfile/msfileaccessor.h>
#include <pappsomspp/msrun/private/timsmsrunreaderms2.h>
#include <pappsomspp/msrun/private/timsmsrunreader.h>
#include <pappsomspp/pappsoexception.h>
#include <pappsomspp/exception/exceptionnotfound.h>
#include <pappsomspp/processing/filters/filtersuitestring.h>
#include "config.h"

using namespace pappso;
using namespace std;


TEST_CASE("Test tims XIC data extraction right form the SQLite db", "[timsXicDataSqliteDb]")
{

#if USEPAPPSOTREE == 1

  // Set the debugging message formatting pattern.
  qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));

  QString file_path_name = "";

   //file_path_name =
  //"/gorgone/pappso/jouy/raw/2021_Tims_TOF/20211124_HeLa/"
  //"11-25-2021_1_HeLa200ng_2321.d/analysis.tdf";

   file_path_name =
  "/gorgone/pappso/fichiers_fabricants/Bruker/tims_doc/tdf-sdk/"
  "example_data/200ngHeLaPASEF_2min_compressed.d/"
  "analysis.tdf";

  // file_path_name =
  //"/home/rusconi/devel/dataForMzml/bruker/20210126_HeLa.d/"
  //"1-26-2021_1_QC_HeLa10ng_826.d/analysis.tdf";

  //file_path_name =
    //"/home/rusconi/devel/dataForMzml/bruker/2021_Tims_TOF/20211124_HeLa/"
    //"11-25-2021_1_HeLa200ng_2321.d/analysis.tdf";

  qDebug() << "The file to use as a test base is: " << file_path_name;
  // When not in debug mode.
  std::cout << __FILE__ << ":" << __LINE__
            << " The file to use as a test base is: "
            << file_path_name.toStdString();

  SECTION("Test TIMS TIC chromatogram extractor")
  {
    INFO("Test the extraction from the SQLite database's Frames table");

    pappso::MsFileAccessor accessor(file_path_name, "a1");

    accessor.setPreferedFileReaderType(pappso::MzFormat::brukerTims,
                                       pappso::FileReaderType::tims);

    pappso::MsRunReaderSPtr p_msreader =
      accessor.msRunReaderSp(accessor.getMsRunIds().front());

    REQUIRE(p_msreader != nullptr);

    REQUIRE(accessor.getFileReaderType() == pappso::FileReaderType::tims);

    pappso::TimsMsRunReader *tims_reader =
      dynamic_cast<pappso::TimsMsRunReader *>(p_msreader.get());

    REQUIRE(tims_reader != nullptr);

    if(tims_reader != nullptr)
      {
        Trace tic_chromatogram = tims_reader->getTicChromatogram();

        REQUIRE(tic_chromatogram.size() != 0);

        qDebug().noquote() << "The TIC chromatogram has "
                           << tic_chromatogram.size() << " points "
                           << "and has the following points:\n"
                           << tic_chromatogram.toString();
        // When not in debug mode.
        std::cout << __FILE__ << ":" << __LINE__ << "The TIC chromatogram has "
                  << tic_chromatogram.size()
                  << " points and has the following points:\n"
                  << tic_chromatogram.toString().toStdString();
      }
  }

  SECTION("Test TIMS MS2 TIC chromatogram extractor")
  {
    INFO("Test the extraction from the SQLite database's Frames table");

    pappso::MsFileAccessor accessor(file_path_name, "a1");

    accessor.setPreferedFileReaderType(pappso::MzFormat::brukerTims,
                                       pappso::FileReaderType::tims_ms2);

    pappso::MsRunReaderSPtr p_msreader =
      accessor.msRunReaderSp(accessor.getMsRunIds().front());

    REQUIRE(p_msreader != nullptr);

    REQUIRE(accessor.getFileReaderType() == pappso::FileReaderType::tims_ms2);

    pappso::TimsMsRunReaderMs2 *tims_reader =
      dynamic_cast<pappso::TimsMsRunReaderMs2 *>(p_msreader.get());

    REQUIRE(tims_reader != nullptr);

    if(tims_reader != nullptr)
      {
        Trace tic_chromatogram = tims_reader->getTicChromatogram();

        REQUIRE(tic_chromatogram.size() != 0);

        qDebug().noquote() << "The TIC chromatogram has "
                           << tic_chromatogram.size() << " points "
                           << "and has the following points:\n"
                           << tic_chromatogram.toString();
        // When not in debug mode.
        std::cout << __FILE__ << ":" << __LINE__ << "The TIC chromatogram has "
                  << tic_chromatogram.size()
                  << " points and has the following points:\n"
                  << tic_chromatogram.toString().toStdString();
      }
  }

#elif USEPAPPSOTREE == 1

  cout << std::endl
       << "..:: NO test TIMS TIC chromatogram extraction ::.." << std::endl;

#endif
}
