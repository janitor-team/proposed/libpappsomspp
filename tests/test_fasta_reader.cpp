//
// File: test_hyperscore.cpp
// Created by: Olivier Langella
// Created on: 13/3/2015
//
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

// make test ARGS="-V -I 2,2"

#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include <iostream>
#include <pappsomspp/fasta/fastareader.h>
#include <pappsomspp/fasta/fastafileindexer.h>
#include <pappsomspp/exception/exceptionoutofrange.h>
#include <QFileInfo>
//#include "common.h"
#include "config.h"

using namespace std;
// using namespace pwiz::msdata;

class FastaSeq : public pappso::FastaHandlerInterface
{
  public:
  const QString &
  getDescription() const
  {
    return description;
  };
  const QString &
  getSequence() const
  {
    return sequence;
  };
  void
  setSequence(const QString &description_in,
              const QString &sequence_in) override
  {
    cout << std::endl << "begin description=" << description_in.toStdString();
    cout << std::endl << "sequence=" << sequence_in.toStdString();
    cout << std::endl << "end" << std::endl;
    description = description_in;
    sequence    = sequence_in;
  };

  private:
  QString description;
  QString sequence;
};


TEST_CASE("Fasta reader test suite.", "[Fasta]")
{
  // Set the debugging message formatting pattern.
  qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));

  SECTION("..:: Test Fasta reader ::..", "[Fasta]")
  {


    QFile fastaFile(QString(CMAKE_SOURCE_DIR)
                      .append("/tests/data/asr1_digested_peptides.txt"));
    FastaSeq seq;
    pappso::FastaReader reader(seq);
    reader.parse(fastaFile);

    /*
   Spectrum spectrum_low_masses(spectrum_parent.applyCutOff(150));
   if (! spectrum_low_masses.equals(sremove_low_masses, precision)) {
       cerr << "spectrum_low_masses() != tandem"<< std::endl;
       return 1;
   }*/
  }
  SECTION("..:: Test Fasta file indexer ::..", "[FastaIndexer]")
  {
    cout << std::endl << "..:: Test Fasta file indexer ::.." << std::endl;
    QFileInfo file(QString(CMAKE_SOURCE_DIR)
                     .append("/tests/data/asr1_digested_peptides.txt"));

    REQUIRE_NOTHROW(
      [&]() { pappso::FastaFileIndexer fasta_file_indexer(file); }());
    pappso::FastaFileIndexer fasta_file_indexer(file);

    FastaSeq seq;
    fasta_file_indexer.getSequenceByIndex(seq, 0);

    fasta_file_indexer.getSequenceByIndex(seq, 2);


    REQUIRE_THROWS_AS(fasta_file_indexer.getSequenceByIndex(seq, 13),
                      pappso::ExceptionOutOfRange);

    INFO(" ExceptionOutOfRange is OK ");


    try
      {
        fasta_file_indexer.getSequenceByIndex(seq, 12);

        REQUIRE(
          seq.getSequence() ==
          "HNMLGGCPK HHHHHLFHHK HNMLGGCPKER YEEHLYER RIEAIPQIDK LTQSMAIIR "
          "AEISMLEGAVLDIRYGVSR AEISMLEGAVLDIR IAYSKDFETLK YEEHLYERDEGDK "
          "GLVQPTR "
          "YIAWPLQGWQATFGGGDHPPK FELGLEFPNLPYYIDGDVK "
          "TYLNGDHVTHPDFMLYDALDVVLYMDPMCLDAFPK MSPILGYWKIK DFETLKVDFLSK "
          "GLVQPTRLLLEYLEEK IKGLVQPTR MSPILGYWK IEAIPQIDK LLLEYLEEK "
          "YIADKHNMLGGCPK "
          "SSKYIAWPLQGWQATFGGGDHPPK SDLEVLFQGPLGSMAEEK "
          "FELGLEFPNLPYYIDGDVKLTQSMAIIR "
          "IEAIPQIDKYLK YGVSRIAYSK VDFLSKLPEMLK MFEDRLCHK LPEMLKMFEDR DEGDKWR "
          "LLLEYLEEKYEEHLYER LTQSMAIIRYIADK KFELGLEFPNLPYYIDGDVK "
          "ERAEISMLEGAVLDIR");

        cout << std::endl << " getSequenceByIndex(seq, 12) is OK " << std::endl;
      }
    catch(pappso::ExceptionOutOfRange &error)
      {
        INFO(QString("ERROR: %1").arg(error.qwhat()).toStdString());
        throw error;
      }

    INFO(" getSequenceByIndex(seq, 2)");
    fasta_file_indexer.getSequenceByIndex(seq, 2);
  }
#if USEPAPPSOTREE == 1


  SECTION("..:: Test Fasta big file indexer ::..", "[FastaBigIndexer]")
  {
    FastaSeq seq;
    cout << std::endl << "..:: Test Fasta big file indexer ::.." << std::endl;
    QFileInfo file2("/gorgone/pappso/moulon/database/Genome_Z_mays_5a.fasta");
    pappso::FastaFileIndexer big_fasta_file_indexer(file2);
    big_fasta_file_indexer.getSequenceByIndex(seq, 12);

    cout << std::endl << seq.getDescription().toStdString() << std::endl;

    // GRMZM2G147579_P01
    // GRMZM2G147579_P01 NP_001159186 hypothetical protein LOC100304271
    // seq=translation; coord=5:217415249..217417029:-1;
    // parent_transcript=GRMZM2G147579_T01; parent_gene=GRMZM2G147579

    REQUIRE(seq.getDescription() ==
            "GRMZM2G147579_P01 NP_001159186 hypothetical protein LOC100304271 "
            "seq=translation; coord=5:217415249..217417029:-1; "
            "parent_transcript=GRMZM2G147579_T01; parent_gene=GRMZM2G147579");
  }

#endif

  SECTION("..:: Test fuzzy fasta file indexer ::..", "[FastaFuzzyIndexer]")
  {
    FastaSeq seq;
    QFileInfo fuzzy_file(
      QString(CMAKE_SOURCE_DIR).append("/tests/data/fuzzy.fasta"));
    pappso::FastaFileIndexer fuzzy_fasta_file_indexer(fuzzy_file);
    fuzzy_fasta_file_indexer.getSequenceByIndex(seq, 0);

    INFO("seq.getDescription().toStdString() ");

    // GRMZM2G147579_P01
    // GRMZM2G147579_P01 NP_001159186 hypothetical protein LOC100304271
    // seq=translation; coord=5:217415249..217417029:-1;
    // parent_transcript=GRMZM2G147579_T01; parent_gene=GRMZM2G147579

    REQUIRE(seq.getDescription() == "YGR254W "); // YGR254W
    fuzzy_fasta_file_indexer.getSequenceByIndex(seq, 2);

    cout << std::endl << seq.getDescription().toStdString() << std::endl;

    // GRMZM2G147579_P01
    // GRMZM2G147579_P01 NP_001159186 hypothetical protein LOC100304271
    // seq=translation; coord=5:217415249..217417029:-1;
    // parent_transcript=GRMZM2G147579_T01; parent_gene=GRMZM2G147579

    REQUIRE(seq.getDescription() == "YGR254Wb"); // YGR254Wb
                                                 /*
                                                   QFileInfo fuzzy_macos_file(
                                                     QString(CMAKE_SOURCE_DIR).append("/test/data/fuzzy_macos.fasta"));
                                                   pappso::FastaFileIndexer fuzzy_macos_fasta_file_indexer(fuzzy_macos_file);
                                                   fuzzy_macos_fasta_file_indexer.getSequenceByIndex(seq, 0);
                                             
                                                   cout << std::endl << "sequence0: " << seq.getDescription().toStdString() <<
                                                   std::endl;
                                             
                                                   // GRMZM2G147579_P01
                                                   // GRMZM2G147579_P01 NP_001159186 hypothetical protein LOC100304271
                                                   // seq=translation; coord=5:217415249..217417029:-1;
                                                   // parent_transcript=GRMZM2G147579_T01; parent_gene=GRMZM2G147579
                                             
                                                   if(seq.getDescription() != "YGR254W ") // YGR254W
                                                     {
                                                       cerr
                                                         << "sequence 0 in fuzzy_macos.fasta is not OK seq.getDescription()
                                                   != " "YGR254W "
                                                         << seq.getDescription().toStdString() << std::std::std::std::endl;
                                                       return 1;
                                                     }
                                                   fuzzy_macos_fasta_file_indexer.getSequenceByIndex(seq, 2);
                                             
                                                   cout << std::endl << "sequence2: "<< seq.getDescription().toStdString() <<
                                                   std::endl;
                                             
                                                   // GRMZM2G147579_P01
                                                   // GRMZM2G147579_P01 NP_001159186 hypothetical protein LOC100304271
                                                   // seq=translation; coord=5:217415249..217417029:-1;
                                                   // parent_transcript=GRMZM2G147579_T01; parent_gene=GRMZM2G147579
                                             
                                                   if(seq.getDescription() != "YGR254Wb") // YGR254Wb
                                                     {
                                                       cerr
                                                         << "sequence 2 in fuzzy_macos.fasta is not OK seq.getDescription()
                                                   != " "YGR254Wb "
                                                         << seq.getDescription().toStdString() << std::endl;
                                                       return 1;
                                                     }*/
  }
}
