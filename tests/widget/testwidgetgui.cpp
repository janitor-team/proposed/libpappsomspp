/**
 * \file pappsomspp/tests/widget/testwidgetgui.cpp
 * \date 17/04/2021
 * \author Olivier Langella
 * \brief minimalist main window to test widgets
 */


/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "testwidgetgui.h"
#include "ui_uitestwidgetgui.h"
#include <QDebug>

TestWidgetGui::TestWidgetGui(QWidget *parent)
  : QMainWindow(parent), ui(new Ui::TestWidgetMainWindow)
{
  qDebug();
  ui->setupUi(this);
  /*
  ui->oboChooserWidget->setMzTarget(100);
  ui->oboChooserWidget->setPrecision(
    pappso::PrecisionFactory::getDaltonInstance(0.02));
    */
}

TestWidgetGui::~TestWidgetGui()
{
}
