/**
 * \file pappsomspp/tests/widget/main.cpp
 * \date 17/04/2021
 * \author Olivier Langella
 * \brief launch minimalist GUI to test widgets
 */


/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include <QApplication>
#include <QIODevice>
#include <pappsomspp/pappsoexception.h>
#include "testwidgetgui.h"

using namespace std;

int
main(int argc, char *argv[])
{
  // Set the debugging message formatting pattern.
  qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));

  QTextStream errorStream(stderr, QIODevice::WriteOnly);
  QApplication app(argc, argv);
  
  try
    {
      QCoreApplication::setOrganizationName("PAPPSO");
      QCoreApplication::setOrganizationDomain("pappso.inrae.fr");
      QCoreApplication::setApplicationName("test Widget");
      TestWidgetGui window;
      window.show();

      return app.exec();
    }
  catch(pappso::PappsoException &error)
    {
      errorStream << "Oops! an error occurred in Test Widget. Dont Panic :"
                  << Qt::endl;
      errorStream << error.qwhat() << Qt::endl;
      app.exit(1);
    }

  catch(std::exception &error)
    {
      errorStream << "Oops! an error occurred in Test Widget. Dont Panic :"
                  << Qt::endl;
      errorStream << error.what() << Qt::endl;
      app.exit(1);
    }
}
