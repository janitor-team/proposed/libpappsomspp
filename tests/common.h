#pragma once

#include <QDebug>
#include <pappsomspp/msfile/msfileaccessor.h>
#include <pappsomspp/pappsoexception.h>
#include <iostream>
#include <iomanip>
#include <odsstream/odsdocwriter.h>

using namespace pappso;

MassSpectrum readMgf(const QString &filename);

QualifiedMassSpectrum readQualifiedMassSpectrumMgf(const QString &filename);


class XicOdsWriter
{
  public:
  XicOdsWriter(CalcWriterInterface &output) : _output(output)
  {
    _output.writeCell("rt");
    _output.writeCell("intensity");
    _output.writeLine();
  };

  void
  write(const Trace &xic)
  {
    auto it = xic.begin();
    while(it != xic.end())
      {

        _output.writeCell(it->x);
        _output.writeCell(it->y);
        _output.writeLine();
        it++;
      }
  };

  private:
  CalcWriterInterface &_output;
};

bool compareTextFiles(const QString &file1, const QString &file2);
