//
// File: test_tandem_run_wrapper.cpp
// Created by: Olivier Langella
// Created on: 12/11/2021
//
/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

// make test ARGS="-V -I 1,1"

// ./tests/catch2-only-tests [xtrunwrapper] -s

// ./tests/catch2-only-tests [xtrunwrapper] -s -a

#include <catch2/catch.hpp>
#include <QDebug>
#include <pappsomspp/processing/tandemwrapper/xtandempresetreader.h>
#include <pappsomspp/processing/tandemwrapper/wraptandeminput.h>
#include <pappsomspp/processing/tandemwrapper/wraptandemresults.h>
#include <pappsomspp/pappsoexception.h>
#include <QFileInfo>
#include <QXmlSimpleReader>

#include "config.h"
#include "common.h"

using namespace pappso;
using namespace std;


TEST_CASE("test xtandem run wrapper", "[xtrunwrapper]")
{

  // Set the debugging message formatting pattern.
  qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));

  INFO("..:: test X!Tandem run wrapper begin ::..");
  SECTION("test XtandemInputSaxHandler")
  {
    WrapTandemResults wrap_results("wrapped_tandem_output.xml",
                                   "original_mzdata_source.mzXML");

    wrap_results.setInputParameters("output, spectrum index", "true");
    REQUIRE(wrap_results.readFile(
      QString(CMAKE_SOURCE_DIR)
        .append("/tests/data/tandem/tandem_result_file.xml")));


    QFile fnew("wrapped_tandem_output.xml");
    REQUIRE(fnew.open(QFile::ReadOnly | QFile::Text));
    // REQUIRE(fnew.errorString().toStdString() == "");
    QTextStream infnew(&fnew);

    QString alltandem = infnew.readAll();


    REQUIRE(alltandem.contains(
      "<note type=\"input\" label=\"list path, default "
      "parameters\">/tmp/xtpcpp-HDpOXY/QExactive_analysis_FDR.xml</note>"));
    REQUIRE(alltandem.contains(
      QString("<note type=\"input\" label=\"spectrum, "
              "path\">%1</note>")
        .arg(QFileInfo("original_mzdata_source.mzXML").absoluteFilePath())));
    REQUIRE(alltandem.contains(
      QString("<note type=\"input\" label=\"output, "
              "path\">%1</note>")
        .arg(QFileInfo("wrapped_tandem_output.xml").absoluteFilePath())));

    REQUIRE(alltandem.contains(
      "<note type=\"input\" label=\"output, spectrum index\">true</note>"));
  }

  SECTION("test XtandemInputSaxHandler")
  {
    WrapTandemInput wrap_input("mzdata_source.mzXML",
                               "wrapped_tandem_input.xml",
                               "wrapped_tandem_output.xml");


    REQUIRE(wrap_input.readFile(
      QString(CMAKE_SOURCE_DIR)
        .append("/tests/data/tandem/tandem_run_params.xml")));

    REQUIRE(wrap_input.getOriginalTandemPresetFileName().toStdString() ==
            "/gorgone/pappso/tmp/xtpcpp.AjyZGg/"
            "Lumos_trypsin_rev_camC_oxM_10ppm_HCDOT_12102017CH.xml");

    REQUIRE(wrap_input.getOriginalMsDataFileName().toStdString() ==
            "/gorgone/pappso/jouy/raw/2021_Lumos/20210729_02_Clouet/"
            "20210729_18_blc.mzXML");

    REQUIRE(wrap_input.getOriginalTandemOutputFileName().toStdString() ==
            "/gorgone/pappso/jouy/users/Lydie/2021/2021_lumos/"
            "20210729_02_Clouet/xtpcpp/20210729_18_blc.xml");

    REQUIRE(compareTextFiles(
      "wrapped_tandem_input.xml",
      QString(CMAKE_SOURCE_DIR)
        .append("/tests/data/tandem/wrapper/wrapped_tandem_input.xml")));
  }

  SECTION("test XtandemPresetReader")
  {
    XtandemPresetReader preset_reader;


    REQUIRE_FALSE(preset_reader.readFile(
      QString(CMAKE_SOURCE_DIR)
        .append("/tests/data/tandem/settings/timstofmgf_wrong.xml")));

    REQUIRE(!preset_reader.errorString().isEmpty());

    REQUIRE(preset_reader.readFile(
      QString(CMAKE_SOURCE_DIR)
        .append("/tests/data/tandem/settings/timstofmgf.xml")));

    qDebug() << preset_reader.getMs2FiltersOptions();
    REQUIRE(preset_reader.getNumberOfThreads() == 5);
    REQUIRE(preset_reader.getMs2FiltersOptions().toStdString() ==
            "chargeDeconvolution|0.02dalton mzExclusion|0.01dalton");

    REQUIRE_FALSE(preset_reader.readFile("nofile"));

    REQUIRE(!preset_reader.errorString().isEmpty());


    REQUIRE(preset_reader.readFile(
      QString(CMAKE_SOURCE_DIR)
        .append(
          "/tests/data/tandem/settings/QExactive_analysis_FDR_nosemi.xml")));

    qDebug() << preset_reader.getMs2FiltersOptions();
    REQUIRE(preset_reader.getNumberOfThreads() == 1);
    REQUIRE(preset_reader.getMs2FiltersOptions().toStdString() ==
            "chargeDeconvolution|0.02dalton mzExclusion|0.01dalton");

    REQUIRE_FALSE(preset_reader.readFile(
      QString(CMAKE_SOURCE_DIR).append("/tests/data/tandem/database.xml")));

    REQUIRE(preset_reader.getCountNote() == 0);

    REQUIRE_FALSE(preset_reader.readFile(
      QString(CMAKE_SOURCE_DIR)
        .append("/tests/data/tandem/tandem_run_params.xml")));
    REQUIRE(preset_reader.getCountNote() == 8);
  }
}
