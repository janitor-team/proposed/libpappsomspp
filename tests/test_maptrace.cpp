#include <catch2/catch.hpp>

#include <QDebug>

#include <pappsomspp/trace/datapoint.h>
#include <pappsomspp/trace/trace.h>
#include <pappsomspp/trace/maptrace.h>

using namespace pappso;


TEST_CASE("Constructing MapTrace objects.", "[MapTrace][constructors]")
{
  // Create a Trace that looks like something real.

  std::vector<DataPoint> data_point_vector;

  // clang-format off
  data_point_vector.push_back(DataPoint("610.02000 12963.5715942383"));
  data_point_vector.push_back(DataPoint("610.07000 15639.6568298340"));
  data_point_vector.push_back(DataPoint("610.12000 55999.7628784180"));
  data_point_vector.push_back(DataPoint("610.17000 9335990.3578681946"));
  data_point_vector.push_back(DataPoint("610.2200000 635674266.1611375809"));
  data_point_vector.push_back(DataPoint("610.27000 54459.4762458801"));
  data_point_vector.push_back(DataPoint("610.32000 205580.6580276489"));
  data_point_vector.push_back(DataPoint("610.37000 84627.3196716309"));
  data_point_vector.push_back(DataPoint("610.42000 51061.7636718750"));
  data_point_vector.push_back(DataPoint("610.47000 20260.0218505859"));
  data_point_vector.push_back(DataPoint("610.52000 29848.3424072266"));
  data_point_vector.push_back(DataPoint("610.57000 27266.8031616211"));
  data_point_vector.push_back(DataPoint("610.62000 36922.3937377930"));
  data_point_vector.push_back(DataPoint("610.67000 44082.4265441895"));
  data_point_vector.push_back(DataPoint("610.72000 32580.1677703857"));
  data_point_vector.push_back(DataPoint("610.77000 39453.8380126953"));
  data_point_vector.push_back(DataPoint("610.82000 63698.7124328613"));
  data_point_vector.push_back(DataPoint("610.87000 34406.7755126953"));
  data_point_vector.push_back(DataPoint("610.92000 33385.7486267090"));
  data_point_vector.push_back(DataPoint("610.97000 22105.9357604980"));
  data_point_vector.push_back(DataPoint("611.02000 34175.9539184570"));
  data_point_vector.push_back(DataPoint("611.07000 18893.1743774414"));
  data_point_vector.push_back(DataPoint("611.12000 33655.8040771484"));
  data_point_vector.push_back(DataPoint("611.17000 1985432.9582707286"));
  data_point_vector.push_back(DataPoint("611.220000 350845489.9638078809"));
  data_point_vector.push_back(DataPoint("611.27000 201093.0304565430"));
  data_point_vector.push_back(DataPoint("611.32000 130582.0210266113"));
  data_point_vector.push_back(DataPoint("611.37000 191842.3922958374"));
  data_point_vector.push_back(DataPoint("611.42000 154248.7982788086"));
  data_point_vector.push_back(DataPoint("611.47000 31618.7268676758"));
  data_point_vector.push_back(DataPoint("611.52000 31222.7547607422"));
  data_point_vector.push_back(DataPoint("611.57000 44491.2401733398"));
  data_point_vector.push_back(DataPoint("611.62000 17576.1995697021"));
  data_point_vector.push_back(DataPoint("611.67000 29514.0727844238"));
  data_point_vector.push_back(DataPoint("611.72000 60104.3011474609"));
  data_point_vector.push_back(DataPoint("611.77000 54284.4918212891"));
  data_point_vector.push_back(DataPoint("611.82000 73222.5983886719"));
  data_point_vector.push_back(DataPoint("611.87000 34145.2793884277"));
  data_point_vector.push_back(DataPoint("611.92000 43328.2853698730"));
  data_point_vector.push_back(DataPoint("611.97000 34486.0655212402"));
  data_point_vector.push_back(DataPoint("612.02000 33462.5801696777"));
  data_point_vector.push_back(DataPoint("612.07000 45740.2189331055"));
  data_point_vector.push_back(DataPoint("612.12000 16315.6034545898"));
  data_point_vector.push_back(DataPoint("612.17000 3137434.1922011375"));
  data_point_vector.push_back(DataPoint("612.220000 244825577.0747365952"));
  data_point_vector.push_back(DataPoint("612.27000 107560.2426452637"));
  data_point_vector.push_back(DataPoint("612.32000 114637.8828430176"));
  data_point_vector.push_back(DataPoint("612.37000 87648.2245483398"));
  data_point_vector.push_back(DataPoint("612.42000 75788.5508422852"));
  data_point_vector.push_back(DataPoint("612.47000 33858.1067199707"));
  data_point_vector.push_back(DataPoint("612.52000 26404.7212524414"));
  data_point_vector.push_back(DataPoint("612.57000 45710.7806396484"));
  data_point_vector.push_back(DataPoint("612.62000 31294.2814941406"));
  data_point_vector.push_back(DataPoint("612.67000 29457.2803955078"));
  data_point_vector.push_back(DataPoint("612.72000 23048.1495971680"));
  data_point_vector.push_back(DataPoint("612.77000 15255.7875061035"));
  data_point_vector.push_back(DataPoint("612.82000 48801.3896789551"));
  data_point_vector.push_back(DataPoint("612.87000 36464.7495727539"));
  data_point_vector.push_back(DataPoint("612.92000 51507.2986145020"));
  data_point_vector.push_back(DataPoint("612.97000 45925.2813415527"));
  data_point_vector.push_back(DataPoint("613.02000 40396.0013732910"));
  data_point_vector.push_back(DataPoint("613.07000 40331.5417785645"));
  data_point_vector.push_back(DataPoint("613.12000 47593.1001892090"));
  data_point_vector.push_back(DataPoint("613.17000 1537602.4452308416"));
  data_point_vector.push_back(DataPoint("613.22000 95458729.2017828822"));
  data_point_vector.push_back(DataPoint("613.27000 117836.8993530273"));
  data_point_vector.push_back(DataPoint("613.32000 172388.2191467285"));
  data_point_vector.push_back(DataPoint("613.37000 117651.7643432617"));
  data_point_vector.push_back(DataPoint("613.42000 134527.9219970703"));
  data_point_vector.push_back(DataPoint("613.47000 21348.3722229004"));
  data_point_vector.push_back(DataPoint("613.52000 480.5255737305"));
  data_point_vector.push_back(DataPoint("613.57000 71578.8419189453"));
  data_point_vector.push_back(DataPoint("613.62000 47088.4295806885"));
  data_point_vector.push_back(DataPoint("613.67000 11516.1672668457"));
  data_point_vector.push_back(DataPoint("613.72000 42134.2611389160"));
  data_point_vector.push_back(DataPoint("613.77000 43366.3011627197"));
  data_point_vector.push_back(DataPoint("613.82000 76272.4001235962"));
  data_point_vector.push_back(DataPoint("613.87000 98113.1940002441"));
  data_point_vector.push_back(DataPoint("613.92000 29165.7173461914"));
  data_point_vector.push_back(DataPoint("613.97000 55852.7840881348"));
  data_point_vector.push_back(DataPoint("614.02000 29957.4020996094"));
  data_point_vector.push_back(DataPoint("614.07000 46380.1217651367"));
  data_point_vector.push_back(DataPoint("614.12000 22395.2603759766"));
  data_point_vector.push_back(DataPoint("614.17000 1585080.8988181949"));
  data_point_vector.push_back(DataPoint("614.22000 35239172.4581222534"));
  data_point_vector.push_back(DataPoint("614.27000 185695.5917358398"));
  data_point_vector.push_back(DataPoint("614.32000 231630.3202667236"));
  data_point_vector.push_back(DataPoint("614.37000 165712.7152709961"));
  data_point_vector.push_back(DataPoint("614.42000 115771.4071350098"));
  data_point_vector.push_back(DataPoint("614.47000 58982.7239074707"));
  data_point_vector.push_back(DataPoint("614.52000 2547873.7498645782"));
  data_point_vector.push_back(DataPoint("614.57000 18437.3129577637"));
  data_point_vector.push_back(DataPoint("614.62000 22860.9023132324"));
  data_point_vector.push_back(DataPoint("614.67000 21072.2604064941"));
  data_point_vector.push_back(DataPoint("614.72000 35379.6509933472"));
  data_point_vector.push_back(DataPoint("614.77000 59054.2345886230"));
  data_point_vector.push_back(DataPoint("614.82000 75995.3719787598"));
  data_point_vector.push_back(DataPoint("614.87000 106325.3341369629"));
  data_point_vector.push_back(DataPoint("614.92000 43670.2942276001"));
  data_point_vector.push_back(DataPoint("614.97000 39008.6439514160"));
  // clang-format on

  REQUIRE(100 == data_point_vector.size());

  // Create a Trace using the DataPoint vector.
  Trace trace(data_point_vector);
  REQUIRE(100 == trace.size());

  SECTION("Construct MapTrace from Trace", "[MapTrace]")
  {
    // Create a MapTrace
    MapTrace map_trace(trace);
    REQUIRE(100 == map_trace.size());

    // And now back to a Trace
    Trace trace1(map_trace);
    REQUIRE(100 == map_trace.size());
    REQUIRE(trace1[0].x == 610.02000);
    REQUIRE(trace1[0].y == 12963.5715942383);
    REQUIRE(trace1[99].x == 614.97000);
    REQUIRE(trace1[99].y == 39008.6439514160);
    REQUIRE(trace1[49].x == 612.47000);
    REQUIRE(trace1[49].y == 33858.1067199707);
  }

  SECTION("Construct MapTrace from MapTrace", "[MapTrace]")
  {
    // Create a MapTrace
    MapTrace map_trace(trace);
    REQUIRE(100 == map_trace.size());

    MapTrace map_trace1(map_trace);
    REQUIRE(100 == map_trace1.size());

    // Back to a trace.
    Trace trace1(map_trace1);
    REQUIRE(trace1[0].x == 610.02000);
    REQUIRE(trace1[0].y == 12963.5715942383);
    REQUIRE(trace1[99].x == 614.97000);
    REQUIRE(trace1[99].y == 39008.6439514160);
    REQUIRE(trace1[49].x == 612.47000);
    REQUIRE(trace1[49].y == 33858.1067199707);
  }
}

TEST_CASE("Initializing MapTrace objects.", "[MapTrace][initializers]")
{
  // Create a vector of doubles (x)
  std::vector<double> x_vector{1234.56789, 1233.56789, 1232.56789};

  // Create a vector of doubles (y)
  std::vector<double> y_vector{7876.54321, 8876.54321, 9876.54321};

  // Create a map
  std::map<pappso_double, pappso_double> map_of_doubles{
    {1234.56789, 7876.54321},
    {1233.56789, 8876.54321},
    {1232.56789, 9876.54321}};
  REQUIRE(3 == map_of_doubles.size());

  SECTION("Initialize MapTrace with vectors of double_s", "[MapTrace]")
  {
    MapTrace map_trace;
    std::size_t map_size = map_trace.initialize(x_vector, y_vector);
    REQUIRE(3 == map_size);
    REQUIRE(map_trace[1232.56789] == 9876.54321);
    REQUIRE(map_trace[1233.56789] == 8876.54321);
    REQUIRE(map_trace[1234.56789] == 7876.54321);
  }

  SECTION("Initialize MapTrace with map of double_s", "[MapTrace]")
  {
    MapTrace map_trace;
    std::size_t map_size = map_trace.initialize(map_of_doubles);
    REQUIRE(3 == map_size);
    REQUIRE(map_trace[1232.56789] == 9876.54321);
    REQUIRE(map_trace[1233.56789] == 8876.54321);
    REQUIRE(map_trace[1234.56789] == 7876.54321);
  }

  SECTION("Initialize MapTrace with operator =", "[MapTrace]")
  {
    MapTrace map_trace1;
    std::size_t map_size1 = map_trace1.initialize(map_of_doubles);
    REQUIRE(3 == map_size1);
    REQUIRE(map_trace1[1232.56789] == 9876.54321);
    REQUIRE(map_trace1[1233.56789] == 8876.54321);
    REQUIRE(map_trace1[1234.56789] == 7876.54321);

    MapTrace map_trace2 = map_trace1;
    REQUIRE(3 == map_trace2.size());
    REQUIRE(map_trace2[1232.56789] == 9876.54321);
    REQUIRE(map_trace2[1233.56789] == 8876.54321);
    REQUIRE(map_trace2[1234.56789] == 7876.54321);
  }
}

TEST_CASE("Basic functionality of MapTrace", "[MapTrace]")
{
  // Create a vector of doubles (x)
  std::vector<double> x_vector{1234.56789, 1233.56789, 1232.56789};

  // Create a vector of doubles (y)
  std::vector<double> y_vector{7876.54321, 8876.54321, 9876.54321};

  MapTrace map_trace;
  map_trace.initialize(x_vector, y_vector);

  SECTION("Get the x values as a vector of double_s", "[MapTrace]")
  {
    std::vector<double> vector_of_doubles = map_trace.xValues();

    REQUIRE(vector_of_doubles.size() == map_trace.size());
    REQUIRE(vector_of_doubles[0] == 1232.56789);
    REQUIRE(vector_of_doubles[1] == 1233.56789);
    REQUIRE(vector_of_doubles[2] == 1234.56789);
  }

  SECTION("Get the y values as a vector of double_s", "[MapTrace]")
  {
    std::vector<double> vector_of_doubles = map_trace.yValues();

    REQUIRE(vector_of_doubles.size() == map_trace.size());
    REQUIRE(vector_of_doubles[0] == 9876.54321);
    REQUIRE(vector_of_doubles[1] == 8876.54321);
    REQUIRE(vector_of_doubles[2] == 7876.54321);
  }

  SECTION("Get a Trace out of a MapTrace", "[MapTrace]")
  {
    Trace trace = map_trace.toTrace();

    REQUIRE(3 == trace.size());
    REQUIRE(trace[0].y == 9876.54321);
    REQUIRE(trace[1].y == 8876.54321);
    REQUIRE(trace[2].y == 7876.54321);
  }

  SECTION("Craft a string representation of the MapTrace.", "[MapTrace]")
  {
    QString result_string = map_trace.toString();

    QString test_string;

    for(auto &pair : map_trace)
      {
        test_string += QString("%1 %2\n")
                         .arg(pair.first, 0, 'f', 10)
                         .arg(pair.second, 0, 'f', 10);
      }

    REQUIRE(result_string.toStdString() == test_string.toStdString());
  }
}
