//
// File: test_isotope_with_spectrum.cpp
// Created by: Olivier Langella
// Created on: 13/3/2015
//
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

// make test ARGS="-V -I 14,14"


#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include <pappsomspp/mzrange.h>
#include <pappsomspp/peptide/peptidenaturalisotopelist.h>
#include <pappsomspp/peptide/peptidefragmentionlistbase.h>
#include <iostream>
#include <QDebug>
#include <QString>

#include "config.h"
#include "common.h"

using namespace pappso;
using namespace std;

TEST_CASE("Isotope with spectrum test suite.", "[spectrumisotope]")
{
  // Set the debugging message formatting pattern.
  qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));

  SECTION("..:: MassSpectrum ::..", "[spectrumisotope]")
  {


    // 	20120906_balliau_extract_1_A01_urnb-1
    // http://pappso.inra.fr/protic/proticprod/angular/#/peptide_hits/947252
    MassSpectrum spectrum = readMgf(
      QString(CMAKE_SOURCE_DIR).append("/tests/data/peaklist_15046.mgf"));

    cout << "spectrum size:" << spectrum.size() << std::endl;

    Peptide peptide("AIADGSLLDLLR");

    list<PeptideIon> cid_ion = PeptideFragmentIonListBase::getCIDionList();
    PeptideFragmentIonListBase frag_cid(peptide.makePeptideSp(), cid_ion);
    PeptideFragmentIonSp pep_frag_ion_sp =
      frag_cid.getPeptideFragmentIonSp(PeptideIon::y, 8);
    PeptideNaturalIsotopeList isotopeListFrag(pep_frag_ion_sp);

    std::map<unsigned int, pappso_double> map_isotope_number =
      isotopeListFrag.getIntensityRatioPerIsotopeNumber();
    std::map<unsigned int, pappso_double> map_isotope_number2real(
      map_isotope_number);

    // iterate through the m/z-intensity pairs
    // for (vector<MZIntensityPair>::const_iterator it=pairs.begin(),
    // end=pairs.end(); it!=end; ++it)
    for(auto peak : spectrum)
      {
        for(auto isotope : isotopeListFrag)
          {
            if(MzRange(isotope.get()->getMz(1),
                       PrecisionFactory::getPpmInstance(100))
                 .contains(peak.x))
              {
                cout << std::endl
                     << isotope.get()->getIsotopeNumber()
                     << "..:: " << isotope.get()->getFormula(1).toStdString()
                     << " mz1=" << isotope.get()->getMz(1)
                     << " ratio=" << isotope.get()->getIntensityRatio(1)
                     << " ::.." << std::endl;
                cout << " real mz=" << peak.x << " real intensity" << peak.y
                     << std::endl;
              }
          }
      }


    map_isotope_number = isotopeListFrag.getIntensityRatioPerIsotopeNumber();

    cout << "isotope levels" << std::endl;
    for(unsigned int i = 0; i < map_isotope_number.size(); i++)
      {
        cout << "frag isotope " << i << " " << map_isotope_number[i] << std::endl;
      }
    /*
     0..:: C39H70O11N11S0 C13(0) H2(0) O17(0) O18(0) N15(0) S34(0) mz1=886.536
    ratio=0.599391 ::.. 7:  real mz=886.535 real intensity75443.3 7: 7: 1..::
    C39H70O11N11S0 C13(1) H2(0) O17(0) O18(0) N15(0) S34(0) mz1=887.539
    ratio=0.261863 ::.. 7:  real mz=887.538 real intensity41018.6 7: 7: 1..::
    C39H70O11N11S0 C13(0) H2(1) O17(0) O18(0) N15(0) S34(0) mz1=887.542
    ratio=0.00653545 ::.. 7:  real mz=887.538 real intensity41018.6 7: 7: 1..::
    C39H70O11N11S0 C13(0) H2(0) O17(0) O18(0) N15(1) S34(0) mz1=887.533
    ratio=0.02424 ::.. 7:  real mz=887.538 real intensity41018.6
    */

    /*
    0..:: C39H70O11N11S0 C13(0) H2(0) O17(0) O18(0) N15(0) S34(0) mz1=886.536
    ratio=0.599391 ::.. 7:  real mz=886.535 real intensity75443.3 7: 7: 1..::
    C39H70O11N11S0 C13(1) H2(0) O17(0) O18(0) N15(0) S34(0) mz1=887.539
    ratio=0.261863 ::.. 7:  real mz=887.538 real intensity41018.6 7: 7: 1..::
    C39H70O11N11S0 C13(0) H2(1) O17(0) O18(0) N15(0) S34(0) mz1=887.542
    ratio=0.00653545 ::.. 7:  real mz=887.538 real intensity41018.6 7: 7: 1..::
    C39H70O11N11S0 C13(0) H2(0) O17(0) O18(0) N15(1) S34(0) mz1=887.533
    ratio=0.02424 ::.. 7:  real mz=887.538 real intensity41018.6
    */
    // SUCCESS
  }
}
