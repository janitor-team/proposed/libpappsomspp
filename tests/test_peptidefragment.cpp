//
// File: test_peptidefragment.cpp
// Created by: Olivier Langella
// Created on: 9/3/2015
//
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/


#include <pappsomspp/pappsoexception.h>
#include <pappsomspp/mzrange.h>
#include <pappsomspp/peptide/peptidefragment.h>

#include <pappsomspp/peptide/peptidefragmention.h>
#include <iostream>
#include <QDebug>
#include <QString>

using namespace pappso;
using namespace std;

int
main()
{


  cout << std::endl << "..:: peptide fragment init ::.." << std::endl;

  PeptideFragment pep_fragment(
    std::make_shared<Peptide>("PEPTIDE"), PeptideDirection::Cter, 4);


  cout << "pep_fragment" << pep_fragment.getMass();
  // SUCCESS
  PeptideFragment pep_fragmentr(
    std::make_shared<Peptide>("PEPTIDE"), PeptideDirection::Nter, 4);


  cout << "pep_fragmentr" << pep_fragmentr.getMass() << " z=1 "
       << pep_fragmentr.getMz(1) << " z=2 " << pep_fragmentr.getMz(2) << std::endl;

  try
    {
      PeptideFragmentIon ion(
        std::make_shared<PeptideFragment>(
          std::make_shared<Peptide>("PEPTIDE"), PeptideDirection::Nter, 4),
        PeptideIon::a);
    }
  catch(const PappsoException &error)
    {
      cerr << "Oops! an error occurred. Dont Panic :" << std::endl;
      cerr << error.qwhat().toStdString() << std::endl;
      return 1;
    }

  try
    {
      PeptideFragmentIon ion(
        std::make_shared<PeptideFragment>(
          std::make_shared<Peptide>("PEPTIDE"), PeptideDirection::Nter, 4),
        PeptideIon::y);
    }
  catch(const pappso::PappsoException &error)
    {
      cout << "ok, y ion is not Nter" << std::endl;
      cout << error.qwhat().toStdString() << std::endl;
    }
  catch(const std::exception &error)
    {
      cout << "ok, y ion is not Nter" << std::endl;
      cout << error.what() << std::endl;
    }
  // cout << pep_fragmentr.getMz(0);
  return 0;
}
