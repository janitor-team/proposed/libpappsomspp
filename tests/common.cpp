
#include "common.h"

using namespace pappso;

MassSpectrum
readMgf(const QString &filename)
{
  try
    {
      qDebug() << __FILE__ << "@" << __LINE__ << __FUNCTION__ << "()";
      MsFileAccessor accessor(filename, "msrun");
      qDebug() << __FILE__ << "@" << __LINE__ << __FUNCTION__ << "()";
      MsRunReaderSPtr reader =
        accessor.msRunReaderSp(accessor.getMsRunIds().front());
      qDebug() << __FILE__ << "@" << __LINE__ << __FUNCTION__ << "()"
               << accessor.getMsRunIds().front().get()->getXmlId();
      std::cout << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " "
                << reader->spectrumListSize() << std::endl;
      MassSpectrumSPtr spectrum_sp = reader->massSpectrumSPtr(0);
      std::cout << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " "
                << spectrum_sp.get()->size() << std::endl;
      spectrum_sp.get()->debugPrintValues();
      spectrum_sp.get()->sortMz();
      return *(spectrum_sp.get());
    }
  catch(pappso::PappsoException &error)
    {
      std::cout << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
                << QString("ERROR reading file %1 :")
                     .arg(filename)
                     .toStdString()
                     .c_str()
                << " " << error.what();
      throw error;
    }
}

QualifiedMassSpectrum
readQualifiedMassSpectrumMgf(const QString &filename)
{
  try
    {
      qDebug();
      MsFileAccessor accessor(filename, "msrun");
      qDebug();
      MsRunReaderSPtr reader =
        accessor.msRunReaderSp(accessor.getMsRunIds().front());
      qDebug() << accessor.getMsRunIds().front().get()->getXmlId();
      std::cout << reader->spectrumListSize() << std::endl;
      QualifiedMassSpectrum spectrum_sp =
        reader->qualifiedMassSpectrum(0, true);
      return spectrum_sp;
    }
  catch(pappso::PappsoException &error)
    {
      std::cout << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
                << QString("ERROR reading file %1 : %2")
                     .arg(filename)
                     .arg(error.qwhat())
                     .toStdString()
                     .c_str();
      throw error;
    }
}

bool
compareTextFiles(const QString &file1, const QString &file2)
{

  QFile fnew(file1);
  fnew.open(QFile::ReadOnly | QFile::Text);
  // REQUIRE(fnew.errorString().toStdString() == "");
  QTextStream infnew(&fnew);

  QFile fold(file2);
  fold.open(QFile::ReadOnly | QFile::Text);
  QTextStream infold(&fold);

  return (infold.readAll().toStdString() == infnew.readAll().toStdString());
}
