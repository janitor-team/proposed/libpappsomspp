//
// File: test_big_hyperscore.cpp
// Created by: Olivier Langella
// Created on: 19/3/2015
//
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/


#include <pappsomspp/mzrange.h>
#include <pappsomspp/peptide/peptidenaturalisotopelist.h>
#include <pappsomspp/peptide/peptidefragmentionlistbase.h>
#include <pappsomspp/massspectrum/massspectrum.h>
#include <pappsomspp/psm/xtandem/xtandemhyperscore.h>
#include <pappsomspp/pappsoexception.h>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <set>
#include <QDebug>
#include <QString>
#include <QFileInfo>
#include "config.h"
#include "saxparsers/xtandemresultshandler.h"

using namespace pappso;
using namespace std;
// using namespace pwiz::msdata;


int
main()
{
  // return 1;
  cout << std::endl
       << "..:: test all identified peptides in an XML tandem result file ::.."
       << std::endl;
  PrecisionPtr precision = PrecisionFactory::getDaltonInstance(0.02);

  //bool refine_spectrum_synthesis = false;
  QFileInfo fileinfo(
    "/gorgone/pappso/formation/Janvier2014/TD/xml_tandem/"
    "20120906_balliau_extract_1_A01_urnb-1.xml");

  XtandemResultsHandler *parser = new XtandemResultsHandler(precision);

  QXmlSimpleReader simplereader;
  simplereader.setContentHandler(parser);
  simplereader.setErrorHandler(parser);

  qDebug() << "Read tandem XML result file '" << fileinfo.filePath() << "'";

  QFile qfile(fileinfo.absoluteFilePath());
  QXmlInputSource xmlInputSource(&qfile);

  if(simplereader.parse(xmlInputSource))
    {
    }
  else
    {
      qDebug() << parser->errorString();
      // throw PappsoException(
      //    QObject::tr("error reading tandem XML result file :\n").append(
      //         parser->errorString()));
    }
  qfile.close();

  // big hyperscore test :
  // check every X!Tandem match in an XML result file
  return 0;
}
