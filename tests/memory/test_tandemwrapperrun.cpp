
// File: test_tandemwrapperrun.cpp
// Created by: Olivier Langella
// Created on: 11/2/2021
//
/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella
 *<olivier.langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

// make test ARGS="-V -I 1,1"

// ./tests/catch2-only-tests [TandemWrapperRun] -s

// valgrind --leak-check=yes ./tests/catch2-only-tests [TandemWrapperRun] -s


#include <catch2/catch.hpp>

#include <QDebug>

#include <pappsomspp/processing/tandemwrapper/tandemwrapperrun.h>
#include <pappsomspp/processing/uimonitor/uimonitortext.h>
#include "../config.h"


using namespace pappso;

TEST_CASE("running X!Tandem on tims raw data.", "[TandemWrapperRun]")
{
  qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));
#if USEPAPPSOTREE == 1

  QTextStream outputStream(stdout, QIODevice::WriteOnly);
  UiMonitorText monitor(outputStream);

  TandemWrapperRun run_tandem("/usr/bin/tandem", "/tmp");

  for(int i = 0; i < 5; i++)
    {
      INFO(QString("run %1").arg(i).toStdString());
      run_tandem.run(monitor,
                     QString("%1/tests/data/tandem/tandem_run_params.xml")
                       .arg(CMAKE_SOURCE_DIR));
    }
#elif USEPAPPSOTREE == 1

  cout << std::endl << "..:: NO test TIMS TDF parsing ::.." << std::endl;

#endif
}
