//
// File: test_fragmentationcid.cpp
// Created by: Olivier Langella
// Created on: 10/3/2015
//
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/


#include <pappsomspp/pappsoexception.h>
#include <pappsomspp/mzrange.h>

#include <pappsomspp/peptide/peptidefragmentionlistbase.h>
#include <iostream>
#include <QDebug>
#include <QString>

using namespace pappso;
using namespace std;

int
main()
{
  cout << std::endl << "..:: test ion direction ::.." << std::endl;
  if(getPeptideIonDirection(PeptideIon::b) != PeptideDirection::Nter)
    {
      cerr << "getPeptideIonDirection(PeptideIon::b) != PeptideDirection::Nter"
           << std::endl;
      return 1;
    }
  if(getPeptideIonDirection(PeptideIon::bstar) != PeptideDirection::Nter)
    {
      cerr
        << "getPeptideIonDirection(PeptideIon::bstar) != PeptideDirection::Nter"
        << std::endl;
      return 1;
    }
  if(getPeptideIonDirection(PeptideIon::bo) != PeptideDirection::Nter)
    {
      cerr << "getPeptideIonDirection(PeptideIon::bo) != PeptideDirection::Nter"
           << std::endl;
      return 1;
    }
  if(getPeptideIonDirection(PeptideIon::a) != PeptideDirection::Nter)
    {
      cerr << "getPeptideIonDirection(PeptideIon::a) != PeptideDirection::Nter"
           << std::endl;
      return 1;
    }
  if(getPeptideIonDirection(PeptideIon::astar) != PeptideDirection::Nter)
    {
      cerr
        << "getPeptideIonDirection(PeptideIon::astar) != PeptideDirection::Nter"
        << std::endl;
      return 1;
    }
  if(getPeptideIonDirection(PeptideIon::ao) != PeptideDirection::Nter)
    {
      cerr << "getPeptideIonDirection(PeptideIon::ao) != PeptideDirection::Nter"
           << std::endl;
      return 1;
    }
  if(getPeptideIonDirection(PeptideIon::bp) != PeptideDirection::Nter)
    {
      cerr << "getPeptideIonDirection(PeptideIon::bp) != PeptideDirection::Nter"
           << std::endl;
      return 1;
    }
  if(getPeptideIonDirection(PeptideIon::c) != PeptideDirection::Nter)
    {
      cerr << "getPeptideIonDirection(PeptideIon::c) != PeptideDirection::Nter"
           << std::endl;
      return 1;
    }
  if(getPeptideIonDirection(PeptideIon::y) != PeptideDirection::Cter)
    {
      cerr << "getPeptideIonDirection(PeptideIon::y) != PeptideDirection::Cter"
           << std::endl;
      return 1;
    }
  if(getPeptideIonDirection(PeptideIon::ystar) != PeptideDirection::Cter)
    {
      cerr
        << "getPeptideIonDirection(PeptideIon::ystar) != PeptideDirection::Cter"
        << std::endl;
      return 1;
    }
  if(getPeptideIonDirection(PeptideIon::yo) != PeptideDirection::Cter)
    {
      cerr << "getPeptideIonDirection(PeptideIon::yo) != PeptideDirection::Cter"
           << std::endl;
      return 1;
    }
  if(getPeptideIonDirection(PeptideIon::z) != PeptideDirection::Cter)
    {
      cerr << "getPeptideIonDirection(PeptideIon::z) != PeptideDirection::Cter"
           << std::endl;
      return 1;
    }
  if(getPeptideIonDirection(PeptideIon::yp) != PeptideDirection::Cter)
    {
      cerr << "getPeptideIonDirection(PeptideIon::yp) != PeptideDirection::Cter"
           << std::endl;
      return 1;
    }
  if(getPeptideIonDirection(PeptideIon::x) != PeptideDirection::Cter)
    {
      cerr << "getPeptideIonDirection(PeptideIon::x) != PeptideDirection::Cter"
           << std::endl;
      return 1;
    }

  cout << std::endl << "..:: peptide fragment init ::.." << std::endl;
  // http://proteus.moulon.inra.fr/w2dpage/proticdb/angular/#/peptide_hits/10053478
  Peptide peptide("DSTIPDKQITASSFYK");
  list<PeptideIon> cid_ion = PeptideFragmentIonListBase::getCIDionList();
  PeptideFragmentIonListBase frag_cid(peptide.makePeptideSp(), cid_ion);

  list<PeptideFragmentIonSp>::const_iterator it = frag_cid.begin();

  while(it != frag_cid.end())
    {
      // unsigned int size = it->get()->size();
      QString name = it->get()->getPeptideIonName();
      cout << it->get()->getSequence().toStdString() << " "
           << it->get()->getPeptideIonName().toStdString() << " "
           << it->get()->getMz(1) << " "
           << it->get()->getFormula(1).toStdString() << std::endl;
      it++;
    }

  return 0;
}
