#include <catch2/catch.hpp>

#include <QDebug>

#include <pappsomspp/trace/datapoint.h>

using namespace pappso;

TEST_CASE("Construct a non-initialized DataPoint.", "[DataPoint]")
{

  DataPoint invalid_data_point;
  REQUIRE(invalid_data_point.isValid() == false);
}


TEST_CASE("Construct DataPoint_s initialized in various ways.",
          "[DataPoint][constructors]")
{

  DataPoint datapoint1(12345.123456789, 987.987654321);

  REQUIRE(datapoint1.x == 12345.123456789);
  REQUIRE(datapoint1.y == 987.987654321);

  REQUIRE(datapoint1.isValid() == true);

  DataPoint datapoint2(datapoint1);

  REQUIRE(datapoint2.x == 12345.123456789);
  REQUIRE(datapoint2.y == 987.987654321);

  std::pair<double, double> xy_pair(12345.123456789, 987.987654321);

  DataPoint datapoint3(xy_pair);

  REQUIRE(datapoint3.x == 12345.123456789);
  REQUIRE(datapoint3.y == 987.987654321);

  datapoint3.initialize(456.6, 987.54);

  REQUIRE(datapoint3.x == 456.6);
  REQUIRE(datapoint3.y == 987.54);

  datapoint3.initialize(datapoint1);

  REQUIRE(datapoint3.x == 12345.123456789);
  REQUIRE(datapoint3.y == 987.987654321);

  // comma-separated values
  QString text1("1234.456789, 9876.54321");
  DataPoint datapoint4;

  datapoint4.initialize(text1);

  REQUIRE(datapoint4.x == 1234.456789);
  REQUIRE(datapoint4.y == 9876.54321);

  // space-separated values
  QString text2("1234.45 987.54");

  datapoint4.initialize(text2);

  REQUIRE(datapoint4.x == 1234.45);
  REQUIRE(datapoint4.y == 987.54);

  DataPoint datapoint_copy = datapoint4;
  REQUIRE(datapoint_copy.x == 1234.45);
  REQUIRE(datapoint_copy.y == 987.54);

  datapoint4.incrementX(5.123466);
  REQUIRE(datapoint4.x == (1234.45 + 5.123466));

  datapoint4.incrementY(8.123466);
  REQUIRE(datapoint4.y == (987.54 + 8.123466));
}

TEST_CASE("Basic functionality of DataPoint", "[DataPoint]")
{
  // Construct a DataPoint
  DataPoint datapoint1(12345.123456789, 987.987654321);

  SECTION("Reset a DataPoint", "[DataPoint]")
  {
    datapoint1.reset();
    REQUIRE(datapoint1.isValid() == false);
  }

  SECTION("Test identity of two DataPoint_s.", "[DataPoint]")
  {
    DataPoint datapoint2(datapoint1);
    REQUIRE(datapoint2 == datapoint1);
  }
}
