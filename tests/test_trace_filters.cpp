//
// File: test_trace_filters.cpp
// Created by: Olivier Langella
// Created on: 28/04/2019
//
/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


// make test ARGS="-V -I 1,1"

// ./tests/catch2-only-tests [filters] -s

//#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include <QDebug>
#include <QString>
#include <iostream>
#include <QFile>
#include <pappsomspp/precision.h>
#include <pappsomspp/msrun/output/mgfoutput.h>
#include <pappsomspp/processing/filters/filterresample.h>
#include <pappsomspp/processing/filters/filterpass.h>
#include <pappsomspp/processing/filters/filtermorpho.h>
#include <pappsomspp/processing/filters/filtertandemremovec13.h>
#include <pappsomspp/processing/filters/filterremovec13.h>
#include <pappsomspp/processing/filters/filtercomplementionenhancer.h>
#include <pappsomspp/psm/xtandem/xtandemhyperscore.h>
#include <pappsomspp/processing/filters/filtersuitestring.h>
#include <pappsomspp/processing/filters/filterchargedeconvolution.h>
#include "common.h"
#include "config.h"


using namespace pappso;
using namespace std;

TEST_CASE("trace filters test suite.", "[filters]")
{
  // Set the debugging message formatting pattern.
  qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));

  // QCoreApplication a(argc, argv);
  SECTION("..:: test trace filters ::..", "[filters]")
  {
    //   OboPsiMod test;

    MassSpectrum spectrum_simple =
      readMgf(QString(CMAKE_SOURCE_DIR)
                .append("/tests/data/peaklist_15046_simple_xt.mgf"));
    //.applyCutOff(150).takeNmostIntense(100).applyDynamicRange(100);
    cout << std::endl << "..:: FilterGreatestY ::.." << std::endl;

    qDebug();
    MassSpectrum most_intense(spectrum_simple);
    most_intense.filter(FilterGreatestY(10));

    REQUIRE(most_intense.size() == 10);

    qDebug();
    most_intense = spectrum_simple;
    most_intense.massSpectrumFilter(
      pappso::MassSpectrumFilterGreatestItensities(10));

    REQUIRE(most_intense.size() == 10);
    qDebug();
    most_intense =
      MassSpectrum(spectrum_simple)
        .massSpectrumFilter(pappso::MassSpectrumFilterGreatestItensities(1000));

    REQUIRE(most_intense.size() == 100);
    qDebug();
    most_intense = MassSpectrum(spectrum_simple).filter(FilterGreatestY(100));

    REQUIRE(most_intense.size() == 100);
    qDebug();
    // 3.018830
    cout << std::endl << "..:: resample filters ::.." << std::endl;
    Trace deca_suite({DataPoint(0, 1),
                      DataPoint(1, 2),
                      DataPoint(2, 3),
                      DataPoint(3, 4),
                      DataPoint(4, 3),
                      DataPoint(5, 2),
                      DataPoint(6, 1),
                      DataPoint(7, 1),
                      DataPoint(8, 2),
                      DataPoint(9, 3),
                      DataPoint(10, 4),
                      DataPoint(11, 5),
                      DataPoint(12, 6)});
    Trace deca_keep({DataPoint(9, 3), DataPoint(10, 4), DataPoint(11, 5)});

    Trace deca_remove({DataPoint(0, 1),
                       DataPoint(1, 2),
                       DataPoint(2, 3),
                       DataPoint(3, 4),
                       DataPoint(4, 3),
                       DataPoint(5, 2),
                       DataPoint(6, 1),
                       DataPoint(7, 1),
                       DataPoint(8, 2),
                       DataPoint(12, 6)});
    Trace deca_remove2({DataPoint(0, 1),
                        DataPoint(1, 2),
                        DataPoint(2, 3),
                        DataPoint(3, 4),
                        DataPoint(4, 3),
                        DataPoint(5, 2),
                        DataPoint(6, 1),
                        DataPoint(7, 1),
                        DataPoint(8, 2)});
    Trace deca_smaller({DataPoint(0, 1), DataPoint(1, 2), DataPoint(2, 3)});

    Trace deca_greater({DataPoint(10, 4), DataPoint(11, 5), DataPoint(12, 6)});
    Trace deca(deca_suite);
    deca.filter(FilterResampleRemoveXRange(9, 11.5));
    REQUIRE(deca == deca_remove);
    deca = deca_suite;
    deca.filter(FilterResampleRemoveXRange(9, 12));
    REQUIRE(deca == deca_remove2);

    deca = deca_suite;
    deca.filter(FilterResampleKeepXRange(9, 11));
    REQUIRE(deca == deca_keep);


    deca = deca_suite;
    deca.filter(FilterResampleKeepSmaller(3));
    REQUIRE(deca == deca_smaller);

    deca = deca_suite;
    deca.filter(FilterResampleKeepGreater(9));
    REQUIRE(deca == deca_greater);
    cout << std::endl << "..:: morpho filters ::.." << std::endl;

    deca = deca_suite;
    deca.filter(FilterMorphoMedian(1));
    Trace deca_median({DataPoint(0, 2),
                       DataPoint(1, 2),
                       DataPoint(2, 3),
                       DataPoint(3, 3),
                       DataPoint(4, 3),
                       DataPoint(5, 2),
                       DataPoint(6, 1),
                       DataPoint(7, 1),
                       DataPoint(8, 2),
                       DataPoint(9, 3),
                       DataPoint(10, 4),
                       DataPoint(11, 5),
                       DataPoint(12, 6)});
    REQUIRE(deca.size() == deca_median.size());
    cout << "deca.size() == deca_median.size() " << deca.size() << std::endl;
    REQUIRE(deca == deca_median);

    cout << std::endl << "..:: FilterTandemDeisotope ::.." << std::endl;
    qDebug();
    MassSpectrum remove_tandem_deisotope =
      MassSpectrum(spectrum_simple)
        .massSpectrumFilter(FilterTandemDeisotope(1.5, 200));

    REQUIRE(remove_tandem_deisotope.size() == 97);
    qDebug();


    QualifiedMassSpectrum spectrum_scan_15968 = readQualifiedMassSpectrumMgf(
      QString(CMAKE_SOURCE_DIR).append("/tests/data/scan_15968.mgf"));
    qDebug();
    QualifiedMassSpectrum spectrum_removed_tandem_deisotope =
      QualifiedMassSpectrum(spectrum_scan_15968).cloneMassSpectrumSPtr();
    qDebug();
    spectrum_removed_tandem_deisotope.getMassSpectrumSPtr()
      .get()
      ->massSpectrumFilter(FilterTandemDeisotope());
    qDebug();
    QFile file("scan_15968_tandem_deisotope.mgf");
    if(file.open(QIODevice::WriteOnly | QIODevice::Text))
      {
        MgfOutput mgf_output(&file);
        mgf_output.write(spectrum_removed_tandem_deisotope);
        mgf_output.close();
        file.close();
      }
    qDebug();

    Peptide pep15968("EITLGFVDLLR");
    qDebug();
    std::list<PeptideIon> ion_list;
    ion_list.push_back(PeptideIon::y);
    ion_list.push_back(PeptideIon::b);
    qDebug();

    cout << std::endl << "..:: FilterRemoveC13 ::.." << std::endl;
    qDebug();
    MassSpectrum remove_c13 =
      MassSpectrum(spectrum_simple)
        .filter(FilterRemoveC13(PrecisionFactory::getDaltonInstance(0.02)));

    REQUIRE(remove_c13.size() == 93);
    qDebug();

    QualifiedMassSpectrum spectrum_removed_c13 =
      QualifiedMassSpectrum(spectrum_scan_15968).cloneMassSpectrumSPtr();

    spectrum_removed_c13.getMassSpectrumSPtr().get()->filter(
      FilterRemoveC13(PrecisionFactory::getDaltonInstance(0.02)));

    QFile file2("scan_15968_remove_c13.mgf");
    if(file2.open(QIODevice::WriteOnly | QIODevice::Text))
      {
        MgfOutput mgf_output(&file2);
        mgf_output.write(spectrum_removed_c13);
        mgf_output.close();
        file2.close();
      }


    XtandemHyperscore hyperscore15968(
      *spectrum_scan_15968.getMassSpectrumSPtr().get(),
      pep15968.makePeptideSp(),
      2,
      PrecisionFactory::getDaltonInstance(0.02),
      ion_list,
      true);
    cout << "peptide " << pep15968.getSequence().toStdString()
         << " hyperscore15968=" << hyperscore15968.getHyperscore() << std::endl;
    cout << "peptide " << pep15968.getSequence().toStdString()
         << " mz1=" << pep15968.getMz(1) << " mz2=" << pep15968.getMz(2)
         << std::endl;

    XtandemHyperscore hyperscore15968_remove_c13(
      *spectrum_removed_c13.getMassSpectrumSPtr().get(),
      pep15968.makePeptideSp(),
      2,
      PrecisionFactory::getDaltonInstance(0.02),
      ion_list,
      true);
    cout << "peptide " << pep15968.getSequence().toStdString()
         << " hyperscore15968_remove_c13="
         << hyperscore15968_remove_c13.getHyperscore() << std::endl;
    cout << "peptide " << pep15968.getSequence().toStdString()
         << " mz1=" << pep15968.getMz(1) << " mz2=" << pep15968.getMz(2)
         << std::endl;


    cout << std::endl
         << "..:: FilterMassSpectrumComplementIonEnhancer ::.." << std::endl;
    qDebug();

    QualifiedMassSpectrum spectrum_complement_ion =
      QualifiedMassSpectrum(spectrum_scan_15968).cloneMassSpectrumSPtr();

    spectrum_complement_ion.getMassSpectrumSPtr().get()->filter(
      FilterComplementIonEnhancer(spectrum_complement_ion,
                                  PrecisionFactory::getDaltonInstance(0.02)));

    QFile file3("scan_15968_complement_ion_enhancer.mgf");
    if(file3.open(QIODevice::WriteOnly | QIODevice::Text))
      {
        MgfOutput mgf_output(&file3);
        mgf_output.write(spectrum_complement_ion);
        mgf_output.close();
        file3.close();
      }

    XtandemHyperscore hyperscore15968_complement_ion_enhancer(
      *spectrum_complement_ion.getMassSpectrumSPtr().get(),
      pep15968.makePeptideSp(),
      2,
      PrecisionFactory::getDaltonInstance(0.02),
      ion_list,
      true);
    cout << "peptide " << pep15968.getSequence().toStdString()
         << " hyperscore15968_complement_ion_enhancer="
         << hyperscore15968_complement_ion_enhancer.getHyperscore()
         << std::endl;
    cout << "peptide " << pep15968.getSequence().toStdString()
         << " mz1=" << pep15968.getMz(1) << " mz2=" << pep15968.getMz(2)
         << std::endl;


    cout << std::endl << "..:: FilterGreatestYperWindow ::.." << std::endl;
    FilterGreatestYperWindow filter_per_window(40, 2);
    qDebug();
    QualifiedMassSpectrum spectrum_greatest_y_per_window =
      QualifiedMassSpectrum(spectrum_scan_15968).cloneMassSpectrumSPtr();
    qDebug();
    filter_per_window.filter(
      *spectrum_greatest_y_per_window.getMassSpectrumSPtr().get());
    qDebug();
    QFile file4("scan_15968_greatest_y_per_window.mgf");
    if(file4.open(QIODevice::WriteOnly | QIODevice::Text))
      {
        MgfOutput mgf_output(&file4);
        mgf_output.write(spectrum_greatest_y_per_window);
        mgf_output.close();
        file4.close();
      }
    qDebug();


    cout << std::endl
         << "..:: FilterGreatestYperWindow  on scan_PXD001468.mgf ::.."
         << std::endl;
    QualifiedMassSpectrum spectrum_scan_PXD001468 =
      readQualifiedMassSpectrumMgf(
        QString(CMAKE_SOURCE_DIR).append("/tests/data/scan_PXD001468.mgf"));
    qDebug();
    QualifiedMassSpectrum spectrum_greatest_y_per_window_PXD001468 =
      QualifiedMassSpectrum(spectrum_scan_PXD001468).cloneMassSpectrumSPtr();
    qDebug();
    spectrum_greatest_y_per_window_PXD001468.getMassSpectrumSPtr()
      .get()
      ->filter(FilterGreatestYperWindow(40, 2));
    qDebug();
    QFile file5("scan_PXD001468_greatest_y_per_window.mgf");
    if(file5.open(QIODevice::WriteOnly | QIODevice::Text))
      {
        MgfOutput mgf_output(&file5);
        mgf_output.write(spectrum_greatest_y_per_window_PXD001468);
        mgf_output.close();
        file5.close();
      }
    qDebug();


    std::shared_ptr<pappso::FilterSuite> filter_suite =
      std::make_shared<pappso::FilterSuite>();
    filter_suite->push_back(std::make_shared<FilterGreatestYperWindow>(40, 2));
    filter_suite->push_back(std::make_shared<FilterRemoveC13>(
      PrecisionFactory::getDaltonInstance(0.02)));

    /*
      XtandemHyperscore hyperscore15968_greatest_y_per_window(
        *spectrum_greatest_y_per_window.getMassSpectrumSPtr().get(),
        pep15968.makePeptideSp(),
        2,
        PrecisionFactory::getDaltonInstance(0.02),
        ion_list,
        true);
      cout << "peptide " << pep15968.getSequence().toStdString()
           << " hyperscore15968_greatest_y_per_window="
           << hyperscore15968_greatest_y_per_window.getHyperscore() <<
      std::endl; cout << "peptide " << pep15968.getSequence().toStdString()
           << " mz1=" << pep15968.getMz(1) << " mz2=" << pep15968.getMz(2) <<
      endl;

    */

    cout << std::endl << "..:: FilterSuiteString ::.." << std::endl;

    std::shared_ptr<pappso::FilterSuiteString> filter_suite_str =
      std::make_shared<pappso::FilterSuiteString>(
        "chargeDeconvolution|0.02dalton");


    cout << std::endl
         << "filter_suite_str = "
         << filter_suite_str.get()->toString().toStdString() << std::endl;

    cout << std::endl
         << "..:: FilterChargeDeconvolution from FilterSuiteString ::.."
         << std::endl;

    std::shared_ptr<pappso::FilterChargeDeconvolution>
      filter_charge_deconvolution =
        std::make_shared<pappso::FilterChargeDeconvolution>(
          filter_suite_str.get()->toString());

    Trace loaded_trace({// First peak z = 2
                        DataPoint(10, 20),
                        DataPoint(10.5, 18),
                        DataPoint(11, 16),
                        // Not a real peak
                        DataPoint(15, 50),
                        DataPoint(16, 51),
                        // Second peak z = 1
                        DataPoint(24, 200),
                        DataPoint(25, 20),
                        // Third peak z = 2
                        DataPoint(31, 20),
                        DataPoint(31.5, 15)});
    Trace theorical_trace({// Not modified peaks
                           DataPoint(15, 50),
                           DataPoint(16, 51),
                           // First peak merged
                           DataPoint(20 - MHPLUS, 54),
                           // Second
                           DataPoint(24, 220),
                           // Third
                           DataPoint(62 - MHPLUS, 35)});
    cout << "Transform Trace data" << std::endl;
    filter_charge_deconvolution->filter(loaded_trace);


    if(loaded_trace != theorical_trace)
      {
        cerr << "transformed Trace are different from theorical trace :"
             << std::endl;

        if(loaded_trace.xValues().size() == theorical_trace.xValues().size())
          {
            cerr << "transformed trace: \t";
            for(std::size_t i = 0; i < loaded_trace.xValues().size(); i++)
              {
                cerr << "(" << loaded_trace.xValues()[i] << ", "
                     << loaded_trace.yValues()[i] << ") ";
              }
            cerr << std::endl;
            cerr << "theorical trace : \t";
            for(std::size_t i = 0; i < theorical_trace.xValues().size(); i++)
              {
                cerr << "(" << theorical_trace.xValues()[i] << ", "
                     << theorical_trace.yValues()[i] << ") ";
              }
            cerr << std::endl;
          }
        else
          {
            cerr << "Number of peaks is different : "
                 << loaded_trace.xValues().size() << " vs "
                 << theorical_trace.xValues().size();
          }
      }

    REQUIRE(loaded_trace == theorical_trace);
  }
  SECTION("..:: test FilterMorphoAntiSpike ::..", "[filters]")
  {
    // MESSAGE("..:: FilterMorphoAntiSpike filters ::..");
    Trace anti_spike_trace({DataPoint(0, 0),
                            DataPoint(1, 1),
                            DataPoint(2, 3),
                            DataPoint(3, 0),
                            DataPoint(4, 0),
                            DataPoint(5, 0),
                            DataPoint(6, 0),
                            DataPoint(7, 0),
                            DataPoint(8, 0),
                            DataPoint(9, 3),
                            DataPoint(10, 0),
                            DataPoint(11, 0),
                            DataPoint(12, 0)});
    FilterMorphoAntiSpike filter_anti_spike(2);
    filter_anti_spike.filter(anti_spike_trace);

    REQUIRE(anti_spike_trace[9].y == 0);
    REQUIRE(anti_spike_trace[2].y == 3);
    // cout << std::endl << anti_spike_trace.toString().toStdString() <<
    // std::endl;

    INFO("filter_anti_spike_too_big");
    FilterMorphoAntiSpike filter_anti_spike_too_big(7);
    filter_anti_spike_too_big.filter(anti_spike_trace);
  }

  SECTION("..:: check empty trace ::..", "[filters]")
  {
    // MESSAGE("..:: FilterMorphoAntiSpike filters ::..");
    Trace empty_trace;
    FilterMorphoMean m_smooth(3);

    REQUIRE_NOTHROW(m_smooth.filter(empty_trace));


    FilterMorphoMinMax m_minMax(5);
    REQUIRE_NOTHROW(m_minMax.filter(empty_trace));

    FilterMorphoMaxMin m_maxMin(5);
    REQUIRE_NOTHROW(m_maxMin.filter(empty_trace));

    Trace xic;
    xic.push_back(DataPoint(1434.7806210000, 85.9931205504));
    xic.push_back(DataPoint(1586.4034760000, 162.9869610431));
    xic.push_back(DataPoint(1643.5517360000, 84.9932005440));
    xic.push_back(DataPoint(1645.8830080000, 98.9920806335));

    FilterMorphoMaxMin m_maxMin2(2);
    REQUIRE_NOTHROW(m_maxMin2.filter(xic));
  }

  SECTION("..:: check FilterQuantileBasedRemoveY ::..", "[filters]")
  {

    Trace xic_PXD0014777 =
      *(readQualifiedMassSpectrumMgf(
          QString(CMAKE_SOURCE_DIR).append("/tests/data/xic/PXD014777_xic.tsv"))
          .getMassSpectrumSPtr()
          .get());

    REQUIRE(Approx(62.43).epsilon(0.01) ==
            quantileYTrace(xic_PXD0014777.begin(), xic_PXD0014777.end(), 0.1));
    REQUIRE(Approx(250.75).epsilon(0.01) ==
            quantileYTrace(xic_PXD0014777.begin(), xic_PXD0014777.end(), 0.5));
    REQUIRE(Approx(136.96).epsilon(0.01) ==
            quantileYTrace(xic_PXD0014777.begin(), xic_PXD0014777.end(), 0.3));


    FilterQuantileBasedRemoveY quantile(0.1);
    xic_PXD0014777 = quantile.filter(xic_PXD0014777);


    QFile fnew("quantile_trace.tsv");
    fnew.open(QFile::WriteOnly | QFile::Text);
    QTextStream out(&fnew);
    out << xic_PXD0014777.toString();
  }
}
