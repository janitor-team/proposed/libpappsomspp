
/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/


#include <iostream>
#include <odsstream/odsdocreader.h>
#include <odsstream/odsdocwriter.h>

#include <pappsomspp/pappsoexception.h>
#include <pappsomspp/msfile/msfileaccessor.h>
#include <pappsomspp/xicextractor/msrunxicextractorfactory.h>
#include <pappsomspp/processing/uimonitor/uimonitortext.h>
#include <pappsomspp/processing/uimonitor/uimonitortext.h>
#include <QDebug>
#include <QElapsedTimer>
#include <QtCore>
#include <QFile>
#include "config.h"
//#include "common.h"

// make test ARGS="-V -I 19,19"

using namespace std;


int
main([[maybe_unused]] int argc, [[maybe_unused]] char *argv[])
{
  // QCoreApplication a(argc, argv);

  qDebug() << "init test XIC extractor";
  cout << std::endl << "..:: Test XIC ::.." << std::endl;
  QElapsedTimer timer;

#if USEPAPPSOTREE == 1
  pappso::MsFileAccessor file_access(
    //"/gorgone/pappso/data_extraction_pappso/mzXML/"
    "/home/langella/data1/mzxml/"
    "20120906_balliau_extract_1_A01_urnb-1.mzXML",
    "file");
  pappso::MsRunReaderSPtr msrun =
    file_access.getMsRunReaderSPtrByRunId("", "runa1");

  pappso::MsRunXicExtractorFactory::getInstance().setTmpDir("/tmp");

  pappso::MzRange mass_range(600, pappso::PrecisionFactory::getPpmInstance(10));

  pappso::XicCoordSPtr xic_coord =
    msrun.get()->newXicCoordSPtrFromSpectrumIndex(
      3000, pappso::PrecisionFactory::getPpmInstance(10));

  xic_coord.get()->mzRange = mass_range;

  std::vector<pappso::XicCoordSPtr> xic_coord_list;
  xic_coord_list.push_back(xic_coord.get()->initializeAndClone());
  // MassRange mass_range_b(600.2, Precision::getPpmInstance(10));

  pappso::MsRunXicExtractorInterfaceSp extractor_pwiz =
    pappso::MsRunXicExtractorFactory::getInstance().buildMsRunXicExtractorSp(
      msrun);

  extractor_pwiz->setXicExtractMethod(pappso::XicExtractMethod::max);


  QTextStream outputStream(stdout, QIODevice::WriteOnly);
  pappso::UiMonitorText monitor(outputStream);
  extractor_pwiz.get()->extractXicCoordSPtrList(monitor, xic_coord_list);
  pappso::XicCstSPtr xic_pwiz = xic_coord_list[0].get()->xicSptr;

  cout << std::endl << "..:: XIC extractor pwiz is OK ::.." << std::endl;

  cout << std::endl << "building XicExtractorDiskSp .." << std::endl;
  timer.start();
  pappso::MsRunXicExtractorFactory::getInstance()
    .setMsRunXicExtractorFactoryType(
      pappso::MsRunXicExtractorFactoryType::disk);
  pappso::MsRunXicExtractorInterfaceSp extractor_disk =
    pappso::MsRunXicExtractorFactory::getInstance().buildMsRunXicExtractorSp(
      msrun);
  cout << std::endl
       << "XicExtractorDiskSp built " << timer.elapsed() / 1000 << " seconds"
       << std::endl;


  xic_coord_list.clear();
  xic_coord_list.push_back(xic_coord.get()->initializeAndClone());
  extractor_disk.get()->extractXicCoordSPtrList(monitor, xic_coord_list);

  pappso::XicCstSPtr xic_disk = xic_coord_list[0].get()->xicSptr;

  if(*xic_pwiz != *xic_disk)
    {
      cerr << "*xic_pwiz != *xic_disk ERROR" << std::endl;

      for(std::size_t i = 0; i < xic_pwiz->size(); i++)
        {
          cerr << "i=" << i << " x=" << (*xic_pwiz)[i].x
               << " y=" << (*xic_pwiz)[i].y << " x=" << (*xic_disk)[i].x
               << " y=" << (*xic_disk)[i].y << std::endl;
        }
      return 1;
    }

  cout << std::endl << "..:: XIC extractor on disk is OK ::.." << std::endl;
  // return 0;

  cout << std::endl << "building XicExtractorDiskBufferSp .." << std::endl;
  timer.restart();

  pappso::MsRunXicExtractorFactory::getInstance()
    .setMsRunXicExtractorFactoryType(
      pappso::MsRunXicExtractorFactoryType::diskbuffer);
  pappso::MsRunXicExtractorInterfaceSp extractor_buffer =
    pappso::MsRunXicExtractorFactory::getInstance().buildMsRunXicExtractorSp(
      msrun);
  cout << std::endl
       << "XicExtractorDiskBufferSp built " << timer.elapsed() / 1000
       << " seconds" << std::endl;


  xic_coord_list.clear();
  xic_coord_list.push_back(xic_coord.get()->initializeAndClone());
  extractor_buffer.get()->extractXicCoordSPtrList(monitor, xic_coord_list);

  pappso::XicCstSPtr xic_buffer = xic_coord_list[0].get()->xicSptr;


  if(*xic_pwiz != *xic_buffer)
    {
      cerr << "*xic_pwiz != *xic_buffer ERROR" << std::endl;
      /*
      for(std::size_t i = 0; i < xic_pwiz->size(); i++)
        {
          cerr << "i=" << i << " " << (*xic_pwiz)[i].y << " "
               << (*xic_buffer)[i].y << std::endl;
        }
        */
      return 1;
    }

  cout << std::endl << "..:: XIC extractor buffered on disk is OK ::.." << std::endl;
#endif
  return 0;
}
