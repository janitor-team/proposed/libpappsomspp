Coding standards
================

When setting/retrieving member data, use the get/set paradigm.
Do not use the get/set paradigm for any function that performs a computation not
setting/getting a member datum. Note that the get/set function name needs to be
formed using the exact member datum name with the "mxx_" prefix.

When setting/retrieving boolean member data use setXxxx() and isXxxx().

Use std::size_t instead of unsigned int (or uint) when the standard library uses
std::site_t.

In the header files, #pragma once should be the first line of code.
Then, the order of the inclusions should be:

#include std stuff

#include Qt stuff

#include the header corresponding to the cpp file
#include all header according to intuition


For ordinary members:

m_exampleVariableName;

For pointer members
===================

mp_variableNamePointer for a conventional pointer that is not allocated in the object and that
does not need destruction in the class destructor

mpa_xxx for a heap-allocated instance in the class that will need destruction

msp_xxx for a shared pointer allocated using std::make_shared<T>()

mcsp_xxx for a constant shared pointer allocated using std::make_shared<T>()


Variables in function scope
===========================

QualifiedMassSpectrum qualified_mass_spectrum;


typedef std::shared_ptr<MassSpectrum> MassSpectrumSPtr;
typedef std::shared_ptr<const MassSpectrum> MassSpectrumCstSPtr;


In the get/set function pair, declare/define first the set and then the get.


Construction of objects and initialization
==========================================

Construction of objects with initialization must be performed traditionally
(C++98). Only when initializing a container, the braces ({xxx,xxx})
initialization is authorized.



Proteowizard fournit des Vectors en sortie de fichier. Je suggere d'implémenter
Trace et MassSpectrum comme des std::vector de new (struct DataPoint (x,y));

std::vector est le meilleur conteneur possible pour des données non mutables
(les données dans les spectres de masse sont non mutables). Les données sont
éminemment mutables quand il y a construction d'un spectre de masse par
combinaison. Là, on fera du std:map.

~~~~~~~~~~~~~~~~~~~~~~~~
https://marcmutz.wordpress.com/effective-qt/containers/

QVector
=======

So let’s break the spell right away: The default container should be vector (std
or Q).

While programmers trained in the STL won’t think about using anything else than
a std::vector as a sequential container (until the profiler tells them to), and
many who have read about the trick will even replace some uses of associative
containers with (sorted) vectors, people who have (only) received Qt training
reach out to QList by default.

If you don’t yet understand why vectors should be preferred, please read The
Good Book, or Effective STL, or, for that matter, Ulrich Drepper’s excellent
paper What Every Programmer Should Know About Memory. However, while Amazon gets
the shipment ready, do continue reading this article and start following the

QVector is perhaps the Qt container closest akin to its STL counterpart. That it
nonetheless performs worse than std::vector on many platforms is due to the fact
that its internal structure is more complex (another indirection through the
d-pointer, e.g.). 

Guideline: Prefer vector (std or Q) over QList. 

QList
=====
So, QList is not a good default container. But are there situations where
QList is preferable over QVector? Sadly, the answer is no. It would be best if,
come Qt 5, the Trolls just went and replaced all occurrences of QList with
QVector. The few benchmarks in which QList outperforms QVector are either
irrelevant in practice, or should be fixable by optimising QVector better.
~~~~~~~~~~~~~~~~~~~~~~~~~~

~~~~~~~~~~~~~~~~~~~~~~~~~
https://www.dvratil.cz/2015/06/qt-containers-and-c11-range-based-loops/

Explique pourquoi dans certaines conditions, 

Q_FOREACH (const QString &v, myObject.getList()) {
   ...
}
 
est plus performant que 

for (const QString &v : myObject.getList()) {
  ...
}

The difference between the first example and this one is that the QStringList in
this example is shared, i.e. reference count of it’s data is higher than 1. In
this particular case one reference is held by myObject and one reference is held
by the copy returned from the getList() method. That means that calling any
non-const method on the list will call detach() and perform a deep copy of the
list. And that is exactly what is happening in the range-based loop (but not in
the Q_FOREACH loop) and that’s why the range-based loop is way slower than
Q_FOREACH in this particular case. The example above could be even simpler, but
this way it highlights the important fact that returning a copy from a method
means that the copy is shared and has negative side-effects when used with
range-based loops. Note that if the method would return a const reference to
QStringList, everything would be OK (because const …).
~~~~~~~~~~~~~~~~~~~~~~~~

~~~~~~~~~~~~~~~~~~~~~~~~
https://web.archive.org/web/20160902015144/http://blog.codeimproved.net/posts/qtl-stl.html

My opinion is that the biggest advantage of the QTL is that it has the same
implementation (including binary compatibility) on all OSes supported by Qt.
Some STL implementations might be below par when it comes to performance or they
might be missing functionality. Some platforms don't even have an STL! On the
other hand, the STL is more customizable and is available in its entirety in
header files... Like I said, there is no clear winner.
~~~~~~~~~~~~~~~~~~~~~~~~

~~~~~~~~~~~~~~~~~~~~~~~~
http://www.acodersjourney.com/2016/11/6-tips-supercharge-cpp-11-vector-performance/


~~~~~~~~~~~~~~~~~~~~~~~~




